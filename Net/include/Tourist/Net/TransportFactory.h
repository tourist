#ifndef TRANSPORTFACTORY_H_
#define TRANSPORTFACTORY_H_

#include "Tourist/AbstractPipe.h"
#include "Tourist/AbstractSocketReactor.h"
#include "Tourist/Net/SocketReactorFactory.h"
#include "Tourist/Util/Config.h"
#include "Tourist/Util/CallbackInterface.h"
#include "Poco/Logger.h"
#include "Poco/Net/SocketNotification.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/DNS.h"
#include "Poco/Net/HostEntry.h"
#include "Poco/Observer.h"
#include <sys/time.h>
#include <map>

using namespace std;

using Tourist::AbstractPipe;
using Tourist::AbstractSocketReactor;
using Tourist::Net::SocketReactorFactory;
using Tourist::Util::Config;
using Tourist::Util::CallbackInterface;

using Poco::Logger;
using Poco::Net::ReadableNotification;
using Poco::Net::ServerSocket;
using Poco::Net::SocketAddress;
using Poco::Observer;

/**
 * Hanlde network transport related functions.
 *
 * It inities different transport protocols like TCP, Reliable UDP, HTTP etc.
 *
 * Also it provides an interface for other components to get the protocol specific
 * pipe for sending/retrieving messages.The ownership of these pipes remains with 
 * this class.
 *
 * \Author Faisal Khan
 * \Version 0.1
 */

namespace Tourist {
namespace Net {

class TransportFactory
{
public:

  /*
   * No initialization of any network is done until
   * initServers is called with a configuration object.
   */
  TransportFactory();
  
  ~TransportFactory();
  
  /* Let's remove the singleton instance, its havoc!
  
  /**
   * The current way of creating a transport factory.
   *
  static TransportFactory* getInstance();

  static void release();

  static void destroy();*/
  
  /**
   * Initialize requested transport protocols for 'Tourist'.
   *
   *  \param protocol Protocol server to start. Combinations are also allowd
   *            e.g TCP|RUDP
   */
  void initServers(Config &config);

  /**
   * Either creates a new protocol pipe or returns an existing one.
   *
   * All the communication in tourist is done through the use of
   * of implementation of different abstract pipes.
   * For example: Tourist::Net::TCPPipe for tcp communication.
   *
   * \param add Remote endpoint
   * \param protocol Transport protocol.
   */
  AbstractPipe* getPipe(SocketAddress& add, int protocol);

  /**
   * Returns true if the TCP protocol is enabled or not.
   */

  bool getTCP();

  /**
   * This method just returns the one of the interfaces on
   * this host.
   */
  string getHostName();

  int getTCPPort();

  /*
   * Bandwidth is measured by iterating through all the pipes in the
   * application, counting bytes transfered in both directions
   * (Bytes-IN, Bytes-Out) and dividing it to the time when this
   * instance was initialized.
   *
   * This approach is not very accurate, as we will lose certain number of
   * bytes suddenly as the pipe is destroyed. Which mean a sudden decrease
   * in the bw usage. A more accurate approach is in the TODO list.
   */
 // int getBWUsage();
  
  /**
   * Call whenever a new peer is connected to this host
   */
  
  int registerConnectionNotification(CallbackInterface *cb);
  
  void stopServers();

  int getNetworkBytesCount();

protected:

  static int _refCount;

  /// Called whenever a node is connected to one of
  /// a protocol sockets.
  void onAccept(ReadableNotification* pNotifcation);

private:

  void initTCP(int port=2014);

  //Not implemented
  void initRUDP(int port=5112);

  //Not implemented
  void initHTTP(int port=5113);

  void updatePipes(bool removeAll = false);

  static TransportFactory* _instance;

  //Abstract socket reactor is not being used at this point (as such). But
  //the intend was to let reactor factory arbitrate between different type of 
  //socket based reactor that we are supporting.
  SocketReactorFactory reactorFactory;
    
  AbstractSocketReactor *tcpReactor;

  Logger &logger;

  int tcpPort;

  ServerSocket *tcpServer;

  vector <AbstractPipe*> networkPipes;

  //Total number of bytes that were sent/receieved by all the network connections
  //created or accepted by this transport.
  int bytesInOut;

  bool isTCP, isRUDP, isHTTP;
  
  //call-back notification for the new connection. As by the current design
  //we are only supporting call-back registration by the main application.
  
  CallbackInterface *connectionNotification;

};

} //namespace Net
} //namespace Tourist

#endif /*CONNECTIONFACTORY_H_*/
