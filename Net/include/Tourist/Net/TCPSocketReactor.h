#ifndef TCPSOCKETREACTOR_H_
#define TCPSOCKETREACTOR_H_

#include "Tourist/AbstractSocketReactor.h"
#include "Poco/Net/SocketReactor.h"
#include "Poco/Thread.h"
#include <iostream>

using namespace std;

using Tourist::AbstractSocketReactor;
using Poco::Net::SocketReactor;
using Poco::Thread;

namespace Tourist {
namespace Net {

class TCPSocketReactor : public AbstractSocketReactor
   /// This class encapsulates SocketReactor class from
   /// POCO. Please refer to its documentation for more
   /// detail.
{
public:
  TCPSocketReactor()
  {
  }

  void addEventHandler(const Socket& socket, const Poco::AbstractObserver& observer)
  {
    _reactor.addEventHandler(socket, observer);
  }

  void removeEventHandler(const Socket& socket, const Poco::AbstractObserver& observer)
  {
    _reactor.removeEventHandler(socket, observer);
  }

  void start()
  {
    thread.setName("TCPSocketReactor");
    thread.start(_reactor);
  }

  void stop()
  {
    _reactor.stop();
    thread.join();
  }

private:
  SocketReactor _reactor;
  //For thread pooling
  Thread thread;
};

} //namespace Net
} //namespace Tourist

#endif /*TCPSOCKETREACTOR_H_*/
