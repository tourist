#ifndef SOCKETREACTORFACTORY_H_
#define SOCKETREACTORFACTORY_H_

#include "Tourist/AbstractSocketReactor.h"

namespace Tourist {
namespace Net {

class SocketReactorFactory
   /// A factory for creating AbstractSocketReactor
{
public:
  /**
   * Creats a SocketReactorFactory
   */
  SocketReactorFactory();

   /**
    * Destroys the SocketReactorFactory
    */
  ~SocketReactorFactory();

  /**
   * Must be overriden by subclasses.
   * Creates a new socketReactor based on the given protocol.
   *
   * \param protoocl TCP, RDUP.
   */
  virtual AbstractSocketReactor* createSocketReactor(int protocol);
};

} //namespace Net
} //namespace Tourist

#endif /*SOCKETREACTORFACTORY_H_*/
