#ifndef PIPENOTIFICATIONS_H_
#define PIPENOTIFICATIONS_H_

#include "Poco/Notification.h"

using Poco::Notification;

/**
 * This class is used by the implementation of AbstractPipe (TCPPipe..) to
 * notify interested parties what kind of notiifcation was recieved (network buffer
 * arrived, connection closed).
 */
 
namespace Tourist {
namespace Net {

class PipeNotification : public Notification {

public:    
  //Initialize a notification without any type.
  PipeNotification()
  {
    type = NO_TYPE;
  }
  
  const static int NO_TYPE;
  
  const static int BUFFER_ARRIVED;
  
  const static int DISCONNECT;

  int type;  
};

const int PipeNotification::NO_TYPE = 0;
const int PipeNotification::BUFFER_ARRIVED = 1;
const int PipeNotification::DISCONNECT = 2;


//Notification that a new buffer has been received over the
//network. The pipe will not be concerned about the type of 
//the buffer.
class BufferNotification : public PipeNotification 
{
public:
  BufferNotification(char *b, int len)
  { 
    this -> buffer = new char[len];
    memcpy(this ->buffer, b, len );
    this -> len = len;    
    type = BUFFER_ARRIVED;
  }
  
  char* buffer;
  int len;
  
};
  
class DisconnectNotification : public PipeNotification 
{
public:
  DisconnectNotification()
  { 
    type = DISCONNECT;
  }  
};
  
} // namespace Net {
} // namespace Tourist

#endif /*PIPENOTIFICATIONS_H_*/
