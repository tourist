#ifndef TCPPIPE_H_
#define TCPPIPE_H_

#include "Tourist/AbstractPipe.h"
#include "Tourist/AbstractMessage.h"
#include "Tourist/AbstractSocketReactor.h"
#include "Tourist/Util/CallbackInterface.h"

#include "Poco/Net/StreamSocket.h"
#include "Poco/Logger.h"
#include "Poco/AutoPtr.h"
#include "Poco/Net/SocketNotification.h"

using namespace std;
using namespace Poco;

using Tourist::Util::CallbackInterface;

using Poco::Net::StreamSocket;
using Poco::Net::ReadableNotification;
using Poco::Net::ShutdownNotification;
using Poco::AutoPtr;

namespace Tourist {
namespace Net {

class TCPPipe : public AbstractPipe
{
public:
  /**
   * Creates a new TCP pipe.
   *
   * \param socket Client socket.
   * \param eeacotr Reactor pattern
   * \param pipeID 128-bit ID of this pipe - intended to be same as remote node.
   */
  TCPPipe(StreamSocket &socket, AbstractSocketReactor* reactor, string pipeID);

  ~TCPPipe();
  /**
   * Implementation of base class virtual method.
   */
  int sendMessage(const char* msg, const unsigned int bufferSize);
  /**
   * Implementation of base class virtual method.
   */
  int getBytesIn();
  /**
   * Implementation of base class virtual method.
   */
  int getBytesOut();
  
  string getPipeID();
  
  void setPipeID(string &id);
  
  int registerDataNotification(void *);
  
  int unregisterDataNotification(void *);
  
  int registerDisconnectNotification(void *);

  int unregisterDisconnectNotification(void *);

  void close();
  
protected:
  /**
   * Call back method indicating that socket has some data to read
   */
  void onReadable(const AutoPtr<ReadableNotification>& pNf);

  /**
   * Call back for tcp socket shutdown notification.
   *
   * Pipe is destroyed in this mehtod.
   */
  void onShutdown(const AutoPtr<ShutdownNotification>& pNf);

  bool isValid();

private:
  StreamSocket _socket;

  AbstractSocketReactor * _reactor;
  
  //Data and Disconnect call-back pointers.
  //Currently we are supporting only one registrant of the 
  //data and disconnect event. In future we might change this to use
  //more than one.
  
  CallbackInterface *dataNotification;

  CallbackInterface *disconnectNotification;
   
  //Owner of this pipe
  string pipeID;

  //Size of buffer for outway data.
  //char *buffer;

  int bytesIn, bytesOut;

  //Indicates if the handshake between a remote node has
  bool isInited;

  bool isClosed;

  Logger &logger;
};

} //namespace Net
} //namespace Tourist

#endif /*TCPPIPE_H_*/
