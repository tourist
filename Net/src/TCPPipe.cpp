/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Net/TCPPipe.h"
#include "Tourist/Net/PipeNotifications.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Util/CallbackInterface.h"
#include "Poco/NObserver.h"
#include "Poco/Observer.h"
#include "Tourist/Util/common.h"

using namespace std;

//using namespace Tourist::Util;

using Poco::Observer;
using Poco::NObserver;
using Poco::AutoPtr;
using Poco::Net::StreamSocket;
using Poco::Net::ReadableNotification;
using Poco::Net::ShutdownNotification;

namespace Tourist {
namespace Net {

TCPPipe::TCPPipe(StreamSocket &socket, AbstractSocketReactor* reactor, string _pipeID)
   :_reactor(reactor),
   _socket(socket),
   logger(Logger::get("Tourist.Net.TCPPipe")),   
   disconnectNotification(NULL), dataNotification(NULL)
{
  _reactor -> addEventHandler(_socket, NObserver<TCPPipe,ReadableNotification>(*this, &TCPPipe::onReadable));
  _reactor -> addEventHandler(_socket, NObserver<TCPPipe,ShutdownNotification>(*this, &TCPPipe::onShutdown));

  bytesIn = bytesOut = 0;
  this -> pipeID = _pipeID; 
  this -> isClosed = false;;
}

TCPPipe::~TCPPipe()
{
   debug(logger, "Destroying pipe!");
   //delete [] buffer;  
}

int TCPPipe::sendMessage(const char* sendBuff, const unsigned int bufferSize)
{
  debug(logger, "[ ID = " + this -> pipeID + " ] Buffer " + string(sendBuff));
  
  int bytesSent = -1;
  try
  {
    bytesSent = _socket.sendBytes(sendBuff, bufferSize);
    bytesOut += bytesSent;

    debug(logger, "[ ID = " + pipeID + " ] Number of bytes sent are " 
         + Util::toString(bytesSent));
  }
  catch (Poco::Exception& e)
  {
    error(logger, "[ ID = " + pipeID + " ] Failed to send message");
  }  
  return bytesSent;
}

void TCPPipe::onReadable(const AutoPtr<ReadableNotification>& pNf)
{
  int len = _socket.available();
  info(logger, "[ ID = " + pipeID + " ] Data available = " + Util::toString(len));
  char buffer[len];
  int n = _socket.receiveBytes(buffer, len);
  info(logger, "[ ID = " + pipeID + " ] Data read = "
                                      + Util::toString(len));
  
  bytesIn += n;
                                      
  if (n > 0)
  {
    BufferNotification *bnotify = new BufferNotification(buffer, len);
    if (dataNotification != NULL)
    {
      debug(logger, "[ ID = " + pipeID + " ] Sending data notification via callback");
      dataNotification -> callback(bnotify);
    }
    else
    {
      error(logger, "[ ID = " + pipeID + " ] No registered listener found for data notification");
    }  
  }
  else
  {
    //We should see if the disconnect notiifcation at this point is
    //redundent or not.
    
    if (disconnectNotification != NULL)
    {
      DisconnectNotification *disconnect = new DisconnectNotification();      
      debug(logger, "[ ID = " + pipeID + " ] Disconnection notification in 'onReadable'");
      disconnectNotification -> callback(disconnect);
    }
    else
    {
      info(logger, "[ ID = " + pipeID + " ] No callback listener for disconnect notification on this pipe");
    }
    debug(logger, "[ ID = " + pipeID + " ] Pipe destoryed for node ");
    
    //We would like to move the pipe clean up to top level component
    //delete this;
    close();
  }
}

void TCPPipe::onShutdown(const AutoPtr<ShutdownNotification>& pNf)
{
  info(logger, "[ ID = " + pipeID + " ] Shutdown notification received for node ");
  if (disconnectNotification != NULL)
  {
    DisconnectNotification disconnect;      
    debug(logger, "[ ID = " + pipeID + " ] Disconnection notification in 'onShutdown'");
    disconnectNotification -> callback(&disconnect);
  }
  else
  {
    info(logger, "[ ID = " + pipeID + " ] No callback listener for disconnect notification on this pipe");
  }      
             
  close();  
  //delete this;
}

int TCPPipe::getBytesIn()
{
  return bytesIn;
}

int TCPPipe::getBytesOut()
{
  return bytesOut;
}

string TCPPipe::getPipeID()
{
  return this -> pipeID;
}

void TCPPipe::setPipeID(string &id)
{
  this -> pipeID = id;
}

int TCPPipe::registerDataNotification(void *callback)
{
  if (callback == NULL) 
  {
    error(logger, "[ ID = " + pipeID + " ] Recieved a null pointer in data-callback registration");
    return -1;
  }
   
  //TODO: Should better handle cast  
  this -> dataNotification = (CallbackInterface*) callback;
  debug(logger, "[ ID = " + pipeID + " ] Registered callback for data notifications");    

  return 0;
}
  
int TCPPipe::registerDisconnectNotification(void *callback)
{
  if (callback == NULL)
  {
    error(logger, "[ ID = " + pipeID + " ] Recieved a null pointer in disconnect-callback registration");
    return -1;
  }
  debug(logger, "[ ID = " + pipeID + " ] Registered callback for disconnect notifications");
  this -> disconnectNotification = (CallbackInterface*) callback;

  return 0;
}

int TCPPipe::unregisterDataNotification(void *callback)
{
  if (this -> dataNotification == NULL)
    return -1;
  
  if (this -> dataNotification == (CallbackInterface*) callback)
  {
    this -> dataNotification = NULL;
    return 0;
  }

  return -1;
}

int TCPPipe::unregisterDisconnectNotification(void *callback)
{
  if (this -> disconnectNotification == NULL)
    return -1;
  
  if (this -> disconnectNotification == (CallbackInterface*) callback)
  {
    this -> disconnectNotification = NULL;
    return 0;
  }

  return -1;

}

void TCPPipe::close()
{
  this -> isClosed = true;

  _reactor->removeEventHandler(_socket, NObserver<TCPPipe,
                            ReadableNotification>(*this, &TCPPipe::onReadable));
  _reactor->removeEventHandler(_socket, NObserver<TCPPipe,
                            ShutdownNotification>(*this, &TCPPipe::onShutdown));

  //_socket.close();  

  debug(logger, "Pipe is now closed!");
}

bool TCPPipe::isValid()
{
  return !(this -> isClosed);
}

} //namespace Net
} //namespace Tourist
