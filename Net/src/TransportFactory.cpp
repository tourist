/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Constants.h"
#include "Tourist/TouristExceptions.h"
#include "Tourist/Net/TransportFactory.h"
#include "Tourist/Net/TCPPipe.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Util/Config.h"
#include "Poco/Timespan.h"
#include "Poco/Net/StreamSocket.h"

//using namespace Tourist::Util;

using Tourist::Util::Config;
using Tourist::AlreadyInitializedException;
using Tourist::InvalidConfigurationException;

using Poco::Timespan;
using Poco::Net::StreamSocket;
using Poco::Net::DNS;
using Poco::Net::HostEntry;

namespace Tourist {
namespace Net {

//int TransportFactory::_refCount = 0;

//TransportFactory* TransportFactory::_instance = 0;

TransportFactory::TransportFactory() :
     logger(Logger::get("Tourist.Net.TransportFactory"))
{
  info(logger, "TransportFactory created");
  isTCP = false;
  isRUDP = false;
  isHTTP = false;

  tcpReactor = NULL;
  tcpServer = NULL;
}

TransportFactory::~TransportFactory()
{
  info(logger, "TransportFactory destroyed");

  if (tcpReactor)
    tcpReactor->removeEventHandler(*tcpServer, Poco::Observer<TransportFactory, 
            ReadableNotification>(*this, &TransportFactory::onAccept));

  //let's remove all the connection pipes
  updatePipes(true); 

  if (tcpReactor)
    delete tcpReactor;
    
  if (tcpServer)
    delete tcpServer;
    


  //stopServers();

}

/*TransportFactory* TransportFactory::getInstance()
{
  if (_instance == 0)
  {
    _instance = new TransportFactory();
  }
  _refCount++;
  return _instance;
}

void TransportFactory::release()
{
  if (--_refCount < 1)
    destroy();
}

void TransportFactory::destroy()
{
  if (_instance != 0)
  {
    delete(_instance);
    _instance = 0;
  }
}*/

void TransportFactory::initTCP(int port)
{
  info(logger, "Initializing TCP server");


  //This check will probably shutdown the application. Which is not
  //required. Just put it here to avoid any re-calls to this method in the
  //code should remove this.
  //TODO Remove this check.
  if (isTCP)
    throw AlreadyInitializedException("TCP protocol is already initialized");

  try
  {
    debug(logger, "TCP Server port = " + Util::toString(port));
    tcpServer = new ServerSocket(port);
    tcpReactor = reactorFactory.createSocketReactor(TCP);
    debug(logger, "Adding call back notifications");
    tcpReactor->addEventHandler(*tcpServer, Poco::Observer<TransportFactory, 
            ReadableNotification>(*this, &TransportFactory::onAccept));
    tcpReactor->start();
    tcpPort = port;
    isTCP = true;
    info(logger, "TCP server started");
  }
  catch (Poco::Exception& e)
  {
    string emsg = e.what();
    fatal(logger, "TCP server initialization faile due to " + emsg);
    throw e;
  }
}

void TransportFactory::initRUDP(int port)
{
  info(logger, "Initializing RUDP server");

  //This check will probably shutdown the application. Which is not
  //required. Just put it here to avoid any re-calls to this method in the
  //code should remove this.
  /*if (isRUDP)
    throw AlreadyInitializedException("RUDP  protocol is already intialized");

  try
  {
    debug(logger, "RUDP server port = " + Util::toString(port));
    //TODO fix the interface address. It shouldn't be localhost.
    host = new Host(&NetAddress("localhost", port), 0);
    rudpReactor = ReliableUDPReactor::getInstance();
    rudpReactor -> start();

  }
  catch (Poco::Exception& e)
  {

  }*/
}

void TransportFactory::initHTTP(int port)
{
  throw Exception("Not Implemented!");
}


void TransportFactory::onAccept(ReadableNotification *notification)
{
  //This method is called as soon as we have a new connection.
  //Once we have a connection request we creats a stream socket (or call it
  //client socket object) and creates a new pipe, a new remote node with null
  //id
  notification->release();

  info (logger, "New connection");
  AbstractPipe * pipe = NULL;
  StreamSocket sock;
  
  try
  {
    sock = tcpServer->acceptConnection();
    SocketAddress address = sock.peerAddress();
    info(logger, "Connection info host = " + address.host().toString() + " port = "
                       + Util::toString(address.port()));

    debug(logger, "Creating a new pipe");
    pipe = new TCPPipe(sock, tcpReactor, NULL_ID);    
   
    //Transport factory takes the ownership of the network pipe.
    //and ensure that memory allocated to pipe is de-allocated.
    networkPipes.push_back(pipe);
    //rn.setPipe(pipe);
  }
  catch (Poco::Exception& e)
  {
    error(logger, "Failed to accept new connection due to " + e.message());
    if (pipe != NULL)
      delete pipe;
  }

  //if (pipe != NULL)
  //  updatePipesCache(rn, pipe);
    
  if (connectionNotification != NULL)
  {
    debug(logger, "Notifying application via callback for new connection arrival");
    connectionNotification -> callback(pipe);
  } 
  else
  {
    debug(logger, "No connection callback registered!");
    
    //TODO Don't create pipe at first place; if we know there isn't any listener
    sock.close();
    delete pipe;
  }

  updatePipes();
}

AbstractPipe* TransportFactory::getPipe(SocketAddress &addr, int protocol)
{
  if (protocol != TCP)
    throw InvalidArgumentException("Currently only TCP is supported");

  info(logger, "Pipe request for host " + addr.host().toString() + ":" 
          + Util::toString(addr.port()) + " protocol " + Util::toString(protocol));

  TCPPipe *pipe = NULL;
  try
  {
    Timespan timeout(0, 500);
    
    //client socket - ownership is taken by pipe object.
    StreamSocket sock;
    sock.connect(addr, timeout);
    sock.setBlocking(false);
    pipe = new TCPPipe(sock, tcpReactor, NULL_ID);
    updatePipes(pipe);
    info(logger, "Pipe successfully created");
  }
  catch (Poco::Exception e)
  {
    error(logger, "Pipe creation failed for " + addr.host().toString() + ":" 
        + Util::toString(addr.port())+ " protocol " + Util::toString(protocol));

    return NULL;
  }

  //updatePipesCache(rn, pipe);
  return pipe;
}

void TransportFactory::initServers(Config &config)
{
  if (config.getTCPEnable())
  {
    int port = config.getTCPPort();
    try
    {
      initTCP(port);
    }
    catch (Poco::Exception& e)
    {
      error(logger, "TCP server failed...Shutting down!");
      throw(e);
    }
  }
  else
  {
    throw InvalidConfigurationException("Only supported protocol TCP is missing in configuration");
  }

  //similarly for UDP/RUDP etc

  //TODO: Enable it when the current issues with the RUDP are resolved.
  /*if (config -> getRUDPEnable())
  {
    int port = config->getRUDPPort();
    try
    {
      initRUDP(port);
    }
    catch (Poco::Exception& e)
    {
      error(logger, "RUDP server failed.. shutting down!");
      throw(e);
    }
  }*/
}

void TransportFactory::stopServers()
{
  if (isTCP)
  {
    //debug(logger, "Stopping TCP server");
    tcpReactor -> stop();
    tcpServer -> close();

   //debug(logger, "Stoped Successfuly");
    
    //We want ot make sure that this delete block is not called more than once.
    isTCP = false;
  }
}

bool TransportFactory::getTCP()
{
  return isTCP;
}

int TransportFactory::getTCPPort()
{
  return tcpPort;
}

string TransportFactory::getHostName()
{
  HostEntry entry = DNS::thisHost();
  return entry.name();
}

/*int TransportFactory::getBWUsage()
{
  int count = 0;
  int dataRate = 0;

  map<RemoteNode, AbstractPipe*>::iterator iter;
  for (iter = networkPipes.begin(); iter != networkPipes.end(); iter++)
  {
    AbstractPipe *p = iter->second;
    RemoteNode rn = iter->first;

    if (p == NULL)
    {
      info(logger, "NUll pipe found for " + rn.Util::toString());
      continue;
    }
    count += p->getBytesIn();
    count += p->getBytesOut();
    debug(logger, rn.Util::toString() + " bytesIn = " + Util::toString(p->getBytesIn()) +
                             " bytesOut = " + Util::toString(p->getBytesOut()));
  }
  time_t curr = time(NULL);
  int seconds = curr - startTime; //time passed

  if (seconds != 0 && count != 0)
    dataRate = count/seconds;

  debug(logger, "BW = " + Util::toString(count) + "/" + Util::toString(seconds) + " = " + Util::toString(dataRate));

  return dataRate;
}*/

/*void TransportFactory::updatePipesCache(RemoteNode& rn, AbstractPipe* pipe)
{
  info(logger, "Adding new pipe to list of existing pipes, old size = "
        + Util::toString(networkPipes.size()) +  " new size = " + Util::toString(networkPipes.size()));

  networkPipes.insert(make_pair(rn, pipe));

  if (logger.getLevel() == Message::PRIO_DEBUG)
  {
    map<RemoteNode, AbstractPipe*>::iterator iter;
    int count = 0;
    debug(logger, "Network pipes:");
    for (iter = networkPipes.begin(); iter != networkPipes.end(); iter++)
    {
      RemoteNode n = iter->first;
      AbstractPipe *p = iter->second;
      debug(logger, Util::toString(count) + "-" + n.Util::toString());
      if (p == NULL)
        debug(logger, "pipe == NULL");

      count++;
    }
  }
}*/

int TransportFactory::registerConnectionNotification(CallbackInterface *notification)
{
  if (notification == NULL) 
  {
    error(logger, "Callback interface is null for new connection interface. Tourist may not function properly!");
    return -1;    
  }
  
  connectionNotification = notification;
  
  return 0;  
}

void TransportFactory::updatePipes(bool removeAll)
{
  //cleanup the pipes may be move it to a separate thread
  //TODO:Check if it is suitable to move it to a separate thread
  vector<AbstractPipe*>::iterator it = networkPipes.begin();
  int totalPipes = networkPipes.size();
  debug(logger, "Total network pipes " + Util::toString(totalPipes));
  while (it < networkPipes.end())
  {
    AbstractPipe *pipe = *it;
    if (pipe == NULL)
      continue;

    bytesInOut = pipe -> getBytesIn() + pipe -> getBytesOut();
    debug(logger, "Calculated bytesIn/bytesOut " + Util::toString(bytesInOut));

    if (!pipe -> isValid() || removeAll)
    {
      networkPipes.erase(it);
      delete pipe;
    }
    it++;
  }
  debug(logger, "Pipes de-allocated " + Tourist::Util::toString((totalPipes - networkPipes.size())));
}

int TransportFactory::getNetworkBytesCount()
{
  return bytesInOut;
}

} //namespace Net
} //namespace Tourist
