/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestTCPPipe.h"
#include "Tourist/TouristExceptions.h"
#include "Tourist/Id.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/AbstractMessage.h"
#include "Tourist/Net/TCPPipe.h"
#include "Tourist/Net/PipeNotifications.h"
#include "Tourist/Util/Config.h"
#include "Tourist/Util/Util.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"
#include "Poco/Thread.h"
#include "Poco/Net/SocketAddress.h"

using Tourist::Message::RemoteNode;
using Tourist::Id;
using Tourist::AbstractMessage;
using Tourist::InvalidArgumentException;
using Tourist::Net::TCPPipe;
using Tourist::Net::PipeNotification;
using Tourist::Net::BufferNotification;
using Tourist::Message::MessageDispatcher;
using Tourist::Util::Config;

using Poco::NObserver;
using Poco::Observer;
using Poco::Thread;
using Poco::Net::SocketAddress;

//using namespace Tourist::Util;

namespace
{

class TransportCallback : public CallbackInterface
{
public:

TransportCallback() : isNotified(false), pipe(NULL) {}

void callback(void *_pipe) 
{
  isNotified = true;
  
  if (_pipe != NULL)    
    pipe = (AbstractPipe*) _pipe;   
}

AbstractPipe *pipe;

//Set to true if the callback is called at least once.
//Used to see if our notification are being passed correctly.
bool isNotified;  
};

class PipeCallback : public CallbackInterface
{
public:

PipeCallback() : isDisconnected(false), buffer(NULL), len(-1), type(0)
{}

void callback(void* data)
{
  PipeNotification *pipeC = (PipeNotification*) data;
  if (pipeC -> type == PipeNotification::BUFFER_ARRIVED)
  {
    BufferNotification *buffN = (BufferNotification*) pipeC;
    
    this -> buffer = new char[buffN -> len + 1];
    this -> buffer[buffN -> len] = '\0';    
    //TODO: check for buffN -> buffer being NULL;
    memcpy(this -> buffer, buffN -> buffer, buffN -> len);
    this -> len = strlen(this -> buffer);    
      
  } 
  else if (pipeC -> type == PipeNotification::DISCONNECT)
  {
    isDisconnected == true;
  }
}

bool isDisconnected;
int type; //type of notification (data or disconnect)
int len;   //size of buffer
char* buffer;   //buffer
};

/*class TestConnection: public TCPServerConnection
{
public:
  TestConnection(const StreamSocket&s) : TCPServerConnection(s)
  {
  }

  void run()
  {
    StreamSocket& ss = socket();
    try
    {
      const int len = ss.available();
      char buffer[len];
      int n = ss.receiveBytes(buffer,  sizeof(buffer));
      while (n > 0)
      {
      }
    }
    catch (Exception &e)
    {
      throw e;
    }
  }

}; */// class TestConnection.

}//namespace

TestTCPPipe::TestTCPPipe(const std::string& name): CppUnit::TestCase(name)
{
}

TestTCPPipe::~TestTCPPipe()
{
}

void TestTCPPipe::setUp()
{  
}

void TestTCPPipe::tearDown()
{ 
}

void TestTCPPipe::testPipeCreation()
{
  //Start the trasnport-1 TCP
  TransportFactory transport1;
  Config config1;
  try 
  {
    config1.setTCPEnable(true);
    config1.setTCPPort(5000);
    
    transport1.initServers(config1);    
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  }
  
  assert(transport1.getTCP() == true);
  assert(transport1.getTCPPort() == 5000);
  
  //Start the trasnport-2 TCP
  TransportFactory transport2;
  Config config2;
  try 
  {
    config2.setTCPEnable(true);
    config2.setTCPPort(5001);
    
    transport2.initServers(config2);    
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  }
  
  assert(transport1.getTCP() == true);
  assert(transport2.getTCPPort() == 5001);
  
  TransportCallback callbackFrom1, callbackFrom2;
  
  assert(callbackFrom1.isNotified == false);
  assert(callbackFrom2.isNotified == false);
  
  transport1.registerConnectionNotification(&callbackFrom1);  
  transport2.registerConnectionNotification(&callbackFrom2);
  
  try
  {
    Id id;
    //RemoteNode rn(id.hexstring(), 0, transport2.getHostName(), transport2.getTCPPort() ); 
    SocketAddress address(transport2.getHostName(), transport2.getTCPPort());
    AbstractPipe *remotePipeFrom1 = transport1.getPipe(address, TCP);
    
    //created pipe shouldn't be null
    assertNotNull(remotePipeFrom1);
    
  }
  catch (Poco::Exception &e)
  {    
    fail(e.what());    
  }  
  
  //transport2 should indicate that there is a new connection by invoking
  //callback. 
  assert(callbackFrom2.isNotified == false);  
}

void TestTCPPipe::testTwoWayMessage()
{
  //Start the trasnport-1 TCP
  TransportFactory transport1;
  Config config1;
  try 
  {
    config1.setTCPEnable(true);
    config1.setTCPPort(5000);
    
    transport1.initServers(config1);    
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  }
  
  assert(transport1.getTCP() == true);
  assert(transport1.getTCPPort() == 5000);
  
  //Start the trasnport-2 TCP
  TransportFactory transport2;
  Config config2;
  try 
  {
    config2.setTCPEnable(true);
    config2.setTCPPort(5001);
    
    transport2.initServers(config2);    
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  }
  
  assert(transport1.getTCP() == true);
  assert(transport2.getTCPPort() == 5001);
  
  TransportCallback callbackFrom1, callbackFrom2;
  
  assert(callbackFrom1.isNotified == false);
  assert(callbackFrom2.isNotified == false);
  
  transport1.registerConnectionNotification(&callbackFrom1);  
  transport2.registerConnectionNotification(&callbackFrom2);  
  
  //These represent pipes which we will get from the transport
  //one directly and the other via a callback as transport-1 cnonects
  //with the transport-2 
  AbstractPipe *remotePipeFrom1 = NULL, *remotePipeFrom2 = NULL;
  
  try
  {        
    SocketAddress address(transport2.getHostName(), transport2.getTCPPort());
    remotePipeFrom1 = transport1.getPipe(address, TCP);
    
    Thread::sleep(100);
    remotePipeFrom2 = callbackFrom2.pipe;
    
    //created pipe shouldn't be null
    assert(remotePipeFrom1 != NULL);
    assert(remotePipeFrom2 != NULL);
    //type cast
    assert(dynamic_cast<TCPPipe*> (remotePipeFrom1) != 0);
    assert(dynamic_cast<TCPPipe*> (remotePipeFrom2) != 0);
  }
  catch (Poco::Exception &e)
  {    
    fail(e.what());    
  }
  
  //If we got this far it should means that virtual pipe connection
  //is established and we are ready to send data, but first lets register
  //with pipe for data/disconnect notification.
  
  PipeCallback pipeNotify1, pipeNotify2;
  
  remotePipeFrom1 -> registerDataNotification(&pipeNotify1);  
  remotePipeFrom1 -> registerDisconnectNotification(&pipeNotify1);
  
  remotePipeFrom2 -> registerDataNotification(&pipeNotify2);  
  remotePipeFrom2 -> registerDisconnectNotification(&pipeNotify2);
  
  //Sending data from pipe1 to pipe2
  char *testData = "Hello from pipe1";
  remotePipeFrom1 -> sendMessage(testData, strlen(testData));
  
  Thread::sleep(200);
  
  assert(pipeNotify2.buffer != NULL);  
  assert(strcmp(pipeNotify2.buffer, testData) == 0);
    
  //Sending data from pipe2 to pipe1
  char *testData2 = "Heloow from pipe2";
  remotePipeFrom2 -> sendMessage(testData2, strlen(testData2));
  Thread::sleep(200);
  
  assert(pipeNotify1.buffer != NULL);
  assert(strcmp(pipeNotify1.buffer, testData2) == 0); 
    
}

/*void TestTCPPipe::testMsgReceive()
{
  /*Id id;
  RemoteNode thisNode(id.hexstring(), 0, "localhost", transport->getTCPPort());
  try
  {
    MessageDispatcher* dispatcher = MessageDispatcher::getInstance();
    dispatcher-> registerNotification("receiveTest",
        NObserver<TestTCPPipe, MessageNotification>
        (*this, &TestTCPPipe::onReceive));

    Timespan timeout(0, 500);
    SocketAddress add("localhost", transport->getTCPPort());
    //client socket.
    StreamSocket sock;
    sock.connect(add, timeout);
    sock.setBlocking(true);

    //Creating message
    hessian_output ho;
    string rpc = ho.start_call("receiveTest");
    rpc = getNodeHeader(ho, rpc, thisNode);
    rpc = ho.set_parameter(rpc, new String("TestMessage"));
    bstractMessage *testMsg = new HessianMessage("receiveTest", rpc);

    const char *buffer = testMsg -> getString().c_str();
    const unsigned int len = testMsg -> getString().length();
    sock.sendBytes(buffer, len);

    //TODO Use wake up call.
    Thread::sleep(1000);
    assert(msgReceived == true);
  }
  catch (Poco::Exception &e)
  {
    fail(e.message());
  }
}*/

/*void TestTCPPipe::onReceive(const AutoPtr<MessageNotification>& notification)
{
  /*msgReceived = true;
  try
  {
    HessianMessage *hessian = (HessianMessage*) notification->message;
    hessian_input *hi = hessian->getMessage();
    string msg = hi->read_string(hi->get_tag());
    assert (msg == "receiveTest");
  }
  catch(Poco::Exception &e)
  {
    fail(e.message());
}*/

CppUnit::Test* TestTCPPipe::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestTCPPipe");
  CppUnit_addTest(pSuite, TestTCPPipe, testPipeCreation);
  CppUnit_addTest(pSuite, TestTCPPipe, testTwoWayMessage); 

  return pSuite;
}
