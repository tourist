/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestTransportFactory.h"
#include "Tourist/AbstractPipe.h"
#include "Tourist/Net/TransportFactory.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Thread.h"

using Tourist::AbstractPipe;
using Tourist::Net::TransportFactory;
using Poco::Thread;
using Poco::Net::StreamSocket;


TestTransportFactory::TestTransportFactory(const std::string& name): CppUnit::TestCase(name),
            callbackedPipe(NULL)
{
}

TestTransportFactory::~TestTransportFactory()   
{
}

void TestTransportFactory::setUp()
{ 
}

void TestTransportFactory::tearDown()
{
}

void TestTransportFactory::testTCPServer()
{
  /*
   * Initialize the TCP server and connect any arbitrary socket to make sure 
   * that server is started successfully. Then we will break the connection and
   * will make the same connection with it to ensure that it is shut down.
   */ 
  TransportFactory transport;
  if (callbackedPipe != NULL)
  {
     free(callbackedPipe);
     callbackedPipe = NULL;
  }
    
  connectionCallbacked = false;
   
  try 
  {
    Config config;
    config.setTCPEnable(true);
    config.setTCPPort(5000);
    
    transport.initServers(config);
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  } 
  
  assert(transport.getTCP() == true);
  assert(transport.getTCPPort() == 5000);
  
  transport.registerConnectionNotification((CallbackInterface*)this);
  int conn = testConnect(transport.getHostName(), transport.getTCPPort());
  
  /*TODO: We need to wait for a short while before checking on the status of the connection
   * There may be a neat way of doing it like waiting on a object
   */
  Thread::sleep(100);  
  assert (conn == 0);
  
  assert(connectionCallbacked == true);
  //assertNotNullPtr(callbackedPipe);
  
  transport.stopServers();    
}

void TestTransportFactory::callback(void *pipe)
{
  //A successful test means that we will recieve a notification that our
  //dmmy connection from method 'testConect' is connected to our transport
  
  /*if (pipe == NULL)
  {
    fail("Recieved pipe from transport was null");
    return;
  }*/
  
  connectionCallbacked = true;
  //callbackedPipe = (AbstractPipe*) pipe;    
}

int TestTransportFactory::testConnect(string host, int port)
{
  try 
  {
    StreamSocket ss;
    ss.connectNB(SocketAddress(host, port)); 
    ss.shutdown(); 
  } 
  catch (Poco::Exception&e)
  {
    return -1;
  }
  
  return 0;
}

CppUnit::Test* TestTransportFactory::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestTCPPipe");
  CppUnit_addTest(pSuite, TestTransportFactory, testTCPServer);  

  return pSuite;
}
