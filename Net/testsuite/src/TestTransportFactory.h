#ifndef TESTTRANSPORTFACTORY_H_
#define TESTTRANSPORTFACTORY_H_

#include "Tourist/Net/TransportFactory.h"
#include "Tourist/Util/CallbackInterface.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"

using Tourist::Net::TransportFactory;
using Tourist::Util::CallbackInterface;

class AbstractPipe;

/**
 * What: 
 *   a) Test the creation of network servers (all available protocol: TCP).
 * How: 
 *  a) A test connection is established to created server to see if we are notified
 *   by the same connection via registered callback connection.
 * 
 */

class TestTransportFactory: public CppUnit::TestCase, public CallbackInterface
{
public:
  TestTransportFactory(const std::string& name);
  
  ~TestTransportFactory();

  void setUp();

  void tearDown();

  void testTCPServer();
  
  static CppUnit::Test* suite();

private:

  TransportFactory transport;
  
  int testConnect(string host, int port);
  
  bool connectionCallbacked;
  
  AbstractPipe *callbackedPipe;
  
  void callback(void *);
  
};
  
#endif /*TESTTRANSPORTFACTORY_H_*/
