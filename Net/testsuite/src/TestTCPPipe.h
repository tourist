#ifndef TESTTCPPIPE_H_
#define TESTTCPPIPE_H_

#include "CppUnit/TestCase.h"
#include "Tourist/Net/TransportFactory.h"
#include "Tourist/Message/MessageDispatcher.h"

using Tourist::Net::TransportFactory;
using Tourist::Message::MessageNotification;

/**
 * What:
 *  a) Two-way Pipe creation 
 *  b) Two-way communication sending our pipe
 *  c) Callback for data and disconnection.
 *  d) Sending a large amount of data.
 *  
 * How:
 *  a) Initiate transport-1, transport-2, Get pipe from 1 and see if we recieve
 *    call back from the 2.
 *  b) Initiate transport-1, transport-2, Get pipe from 1 (register data/disconnection
 *     callback and as we get conneciton callback at transport-2 we register
 *     data/disconnect call-backs. Send data using pipe-1 and see if get it at pipe2.
 *  c) Above test will test both b&c.
 *  d) TODO.
 */

class TestTCPPipe: public CppUnit::TestCase
{
public:
  TestTCPPipe(const std::string& name);
  ~TestTCPPipe();

  void setUp();

  void tearDown();

  /**
   * Test for (a) - See class description.
   */
  void testPipeCreation();
  
  /**
   *  Test for (b) and (c). See class description.
   */
  void testTwoWayMessage();

  static CppUnit::Test* suite();

private:  


};

#endif /*TESTTCPPIPE_H_*/
