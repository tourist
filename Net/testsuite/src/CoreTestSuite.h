#ifndef CORETESTSUITE_H_
#define CORETESTSUITE_H_

#include "CppUnit/TestSuite.h"

class CoreTestSuite
{
public:
  static CppUnit::Test* suite();
};

#endif /*CORETESTSUITE_H_*/
