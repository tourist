/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/**
 * Main class for testing different components
*/

#include "Tourist/App/Application.h"
#include "Tourist/App/PeerConnectionWorker.h"
#include "Tourist/Id.h"
#include "Tourist/Node.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/PeerNotification.h"
#include "Tourist/App/Bootstrap.h"
#include "Tourist/App/Bootstrapper.h"
#include "Tourist/AvlCache.h"
#include "Tourist/App/CacheXfer.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Util/Config.h"
#include "Tourist/Net/TCPPipe.h"

#include "Poco/Thread.h"

using Tourist::Id;
using Tourist::App::PeerConnectionWorker;
using Tourist::Message::RemoteNode;
using Tourist::Message::PeerNotification;
using Tourist::Node;
using Tourist::App::CacheXfer;
using Tourist::Util::Config;
using Tourist::Net::TCPPipe;

using Poco::Thread;

using namespace Tourist::Util; 

namespace Tourist {
namespace App {

Application::Application() : logger(Logger::get("Tourist.App.Application")),
  threadPool("Application"), nodeCache(5), cacheXfer(&this -> config)
{ 
  this -> shouldBeRunning = true;
  bootstrap = NULL;
  bootstrapper = NULL;

  startTime = time(NULL);
}

Application::Application(Config conf) : logger(Logger::get("Tourist.App.Application")),
  threadPool("Application"), cacheXfer(&this -> config)
{
  this -> shouldBeRunning = true;
  this -> config = conf;
  bootstrap = NULL;
  bootstrapper = NULL;

  startTime = time(NULL);
}

Application::~Application()
{
  debug(logger, "Destroying application!");
  transport.stopServers();
  
  if (bootstrap != NULL)
    delete bootstrap;

  if (bootstrapper != NULL)
    delete bootstrapper;
}

int Application::init()
{
  this -> bootstrap = new Bootstrap(this);
  this -> bootstrapper = new Bootstrapper(this);

  info(logger, "Initializing network servers");
  try
  {
    transport.initServers(config);
  }
  catch (Poco::Exception &e)
  {
    string emsg = e.what();
    error(logger, "Network initialization failed!" + emsg);    
    return -1;
  }  
  
  int stat = transport.registerConnectionNotification((CallbackInterface*) this);
  if (stat != 0)
  {
    error(logger, "Couldn't register network connection callback");
    return -1;
  } 
  
  //below two objects will be used by CacheXfer.
  this -> config.setMessageBus(&messageBus);
  this -> config.setMessageTypeHandler(&messageTypeData);
  this -> config.setLocalNode(&nodeCache);
  //Start the cacheXfer deamon
  this -> cacheRun.start(cacheXfer);

  //check boot role and run bootstrap or bootstrapper accordingly.
  int bootRole = this -> config.getBootRole();
  if (bootRole == 1)
  {    
    //run the Bootstrapper
    this -> bootRun.start(*bootstrapper);

  }
  else if (bootRole == 2 || bootRole == 3) 
  {    
    this -> bootRun.start(*bootstrap);
  }

  return 0;
}

void Application::run()
{   
  debug(logger, "Application running now!");

  while (this -> shouldBeRunning)
  {    
    //dispatch thread for handling peer handshake.
    if ( (queue.size() > 0) &&  (threadPool.available() > 0))  
    {
       PeerNotification *notification = NULL;
       notification = dynamic_cast<PeerNotification*>(queue.dequeueNotification());
       if (notification != NULL && notification -> pipe != NULL)
       {     
          //TODO: It may be required to send multiple remote node object depending on the 
	  //support protocols.      
	  RemoteNode response(NULL, this -> nodeCache.getID(), this -> nodeCache.getLevel(), 
	       transport.getHostName(), transport.getTCPPort());  
	  response.setBwUsage(this -> getBwUsage());

          debug(logger, "Id of this node is " + nodeCache.getID());

          AutoPtr<PeerConnectionWorker> worker(new PeerConnectionWorker(this, notification -> pipe, response, false, 5)); 
	  workers.push_back(worker);
          worker -> duplicate();
	  threadPool.start(*worker);
       }
    }

    //check if the boot role has been taken care of
    if (this -> config.getBootRole() == 2)
    {
      //TODO
      //if boot was successful and bootstrap isn't running
      //run the bootstrapper
    }
    
    Thread::sleep(1000);

    //cleanup worker
    debug(logger, "Cleaning-up the worker threads for peer connection");
    vector< AutoPtr<PeerConnectionWorker> >::iterator it = workers.begin();
    int totalWorkers = workers.size();
    while (it < workers.end())
    {
      PeerConnectionWorker *w = *it;
      if (w == NULL)
      {
        continue;
      }
    
      if (w -> isStopped())
      {
        workers.erase(it);
        w -> release();
      }      
      it++;
    }
    debug(logger, "Total workers = " + toString(totalWorkers) + " removed = " + toString(totalWorkers - workers.size()));
  }
}

void Application::callback(void *data)
{
  AbstractPipe *pipe = (AbstractPipe*) data;  
  if (pipe == NULL)
  {
    error(logger, "Rejected pipe in call back of peer connection");
    return;
  }  

  info(logger, "Notification for new pipe received, enqueueing for processing!");
  
  PeerNotification *notification = new PeerNotification(pipe);
  queue.enqueueNotification(notification);
}

bool Application::getConnection(RemoteNode rnode, RemoteNode **cacheNode)
{
  debug(logger, "Application::getConnection() " + rnode.toString());
  Node *temp;
  bool status = this -> nodeCache.findId(rnode, &temp);

  *cacheNode = (temp == NULL) ? NULL : (RemoteNode*) temp;

  if (status && (*cacheNode != NULL && (*cacheNode) -> getPipe() != NULL))
  {
    //TODO:: Why I need to put braces for this macro
    info(logger, "Found existing connection for " + rnode.toString());
  }
  else
    status = (connectionTo(rnode.getHost(), rnode.getPort(), TCP, cacheNode, 50) == 0) 
              ? true : false ;

  return status;
}

int Application::connectionTo(string host, int port, int protocol,
    RemoteNode **rn, int timeout)
{
  int status = -1;

  debug(logger, "Peer Connection request for host =" + host + ":" 
            + toString(port) + " protocol = " + toString(protocol));

  if (threadPool.available() == 0)
  {
    return -1;       
  }

  //TODO:Check it against some dynamic list then a fixed list
  if (protocol != TCP)
  {
    error(logger, "Requested protocol not in allowed list");
    return -1;
  }

  //TODO:Transport layer errors

  time_t start = time(NULL); 
  SocketAddress add(host, port);
  AbstractPipe *newPipe = transport.getPipe(add, protocol);  
  
  if (newPipe == NULL)
  {
    error(logger, "Unable to connect to host " + host + ":" + toString(port));
    return -1;
  }

  time_t now = time(NULL);
  if ((now - start) > timeout)
  {
    error(logger, "Peer connected after timeout was expired");
    debug(logger, "Given timeout value = " + toString(timeout) + " now = " + toString(now));        
    newPipe -> close();
    delete newPipe;
    return -1; 
  }  

  RemoteNode response(NULL, this -> nodeCache.getID(), this -> nodeCache.getLevel(), 
                transport.getHostName(), transport.getTCPPort());  

  debug(logger, "Id of this node is " + nodeCache.getID());
    
  PeerConnectionWorker worker(this, newPipe, response, true, timeout);
  threadPool.start(worker);

  while (!worker.isStopped())
    Thread::sleep(500);
 
  RemoteNode *result = worker.getResult();
  if (worker.isStatusOK() and result != NULL)
  {
    *rn = result; 
    (*rn) -> setConfig(&this -> config);
    status = 0; 
  }
  else
    rn =  NULL;
  
  //fprintf(stderr, "Status %d\n", status);
  //fprintf(stderr, "Node port %d\n", result -> getPort());
  return status;
}

int Application::getRemoteCache(RemoteNode *contactNode, int cacheType, int startLevel,
      int endLevel, int timeout, NodeSet &result)
{
  return cacheXfer.getRemoteCache(contactNode, cacheType, startLevel, endLevel, timeout, result);
}

void Application::addNewNode(RemoteNode *node)
{
  info(logger, "Caching a node " + node -> toString());
  //add it to the local node.
  node -> setConfig(&this -> config);
  int status = this -> nodeCache.addRemote(node);
  info (logger, "Node addded " + toString(status));
}

void Application::getLocalNodeInfo(RemoteNode &rn)
{
  rn.setHost(this -> transport.getHostName()); 
  rn.setPort(this -> transport.getTCPPort());
  rn.setProtocol(TCP);
  rn.setID(this -> nodeCache.getID());
  rn.setBwUsage(this -> getBwUsage());
}

int Application::getNodeCount(int level, unsigned int flags)
{
  return this -> nodeCache.getNodeCount(level, flags);
}

bool Application::getTopNode(Node &query, Node **result, int type)
{
  return this -> nodeCache.getTopNode(query, result, type);
}

bool Application::getForwardingNode(Node &query, Node **result, int type)
{
  bool status;
  NodeSet nset;
  if (type == PREFIX) 
    status = this -> nodeCache.searchRemoteLinksViaSuffix(query, nset, HIGHER_LEVELS|EQUAL_LEVELS);
  else
    status = false;

  if (status) {
    NodeSet::Iter iter = nset.first();
    *result = (iter) ? iter -> key : NULL;
  }

  return status;
}

vector<TransportInfo> Application::getEnabledTransports()
{
  vector<TransportInfo> result;

  if (this -> config.getTCPEnable())
  {
    TransportInfo t;
    t.host = this -> transport.getHostName();
    t.port = this -> config.getTCPPort();
    t.type = TCP;
    result.push_back(t);
  }    

  //TODO: do it for other transport as well.  
  return result;
}

Config* Application::getConfig()
{
  return &this -> config;
}

void Application::stopApp()
{
  this -> shouldBeRunning = false;
}

void Application::updatePipesCache(RemoteNode &rn, AbstractPipe *pipe)
{
  info(logger, "Adding new pipe to list of existing pipes, old size = "
      + Util::toString(networkPipes.size()) + " new size = "
      + Util::toString(networkPipes.size()));

  networkPipes.insert(make_pair(rn, pipe));

  if (logger.getLevel() == Poco::Message::PRIO_DEBUG)
  {
    map<RemoteNode, AbstractPipe*>::iterator iter;
    int count = 0;
    debug(logger, "Network pipes:");
    for (iter = networkPipes.begin(); iter != networkPipes.end(); iter++)
    {
      RemoteNode n = iter->first;
      AbstractPipe *p = iter->second;
      debug(logger, Util::toString(count) + "-"+ n.toString());
      if (p == NULL)
        debug(logger, "pipe == NULL");

      count++;
    }
  }
}

bool Application::searchRemoteLinks(Node& query, NodeSet& result, unsigned int flags)
{
	return nodeCache.searchRemoteLinks(query, result, flags);
}

Node* Application::getNextHop(NodeSet &audienceSet, Node *source, int step, int type)
{
	Node * returnVal = NULL;
	NodeSet::Iter iter(audienceSet);
	NodeSet prefixNodes;
	unsigned int seqN = -1;
	char *method = "proto_getNextHop";

	/*DebugLevel(4, 
		seqN = logMessage(seqN, method, "audienceSize", toString(audienceSet.treeSize));
	);*/

	while (iter)
	{
		Node * nextHop = (Node*) iter->key;
		if (nextHop == source)
		{
			iter++; continue;
		}
		
		elem_t localMask1, remoteMask1, localMask2, remoteMask2;
		//step should be within the range of prefixIndex array.
		
		if (type == TYPE_SUFFIX)
		{
			localMask1 = nodeCache.suffixIndex(step-1);
			remoteMask1 = nextHop->suffixIndex(step-1);
			localMask2 = nodeCache.suffixIndex(step);
			remoteMask2 = nextHop->suffixIndex(step);
		}
		else
		{
			localMask1 = nodeCache.prefixIndex(step-1);
			remoteMask1 = nextHop->prefixIndex(step-1);
			localMask2 = nodeCache.prefixIndex(step);
			remoteMask2 = nextHop->prefixIndex(step);
		}

		if (step == 1)
		{
			//first bit should be different for the recipient
			if (localMask2 != remoteMask2)
			{
				prefixNodes.insert(nextHop);
			}
		}
		else
		{
			if (localMask1 == remoteMask1 && localMask2 != remoteMask2)
			{
				prefixNodes.insert(nextHop);
			}
		}
		iter++;
	}
	
	/*if (returnVal)
		logMessage(seqN, "getNextHop", "hop", returnVal->hexstring());
	else
		logMessage(seqN, "getNextHop", "hop", "NULL");*/

	//get node for highest level.
  if (prefixNodes.treeSize >0)
  {
    iter = prefixNodes.first();
    int level = -1;
    while (iter)
    {
      Node *nextHop = (Node*) iter->key;
      if (nextHop -> getLevel() == 0)
      {
        returnVal = nextHop;
        break;
      }
      if (nextHop->getLevel() < level)
      {
        returnVal = nextHop;
        level = nextHop->getLevel();
      }
      iter++;
    }
  }

	return returnVal;
}

void Application::setLevel(int newLevel)
{
	nodeCache.setLevel(newLevel);
}

int Application::getBwUsage()
{
  time_t now = time(NULL);
  int lapsed = now - startTime;

  if (lapsed != 0)
    return transport.getNetworkBytesCount() / lapsed;

  else
    return 0;
}

} // namespace App
} // namespace Tourist

int main(int argc, char **argv)
{
  struct timeval tv;
  struct timezone tz;
  //random generator
  if (gettimeofday(&tv, &tz) == 01)
    srand(time(NULL));
  else
    srand(tv.tv_usec);

  Config config;

  fprintf(stderr, "Port number %d" + config.getTCPPort());

  Tourist::App::Application app1(config);

  if (app1.init() != 0)
  {
    fprintf(stderr, "Failed to initialize the application, check log for detail!");
    return -1; 
  } 

  /*RemoteNode rn1, rn2, rn3;
  app1.addNewNode(&rn1);
  app1.addNewNode(&rn2);
  app1.addNewNode(&rn3);

  fprintf(stderr, "Number of nodes in cache %d\n", app1.getNodeCount(0));*/
 
  Thread t;
  t.start(app1);
    
  while (1)
    Thread::sleep(5);
}
