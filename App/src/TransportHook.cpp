/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/App/TransportHook.h"

TransportHook::TransportHook()
{  
  init();
}

TransportHook::~TransportHook()
{  
}

void TransportHook::init()
{
  //Starting the transport factory.
  
}

int TransportHook::registerMessageHnadler(int messageID, AbstractMessage *msg)
{
  if (msg == NULL)
    return -1;
  
  FastMutex::ScopedLock(mutex);
  //TODO:: We shoudln't allow overriding the existing handler
  handlers[messageID] = msg;
  return 0;
}

int TransportHook::unregisterMessageHandler(int messageID)
{  
  FastMutex::ScopedLock lock(mutex);
  HandlerMap::iterator it = handlers.find(messageID)
  if (it != handler.end())
  {
    handlers.erase(it);
    return 0;
  }
  
  return -1;
}
