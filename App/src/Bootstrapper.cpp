/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef BOOTSTRAP_H_
#define BOOTSTRAP_H_

#include "Tourist/App/Bootstrapper.h"
#include "Tourist/App/Application.h"
#include "Tourist/Message/MsgBootstrapRequest.h"
#include "Tourist/Message/MsgBootstrapReply.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Util/Util.h"
#include "Poco/Thread.h"


using Tourist::Message::MessageHeader;
using Tourist::Message::MessageTypeData;
using Tourist::Message::MsgBootstrapRequest;
using Tourist::Message::MsgBootstrapReply;
using Poco::Thread;

using namespace Tourist::Util;

namespace Tourist {
namespace App {

Bootstrapper::Bootstrapper(Application *app):logger(Logger::get("Tourist.App.Bootstrapper"))
{
  this -> parentApp = app;
  this -> shouldBeRunning = true;
  setConfig(this -> parentApp);
}

Bootstrapper::~Bootstrapper()
{
}

void Bootstrapper::run()
{  
  
  if (this -> parentApp == NULL)
  {
    error(logger, "Parent app pointer is NULL.. bootstrap will exit..");
    return;
  }

  //register for call back
  info(logger, "Registering component : Bootstrapper");
  if (this -> config == NULL) 
  {
    error(logger, "Configuration object is NULL.. boostrap will exit.!");
    return;
  }

  this -> config -> getMessageBus() -> registerNotification("Bootstrapper", 
      NObserver<Bootstrapper, MessageNotification>(*this, &Bootstrapper::onMessage));

  info(logger, "Registered componnet : Bootstrapper");
  info(logger, "Bootstrapper is now at service!");

  while (shouldBeRunning)
  {
    processQueue();
    Thread::sleep(3000);
  }      

  isStopped = true;
}

void Bootstrapper::processQueue()
{
  //consume dispatched message 
  debug(logger, "Queue size = " + toString(queue.size()));
  while (queue.size() != 0)
  {
    //We Got the message.. let's process it
    MessageNotification *msgNotify = dynamic_cast<MessageNotification*> 
	    (queue.dequeueNotification());

    if (msgNotify == NULL)
    {
      error(logger, "Notification for bootstrap response is NULL!");
      continue; //let's see if there is any other message in queue.
    }

    MsgBootstrapRequest *request = dynamic_cast<MsgBootstrapRequest*>
	    (msgNotify -> message);

    if (request == NULL)
    {
      error(logger, "Request object was null");
      continue;
    }

    RemoteNode query = request -> getRequestee();    
    int type = request -> getRequestType();

    debug(logger, "Bootstrap request received from " + query.toString());
    debug(logger, "Bootstrap request type " + toString(type));

    bool topPrefix = false;
    bool topSuffix = false;

    RemoteNode *tpPrefix = NULL;
    RemoteNode *tpSuffix = NULL;
    RemoteNode *fwdTpPrefix = NULL;
    RemoteNode *fwdTpSuffix = NULL;

    Node *temp = NULL;
    if (type == MsgBootstrapRequest::TOP_PREFIX || 
          type == MsgBootstrapRequest::ALL) 
      topPrefix = this -> parentApp -> getTopNode(query, &temp, PREFIX);
    tpPrefix = (RemoteNode*)temp;

    temp = NULL;
    if (type == MsgBootstrapRequest::TOP_SUFFIX || 
          type == MsgBootstrapRequest::ALL) 
      topSuffix = this -> parentApp -> getTopNode(query,  &temp, SUFFIX);
    tpSuffix = (RemoteNode*)temp;

    MsgBootstrapReply reply;
    reply.setComponentName(request -> getSourceCompName());
    reply.setSourceCompName("Bootstrapper");
    reply.setMessageID(request -> getMessageID());

    //if top prefix was found include that in the reply
    if (topPrefix) 
    {
      info(logger, "Got top prefix node " + tpPrefix -> toString());  
      reply.setStatusTopPrefix(MsgBootstrapReply::INCLUDED);
      reply.setTopPrefix(*tpPrefix);
    } 
    else if (type == MsgBootstrapRequest::TOP_PREFIX || 
               type == MsgBootstrapRequest::ALL) 
    { 
      //if we couldn't find top prefix for the node but it was
      //requested. lookup the top prefix via suffix table and
      //forward request accordingly
      temp = NULL;
      bool canFwd = this -> parentApp -> getForwardingNode(query, &temp, PREFIX);
      fwdTpPrefix = (RemoteNode*) temp;

      if (canFwd)
        reply.setStatusTopPrefix(MsgBootstrapReply::FORWARDED);
      else //I am unable to handle this request.
        reply.setStatusTopPrefix(MsgBootstrapReply::NONE);      
    }

    //if top suffix was found in our cache.
    if (topSuffix)
    {
      reply.setStatusTopSuffix(MsgBootstrapReply::INCLUDED);
      reply.setTopSuffix(*tpSuffix);
    } 
    else if (type == MsgBootstrapRequest::TOP_SUFFIX ||
              type == MsgBootstrapRequest::ALL)
    {
      temp = NULL;
      bool canFwd = this -> parentApp -> getForwardingNode(query, &temp, SUFFIX);
      fwdTpSuffix = (RemoteNode*) temp;

      if (canFwd)
        reply.setStatusTopSuffix(MsgBootstrapReply::FORWARDED);
      else
        reply.setStatusTopSuffix(MsgBootstrapReply::NONE);
    }

    //send reply back to the node that requested.
    string t;
    int status = reply.getString(t);
    if (status != 0) 
    {
      error(logger, "Request couldn't be fullfilled as serialization for reply object failed");
      continue;
    }

    MessageHeader msgHeader(MessageHeader::HEADER_VERSION, t.length(), MessageTypeData::BOOTSTRAP_REP);
    
    status = -1;
    RemoteNode *rConnection; 
    bool isNodeConnected = this -> parentApp -> getConnection(query, &rConnection);
    if (isNodeConnected)
    {
        info(logger, "Sending boot reply to requesting node");
        status = rConnection -> sendMessage(&msgHeader, &reply);
	info(logger, "Bootstrp reply status = " + toString(status));
    } 
    else 
    {
      error(logger, "Failed to get connection handler for requested node!");
    }

    if (status <= 0 )
    {
      error(logger, "Couldn't send reply for boot strap reply");
      continue;
    }

    //if we were able to find a fwd node in above steps
    //it means we are going to forward the request to it
    if (fwdTpPrefix)
    {
      MsgBootstrapRequest fwrequest(request -> getComponentName(), request -> getSourceCompName(),
          request -> getRequestee(), MsgBootstrapRequest::TOP_PREFIX);
           
      string temp;
      int status = fwrequest.getString(temp);
      if (status == 0) 
      {
        MessageHeader msgHeader(MessageHeader::HEADER_VERSION, temp.length(), MessageTypeData::BOOTSTRAP_REQ);
	status = fwdTpPrefix -> sendMessage(&msgHeader, &fwrequest);
	if (status <= 0)
	  error(logger, "Failed to forward request for topPrefixNode " + query.toString());
	else
	  info(logger, "Forwarded request for topPrefixNode " + query.toString() + " to " + fwdTpPrefix -> toString());
      }
    } 
    else
      info(logger, "No forward prefix node to contact");

    //same as fwdTpPrefix
    if (fwdTpSuffix)
    {
      MsgBootstrapRequest fwrequest(request -> getComponentName(), request -> getSourceCompName(),
          request -> getRequestee(), MsgBootstrapRequest::TOP_SUFFIX);
           
      string temp;
      int status = fwrequest.getString(temp);
      if (status == 0) 
      {
        MessageHeader msgHeader(MessageHeader::HEADER_VERSION, temp.length(), MessageTypeData::BOOTSTRAP_REQ);
	status = fwdTpSuffix -> sendMessage(&msgHeader, &fwrequest);
	if (status <= 0)
	  error(logger, "Failed to forward request for topSuffixNode " + query.toString());
	else
	  info(logger, "Forwarded request for topSuffixNode " + query.toString() + " to " + fwdTpSuffix -> toString());
      }
    }    
    else
      info(logger, "No forward suffix node to contact");

    debug(logger, "Client Serviced!");
  }
}

void Bootstrapper::stop()
{
  this -> shouldBeRunning = false;
}

bool Bootstrapper::getIsStopped()
{
  return this -> isStopped;
}

void Bootstrapper::setConfig(Application* app)
{
  if (app != NULL)
  {
    if (app -> getConfig() == NULL)
    {
	    error(logger, "Config object is NUL");
	    return;
    
    }
    this -> config = app -> getConfig();
  }
}

void Bootstrapper::onMessage(const AutoPtr<MessageNotification>& notification)
{
  debug(logger, "Bootstrapper::onMessage()");
  MessageNotification *notificationObj = const_cast<MessageNotification*>(notification.get());
  queue.enqueueNotification(notificationObj);
}

}
}

#endif /*BOOTSTRAP_H_ */
