/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/App/PeerConnectionWorker.h"
#include "Tourist/App/Application.h"
#include "Tourist/Net/PipeNotifications.h"
#include "Tourist/Net/TCPPipe.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/MsgPeerAnnouncement.h"
#include "Tourist/Util/Util.h"
#include "Poco/Thread.h"

using Tourist::App::Application;
using Tourist::Message::RemoteNode;
using Tourist::Message::MsgPeerAnnouncement;
using Tourist::Message::MessageHeader;
using Tourist::Net::PipeNotification;
using Tourist::Net::BufferNotification;
using Tourist::Net::TCPPipe;
using Poco::Thread;

using namespace Tourist::Util;

namespace Tourist 
{
namespace App 
{

FastMutex PeerConnectionWorker::mutex;

int PeerConnectionWorker::ref_count = 0;

PeerConnectionWorker::PeerConnectionWorker(Application *app, AbstractPipe *pipe,
     RemoteNode &response, bool isActive, int timeout) : messageProcessor(&msgTypeData), 
      logger(Logger::get("Tourist.App.PeerConnectionWorker")), rc(1)

{
  this -> parent = app;
  this -> pipe = pipe;
  this -> isActive = isActive;
  this -> shouldBeRunning = true;
  this -> stopped = false;
  this -> statusOK = false;
  this -> timeout = timeout;
  this -> response = response;  
  ++ref_count;
} 


PeerConnectionWorker::~PeerConnectionWorker()
{
  debug(logger, "PeerConnectionWorker expired");
   if (!isStatusOK())
  {
    this -> pipe -> close();
    //delete this -> pipe; //being handled by TransportFactory class
  }
 
  --ref_count;
}

void PeerConnectionWorker::duplicate()
{
  debug(logger, "PeerConnectionWorker::duplicate()");

  ++rc;
}

void PeerConnectionWorker::release()
{
  debug(logger, "PeerConnectionWorker::release()");

  if (--rc == 0)
  {
    delete this;
  }
}

void PeerConnectionWorker::run()
{  
  if (this -> pipe == NULL || ! (this -> pipe -> isValid()))
  {
    error(logger, "Connection pipe is NULL, was it terminated in transition?");
    this -> shouldBeRunning = false;
    return;
  }
  else
  {
    info(logger, "Pipe is not NULL, remove this log message!");
  }

  debug(logger, "Registering notification for data/disconnection notification!");

  this -> pipe -> registerDataNotification((CallbackInterface*) this);
  this -> pipe -> registerDisconnectNotification((CallbackInterface*) this);  
  time_t start = time(NULL);  

  //if we are not making connection to remote node
  if (!this -> isActive)
  {   
    info(logger, "Sending handshake information to remote node");
    //we should send reply first.
    int status = sendResponse(false);
    if (status < 0)
      shouldBeRunning = false;    
  }
 
  time_t now = time(NULL);
  //main loop
  while (shouldBeRunning && ((now - start) < timeout))
  {
    Thread::sleep(1000);

    if (queue.size() > 0 )
    {
      PipeNotification *pipeNotify = dynamic_cast<PipeNotification*>
                        (queue.dequeueNotification());       
    
      switch (pipeNotify -> type)
      {
        case PipeNotification::BUFFER_ARRIVED:
	{
  	  info(logger, "Received data buffer");
	  BufferNotification *buffNotify = (BufferNotification*) pipeNotify;
	  messageProcessor.addBytes(buffNotify -> buffer, buffNotify -> len);      
	  AbstractMessage *msg = messageProcessor.popMessage();
	  if (msg != NULL)
	  {
	    handleHandshake(msg);
          }   
          break;
       }
       case PipeNotification::DISCONNECT:
       {
         info(logger, "Pipe disconnected !");
         shouldBeRunning = false; //stop the worker
         break;
       }
     }
   }  

    now = time(NULL);  
    debug(logger, "Timeout = " + toString(timeout) + " (now - start) " + toString((now - start)));
  }
  //set the running to false in case the loop exited due to timeout value.
  stopped = true;
  cleanUp();
}

//We recieved message on the pipe.
void PeerConnectionWorker::callback(void *data)
{
  FastMutex::ScopedLock lock(mutex);

  if (data == NULL)
  {
    error(logger, "Call back received for pipe is null ");
    return; 
  }    

  PipeNotification *pipeNotify = (PipeNotification*) data;
  queue.enqueueNotification(pipeNotify);   
}

void PeerConnectionWorker::stopWorker()
{
  this -> shouldBeRunning = false;
  //cleanUp();
}

bool PeerConnectionWorker::isStopped()
{
  return this -> stopped;
}

bool PeerConnectionWorker::isStatusOK()
{
  return this -> statusOK;
}

RemoteNode* PeerConnectionWorker::getResult()
{
  return this -> result;
}

void PeerConnectionWorker::cleanUp()
{
  int status = this -> pipe -> unregisterDataNotification((CallbackInterface*)this);
  if (status == 0)
    debug(logger, "Successfully un-registered call back for data on the pipe");
 
  status =  this -> pipe -> unregisterDisconnectNotification((CallbackInterface*)this);
  if (status == 0)
    debug(logger, "Successfully un-registered call back for disconnect on the pipe");

}

int PeerConnectionWorker::sendResponse(bool isAck)
{
  string str, header_str;

  //we should send reply first. 
  MsgPeerAnnouncement pAnnounce("HANDSHAKE", "HANDSHAKE", this -> response);   
  pAnnounce.setIsAck(isAck);  
  pAnnounce.getString(str);

  if (pAnnounce.isInErrorState())
  {
    error(logger, "Failed to send response for handshake!");
    return -1;
  }

  MessageHeader header(MessageHeader::HEADER_VERSION, str.length(), pAnnounce.getType());  
  header.getString(header_str);

  int bufferSize = MessageHeader::HEADER_SIZE + str.length();
  char * buffer = new char[bufferSize];
  memcpy(buffer, header_str.c_str(), MessageHeader::HEADER_SIZE);
  memcpy(buffer + MessageHeader::HEADER_SIZE, str.c_str(), str.length());

  int status = -1;
  if (this -> pipe != NULL)
    status = this -> pipe -> sendMessage(buffer, bufferSize);
  else 
    error(logger, "Error in sending response to peer. It looks like peer is disconnected");
  
  return status;
}

void PeerConnectionWorker::handleHandshake(AbstractMessage *message)
{
  if (message == NULL)
  {
    error(logger, "Null object in handle Handshake");
    return;
  }
  
  MsgPeerAnnouncement *pAnnounce = dynamic_cast<MsgPeerAnnouncement*>(message);

  if (pAnnounce -> isInErrorState())
  {
    error(logger, "Failed to process peer announcement message from remote node; Handshake will terminate!");
    this -> statusOK = false;
    this -> shouldBeRunning = false;
    return;
  }
 
  if (isActive && !pAnnounce -> getIsAck())
  {
    //part two of three way handshake at requesting side
    int status = sendResponse(true);
    if (status > 0)
    {
      info(logger, "Responded to remote node handshake request. Waiting for final Ack..");
      result = &pAnnounce -> getSendingPeer();
    } 
    else    
    {
      this -> statusOK = false;
      error(logger, "Failed to response to remote node handshake request. Handshake failed!");
    }
  }
  else if (isActive && pAnnounce -> getIsAck())
  {
    info(logger, "Got final ack for our handshake request. Handshake completed");
    this -> statusOK = true;
    this -> shouldBeRunning = false;   
    result -> setPipe(this -> pipe);
  }
  else 
  {
    //final Ack
    result = &pAnnounce -> getSendingPeer();

    //remote connection should be cached
    this -> parent -> addNewNode(result);

    result -> setPipe(this -> pipe);

    int status = sendResponse(true);
    if (status > 0)
    {
      info(logger, "Sent final Ack; Hanshake completed");
      this -> statusOK = true;
    } 
    else
      this -> statusOK = false;

    this -> shouldBeRunning = false;
  }
  
}

} //namespace App
} //namespace Tourist
