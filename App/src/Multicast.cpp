#include "Tourist/App/Multicast.h"
#include "Poco/Logger.h"
#include "Tourist/Constants.h"
#include "Tourist/Util/Util.h"
#include "Poco/Instantiator.h"

using namespace Tourist::Util;

using Poco::Instantiator;

namespace Tourist{
namespace App{


Multicast::Multicast(Application *_app, int objCount)  : 
logger(Logger::get("Tourist.App.Multicast." + Util::toString(objCount) ))
{
	this->app = _app;
	this->shouldBeRunning = true;
}

Multicast::~Multicast()
{
}

void Multicast::run()
{
	//Register for incoming multicast messages
	receiveMulticastMsg();

	while(shouldBeRunning)
	{
		processQueue();
		Poco::Thread::sleep(500);
	}

}

void Multicast::stop()
{
  this -> shouldBeRunning = false;
}

void Multicast::processQueue()
{
  //debug(logger, "Multicast::processQueue()");

	  //consume dispatched message 
		while (queue.size() != 0)
		{
			info(logger, "Processing multicast message");
			//We Got the message.. let's process it
			MessageNotification *msgNotify = dynamic_cast<MessageNotification*>
				(queue.dequeueNotification());
			if (msgNotify == NULL)
			{
				error(logger, "Notification for multicast  response is NULL!");
				continue; //let's see if there is any other message in queue.
			}

			debug(logger, "Multicast Message Received. Parsing message ....");

			MsgMulticast *msgMulticast = dynamic_cast<MsgMulticast*> (msgNotify -> message);
			debug(logger, "Multicast Initiated by: " + msgMulticast->getInitiatingPeer().toString());
			RemoteNode thisNode;
			app->getLocalNodeInfo(thisNode);
			debug(logger, "This node is: " + thisNode.toString());

			
			//Initiate Tree based Multicast
			// Call localNode method findRemoteLinks with approprate flags
			// in a loop send multicast event to all nodes in NodeSet 
			// end condition: if findRemoteLinks reuturns no node then no sendEvent & multicast finished.
			
			RemoteNode initNode = msgMulticast->getInitiatingPeer();
			multicastRouting(&initNode, msgMulticast->getEventType(), msgMulticast->getStep());	

		}
}

void Multicast::multicastRouting(Node* source, int event_type, int step)
{
	NodeSet audienceSet;
	int type = TYPE_PREFIX;
	int multicastCount = 0;
	bool status=false;

	if (event_type >= 1 && event_type <= 4) {
		status = app->searchRemoteLinks(*source, audienceSet, LOWER_LEVELS|EQUAL_LEVELS|FIND_PREFIXES|FIND_ALL);
	} else {
		status = app->searchRemoteLinks(*source, audienceSet, LOWER_LEVELS|EQUAL_LEVELS|FIND_SUFFIXES|FIND_ALL);
		type = TYPE_SUFFIX;
	}

	debug(logger, status?"Audience Set Found":"No Audience Set Found");

	if(status) {
	//TODO: Use variable instead of 64
	for (int i=step+1; i<=64; i++) {
		Node *nextHop = app->getNextHop(audienceSet, source, i, type);
    if (nextHop == NULL || nextHop->hexstring() == source->hexstring()) {
			continue;
		}

		debug(logger, "Sending event to an audience set node ..... ");
	  int ack = sendEvent(source, nextHop, event_type, i);

		if (ack > 0)
			multicastCount++;
	}
	}

}


void Multicast::levelChangeEvent(int newLevel, Node *localNode)
{
  //Initiating prefix multicast
  int prefixEvent, suffixEvent;
  if (localNode->getLevel() > newLevel)
  {
    prefixEvent = EVENT_PREFIX_LEVEL_UP;
    suffixEvent = EVENT_SUFFIX_LEVEL_UP;
  }
  else
  {
    prefixEvent = EVENT_PREFIX_LEVEL_DOWN;
    suffixEvent = EVENT_SUFFIX_LEVEL_DOWN;    
  }
  
	Node *pTopNode;
	if(! (app -> getTopNode(*localNode, &pTopNode, PREFIX)))
		error(logger, "Cannot find Top Node!");
	else debug(logger, "Top Node found");
  int prefixAck = sendEvent(localNode, pTopNode, prefixEvent, 0);

	if(! (app -> getTopNode(*localNode, &pTopNode, SUFFIX)))
		error(logger, "Cannot find Top Node!");
	else debug(logger, "Top Node found");
  int suffixAck = sendEvent(localNode, pTopNode, suffixEvent, 0);

}


int Multicast::sendEvent(Node *source, Node *target, int event_type, int step) 
{
  if (source == NULL || target == NULL)
    return -1;

  unsigned int seqN = 0;
  string log;
  const char* method = "eventOut_sendEvent";

  /* ------------------ LOG [Send Event]----------------------- */  

  string eventType = "Un-Known";
  switch(event_type)
  {
    case EVENT_PREFIX_JOIN:
      eventType = "Prefix join";
      break;
      
    case EVENT_PREFIX_DISCONNECT:
      eventType = "Prefix disconnect";
      break;
      
    case EVENT_PREFIX_LEVEL_UP:
      eventType = "Prefix level up";
      break;
      
    case EVENT_PREFIX_LEVEL_DOWN:
      eventType = "Prefix level down";
      break;

    case EVENT_SUFFIX_JOIN:
      eventType = "Suffix join";
      break;
      
    case EVENT_SUFFIX_DISCONNECT:
      eventType = "Suffix disconnect";
      break;
      
    case EVENT_SUFFIX_LEVEL_UP:
      eventType = "Suffix level up";
      break;
      
    case EVENT_SUFFIX_LEVEL_DOWN:
      eventType = "Suffix level down";
      break;	
  }

	
	debug(logger, 
				"source:source_level= " + source->hexstring() + ":" + Util::toString(source->getLevel()) +
				"\ntarget:target_level= " + target->hexstring() + ":" + Util::toString(target->getLevel()) +
				"\nevent:step= " + eventType + ":" + Util::toString(step) );

				
    /* ---------------------------------------------------------- */ 

	//Get pointer to Topnode
  
	RemoteNode *topNode;
	
	RemoteNode *rNodePtr = (RemoteNode*) target;
	RemoteNode rNode = *rNodePtr;
	
	int statusConn = app->getConnection(rNode, &topNode);	
	
	//Register Message Type
	MessageTypeData msgType;
	msgType.registerMessageObj(MessageTypeData::MULTICAST, new Instantiator<MsgMulticast, AbstractMessage>);

	RemoteNode srcNode;
	app->getLocalNodeInfo(srcNode);
	
	MsgMulticast *msgMulticast = new MsgMulticast("Multicast","Multicast", srcNode, event_type, step);
	string str;
	msgMulticast->getString(str);
		
	//Instantiate MessageHeader object
	MessageHeader *msgHeader = new MessageHeader(MessageHeader::HEADER_VERSION, str.length(), MessageTypeData::MULTICAST);
	
	//Send Multicast Request Message to Top Node	
	topNode->sendMessage(msgHeader, msgMulticast);
	
  return true;
}



//------------------------------------------------------------------------------
void Multicast::receiveMulticastMsg()
{
	app->getConfig()->getMessageBus()->registerNotification("Multicast",
      NObserver<Multicast, MessageNotification>(*this, &Multicast::onMulticastMsg));
}

void Multicast::onMulticastMsg(const AutoPtr<MessageNotification>& notification)
{
	debug(logger, "Multicast::onMulticastMsg()");

	MessageNotification *notificationObj = const_cast<MessageNotification*>(notification.get());
	queue.enqueueNotification(notificationObj);

}

} //namespace Tourist
} //namespace App
