/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/App/Bootstrap.h"
#include "Tourist/App/Application.h"
#include "Tourist/Message/MsgBootstrapRequest.h"
#include "Tourist/Message/MsgBootstrapReply.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Util/Util.h"
#include <math.h>

using Tourist::Message::MsgBootstrapRequest;
using Tourist::Message::MsgBootstrapReply;
using Tourist::Message::MessageHeader;

using namespace Tourist::Util;

namespace Tourist {
namespace App {

const int Bootstrap::BOOT_INIT = 1;
const int Bootstrap::BOOT_TIMEOUT = 2;
const int Bootstrap::BOOT_OK = 3;

Bootstrap::Bootstrap(Application *app) : logger(Logger::get("Tourist.App.Bootstrap"))
{
  this -> parentApp = app;
  this -> shouldBeRunning = true;

  //the non-negative value of the next two components will
  //decide if they were set properly by the bootstrap process.
  //
  this -> rNodeBWUsage = -1;
  this -> newLevel = -1;

  topPrefixNode = topSuffixNode = NULL;

  setConfig(this -> parentApp);
}

Bootstrap::~Bootstrap()
{
}

void Bootstrap::run()
{
  debug(logger, "Bootstrap::run()");

  if (this -> parentApp == NULL)
  {
    error(logger, "Parent app pointer is NULL.. bootstrap will exit..");
    return;
  }

  this -> config -> getMessageBus() -> registerNotification("Bootstrap",
     NObserver<Bootstrap, MessageNotification>(*this, &Bootstrap::onMessage));

  info(logger, "Registered component: Bootstrap");
  
  vector<SocketAddress> bootNodes = this -> config -> getBootAddresses();
  //try to connect to a node and bootstrap this node
  if (bootNodes.size() == 0)
  {
    error(logger, "No boot nodes information available.. node may not perform well");     
    return;
  }

  status = Bootstrap::BOOT_INIT;
 
  RemoteNode *contactNode = NULL;
  for (int i=0; i<bootNodes.size(); i++)
  {
    if (!shouldBeRunning)
      break;

    SocketAddress address = bootNodes.at(i);

    info(logger, "Trying to contact host " + address.host().toString() + ":" + toString(address.port()) +" for bootstrap");
    debug(logger, "Timeout value for connection is 1000");
    int status = this -> parentApp -> connectionTo(address.host().toString(), address.port(), TCP, &contactNode, 1000);

    debug(logger, "Connection status = " + toString(status));
    if (status != 0)      
      continue;

    debug(logger, "Boot node level " + toString(contactNode -> getLevel()));
    debug(logger, "Boot node bwUsage " + toString(contactNode -> getBwUsage()));
    debug(logger, "Our BW threshold " + toString(this -> config -> getBWThreshold()));

    //our new level
    newLevel = contactNode -> getLevel();
    //if the threshold is set to 0, then bandwidth cost
    //need not to be calculated
    if (this -> config -> getBWThreshold() != 0)
    {
      int bwCost = contactNode -> getBwUsage() / this -> config -> getBWThreshold();
      int levelInc = 0;
      if (bwCost != 0) 
        levelInc = log(bwCost)/log(2);

      newLevel += (int) levelInc;
    }

    info(logger, "Our new level " + toString(newLevel)); 

    RemoteNode thisNode;
    this -> parentApp -> getLocalNodeInfo(thisNode);

    MsgBootstrapRequest bootreq("Bootstrapper", "Bootstrap", thisNode, MsgBootstrapRequest::ALL);
    string temp;
    status =  bootreq.getString(temp);
    if (status != 0)
    {
      error(logger, "Couldn't create Bootstrap request");
      continue;
    }    

    int payloadSize = temp.length();
    MessageHeader header(MessageHeader::HEADER_VERSION, payloadSize, MessageTypeData::BOOTSTRAP_REQ);
    int stat = contactNode -> sendMessage(&header, &bootreq);

    if (status < 0)
    {
      error(logger, "Failed to send bootstrap request to node");                   
      delete contactNode;
      //TODO::break the existing connections to the node
      continue;
    }
    
    //we have sent request for bootstrap..let's wait for the response from the node.
    //We shouldn't wait for longer than 'this -> bootWait_1'
    time_t start = time(NULL);
    while (shouldBeRunning)
    {
      time_t now = time(NULL);
      //TODO: Fix this hard-coded wait time
      if ( (now - start) > 20) 
      {
        error(logger, "Boot wait time expired for the current connection");
        break;
      }                   
      
      processQueue();
      Poco::Thread::sleep(500);
    }      
  }

  //
  postProcessBoot();

  isStopped = true;
}

void Bootstrap::processQueue()
{
  debug(logger, "Bootstrap::processQueue()");

  //consume dispatched message 
  while (queue.size() != 0)
  {
    info(logger, "Processing bootstrap reply message");

    //We Got the message.. let's process it
    MessageNotification *msgNotify = dynamic_cast<MessageNotification*> 
	    (queue.dequeueNotification());

    if (msgNotify == NULL)
    {
      error(logger, "Notification for bootstrap response is NULL!");
      continue; //let's see if there is any other message in queue.
    }

    MsgBootstrapReply *reply = dynamic_cast<MsgBootstrapReply*>
	    (msgNotify -> message);
    if (reply == NULL)
    {
      error(logger, "Reply object is NULL!");
      continue;
    }

    RemoteNode tp;
    if (topPrefixStatus != MsgBootstrapReply::INCLUDED)
    {
      if (reply -> getStatusTopPrefix() == MsgBootstrapReply::INCLUDED)
      {
	tp = reply -> getTopPrefix();
	topPrefixStatus = MsgBootstrapReply::INCLUDED;

	info(logger, "Top prefix node is included in the response");
	debug(logger, "Top prefix node = " + tp.toString());
      }
      else if (reply -> getStatusTopPrefix() == MsgBootstrapReply::FORWARDED)
      {
	topPrefixStatus = MsgBootstrapReply::FORWARDED;
	info(logger, "Top prefix request was forwarded by boot node");
      }
      else
      {
	topPrefixStatus = MsgBootstrapReply::NONE;
	info(logger, "No top prefix node in reply");
      }
    }

    RemoteNode ts;
    if (topSuffixStatus != MsgBootstrapReply::INCLUDED)
    {
      if (reply -> getStatusTopSuffix() == MsgBootstrapReply::INCLUDED)
      {  
	ts = reply -> getTopSuffix();
	topSuffixStatus = MsgBootstrapReply::FORWARDED;

	info(logger, "Top suffix node is included in the response");
	debug(logger, "To suffix node = " + ts.toString());
      }      
      else if (reply -> getStatusTopSuffix() == MsgBootstrapReply::FORWARDED)
      {
	topSuffixStatus = MsgBootstrapReply::FORWARDED;
	info(logger, "Top suffix request was forwarded by boot node");
      }
      else
      {
	topSuffixStatus = MsgBootstrapReply::NONE;            
	info(logger, "No top suffix node in reply");
      }
    }

    //We have processed all the messages in queue.. 
    //Let's see if we are now ready for the downloading of 
    //prefix/suffix caches...

    if (topPrefixStatus == MsgBootstrapReply::INCLUDED)
    {
      //download cache.      
      bool status = this -> parentApp -> getConnection(tp, &topPrefixNode);
      if (status)
      {
        info(logger, "Got connection for the Top Prefix node " + tp.toString());
	prefixCache.empty(); //if there are nodes from previous partially
	                     //successful cache xfer request.
        this -> parentApp -> getRemoteCache(topPrefixNode, PREFIX, newLevel, MAX_LEVEL, 
	   5, prefixCache); 
	debug(logger, "Receieved " + toString(prefixCache.size()) + " prefix cache ");
      }
      else
      {
        error(logger, "Failed to get connection for top prefix node");
      }
    }

    if (topSuffixStatus == MsgBootstrapReply::INCLUDED)
    {
      //download cache.
      bool status = this -> parentApp -> getConnection(ts, &topSuffixNode);
      if (status)
      {
        info(logger, "Got connection for the Top Suffix node " + ts.toString());
        
	suffixCache.empty();
        this -> parentApp -> getRemoteCache(topSuffixNode, SUFFIX, newLevel, MAX_LEVEL, 
	   5, suffixCache); 

	debug(logger, "Receieved " + toString(suffixCache.size()) +" suffix cache ");
      }
      else
      {
        error(logger, "Failed to get connection for top prefix node");
      }
    }

   //Stop if:
    //  a) Bot top and prefix were included
    //  b) One of them was included and other is None
    else if( ( topPrefixStatus == MsgBootstrapReply::INCLUDED ||
               topPrefixStatus == MsgBootstrapReply::NONE   ) &&
	     ( topSuffixStatus == MsgBootstrapReply::INCLUDED ||
	       topSuffixStatus == MsgBootstrapReply::NONE ))
      //We are done.. let's stop the boot process
      shouldBeRunning = false;
  }
}
\
void Bootstrap::stop()
{
  this -> shouldBeRunning = false;
}

bool Bootstrap::getIsStopped()
{
  return this -> isStopped;
}

void Bootstrap::onMessage(const AutoPtr<MessageNotification>& notification)
{
  debug(logger, "Bootstrap::onMessage()");
  MessageNotification *notificationObj = const_cast<MessageNotification*>(notification.get());
  queue.enqueueNotification(notificationObj);
}

void Bootstrap::setConfig(Application* app)
{
   if (app != NULL)
  {
    if (app -> getConfig() == NULL)
    {
	    error(logger, "Config object is NUL");
	    return;
    }
    this -> config = app -> getConfig();
  }
}

void Bootstrap::postProcessBoot() 
{ 
  if (topPrefixStatus == MsgBootstrapReply::INCLUDED &&
        topPrefixNode != NULL)
    this -> parentApp -> addNewNode(topPrefixNode);

  if (topSuffixStatus == MsgBootstrapReply::INCLUDED &&
        topSuffixNode != NULL)
    this -> parentApp -> addNewNode(topSuffixNode);
  
  if (prefixCache.size() > 0) {
    NodeSet::Iter iter = prefixCache.first();
    while(iter) {
      RemoteNode *node = (RemoteNode*) iter -> key; 
      if (node != NULL) 
      {
        debug(logger, "Adding node received as prefix cache request " + node -> toString());  
        this -> parentApp -> addNewNode(node);
      }
      iter++;
    }
  }

  if (suffixCache.size() > 0) {
    NodeSet::Iter iter = suffixCache.first();
    while(iter) {
      RemoteNode *node = (RemoteNode*) iter -> key; 
      if (node != NULL) 
      {
        debug(logger, "Adding node received as suffix cache request " + node -> toString());  
        this -> parentApp -> addNewNode(node);
      }
      iter++;
    }
  }
}

}
}
