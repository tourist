/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/App/CacheXferWorker.h"
#include "Tourist/LocalNode.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "Tourist/Message/MsgCacheXferReply.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/MessageNotification.h"
#include "Tourist/Util/Util.h"

#include "Poco/Thread.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"

using Tourist::LocalNode;
using Tourist::Message::RemoteNode;
using Tourist::Message::MsgCacheXferRequest;
using Tourist::Message::MessageHeader;
using Tourist::Message::MessageTypeData;
using Tourist::Message::MsgCacheXferReply;

using namespace Tourist::Util;

using Poco::Observer;
using Poco::NObserver;
using Poco::Thread;

namespace Tourist
{
namespace App
{

//task code 
int CacheXferWorker::TASK_REMOTE_TO_LOCAL = 1;
int CacheXferWorker::TASK_LOCAL_TO_REMOTE = 2;

//status code initialization
int CacheXferWorker::STATUS_UNKNOWN = -1;
int CacheXferWorker::STATUS_OK = 0;
int CacheXferWorker::STATUS_TIMEDOUT = 1;
int CacheXferWorker::STATUS_TIMEDOUT_PARTIAL = 2;
int CacheXferWorker::STATUS_CONNECT_FAILED = 3;

//state code initialization
const int CacheXferWorker::AWAIT_RESPONSE = 1;
const int CacheXferWorker::PROCESSING_RESPONSE = 2;

//evnet code initialization
int CacheXferWorker::FINISHED = 1;
int CacheXferWorker::RESPONSE_RECVD = 2;
int CacheXferWorker::RESPONSE_OK = 3;
int CacheXferWorker::RESPONSE_PARTIAL = 4;
int CacheXferWorker::STOPPED = 5;

//maintains reference count
int CacheXferWorker::ref_count = 0;

CacheXferWorker::CacheXferWorker(RemoteNode *contactNode, string threadName,
    MsgCacheXferRequest *request, MessageDispatcher *dispatcher, int timeout) :
  logger(Logger::get("Tourist.Message.CacheXferWorker")), rc(1)
{
  this -> contactNode = contactNode;
  this -> threadName = threadName;
  this -> shouldbeRunning = true;
  this -> stopped = false;
  this -> taskType = TASK_REMOTE_TO_LOCAL;
  this -> xferRequest = request;
  this -> dispatcher = dispatcher;
  this -> timeout = timeout;
  ++ref_count;
}

CacheXferWorker::CacheXferWorker(RemoteNode *contactNode, string threadName,
    MsgCacheXferRequest *remoteRequest, MessageDispatcher *dispatcher, 
    LocalNode *localnode, int timeout) :
   logger(Logger::get("Tourist.Message.CacheXferWorker")), rc (1)
{
  this -> contactNode = contactNode;
  this -> threadName = threadName;
  this -> shouldbeRunning = true;
  this -> stopped = false;
  this -> taskType = TASK_LOCAL_TO_REMOTE;
  this -> xferRequest = remoteRequest;
  this -> dispatcher = dispatcher;
  this -> node = localnode;  
  this -> state = 0; //unknown
  this -> timeout = timeout;
  ++ref_count;
}

CacheXferWorker::~CacheXferWorker()
{
  debug(logger, "CacheXferWorker expired!");
  --ref_count;
}

void CacheXferWorker::duplicate()
{
 debug(logger, "CacheXferWorker::duplicate() ");
  ++rc;
}

void CacheXferWorker::release()
{
  debug(logger, "CacheXferWorker::release()");

  if (--rc == 0) 
  {
    delete this;
  }
}

void CacheXferWorker::startWorker()
{
  shouldbeRunning = true;
  stopped = false;
}

void CacheXferWorker::stopWorker()
{
  shouldbeRunning = false;
  //changeState(CacheXferWorker::STOPPED);
}

void CacheXferWorker::cleanUp()
{
  shouldbeRunning = false;
  dispatcher -> deregisterNotification(this->threadName,
      NObserver<CacheXferWorker, MessageNotification>(*this, &CacheXferWorker::onMessage));
}

void CacheXferWorker::run()
{
  
  debug(logger, "CacheXferWorker::run()");

  time_t start = time(NULL);
  this -> threadName = Thread::current() -> name();

  info(logger, "[ Thread = " + this -> threadName+ " ] RemoteNode = "
      + this -> contactNode -> toString());

  /** 
   * register to recieve all notifications addressed to this thread
   * make a buffer for notifications and process them one by one.  
   */
  dispatcher -> registerNotification(this -> threadName,
      NObserver<CacheXferWorker, MessageNotification>(*this, &CacheXferWorker::onMessage));
  debug(logger, "[" + this -> threadName+ "] registered notification");

  //Possible exit condition from the infinite loop
  //Parent set the flag for stop 'shouldbeRunning'
  //Task at hand is finished.

  if (getTaskType() == TASK_REMOTE_TO_LOCAL)
  {
    //Send the request to remote node, set the status and wait
    //periodically check if the controller is interested in shutting this
    //thread down. check if there is a reply in the queue, handle the reply
    //set the status.  
    try
    {
      //break the event loop if msg send fails
      string temp;
      xferRequest -> setSourceCompName(this -> threadName);
      xferRequest -> getString(temp);
      int payloadSize = temp.length();
      MessageHeader msgHeader(MessageHeader::HEADER_VERSION, payloadSize, MessageTypeData::CACHE_XFER_REQ);
      int stat = contactNode -> sendMessage(&msgHeader, xferRequest);
      if (stat < 0)
      {
        debug(logger, "Message sending failed to remote node");
        status = STATUS_CONNECT_FAILED;
        cleanUp();
        return;
      }
    }
    catch (Poco::Exception &e)
    {
      string ermsg = e.what();
      error(logger, "Failed to send transfer request " +ermsg);
    }

    state = CacheXferWorker::AWAIT_RESPONSE;
    time_t now = time(NULL);

    while (shouldbeRunning)
    {
      if ( (now - start) > timeout)
      {
        status = (state == CacheXferWorker::RESPONSE_PARTIAL) ?
                 STATUS_TIMEDOUT_PARTIAL : STATUS_TIMEDOUT;
        info (logger, "Timeout expired! " + toString((now - start)));
        break;
      }
      
      processResponse();

      if (state == CacheXferWorker::RESPONSE_OK)
      {
         status = CacheXferWorker::STATUS_OK;
         break;
      }       

      Thread::sleep(1000);
      now = time(NULL);
      debug(logger, "Timeout = " + toString(timeout) + " (now - start) = "
            + toString((now - start)));
    }
  }
  else
  { 
    // we are here, it means that we are going to process request       
    processRequest();               
  }
  cleanUp();
  state = CacheXferWorker::STOPPED;
  stopped = true;
}
/**
 * Handle new event and take decision based on the exisitng
 * state and the incoming event
 */
/*void CacheXferWorker::changeState(int event)
{
  debug(logger, " state = " + toString(this -> state) + " event = " + toString(event));
  switch (this -> state)
  {
    info(logger, "state = " + Util::toString(this -> state) + " event = " + Util::toString(event));

    case CacheXferWorker::AWAIT_RESPONSE:
      if (event == STOPPED)
      {
        cleanUp();
        state = CacheXferWorker::FINISHED;
        status = CacheXferWorker::STATUS_TIMEDOUT;

      }
      else if (event == CacheXferWorker::RESPONSE_RECVD)
      {
        state = CacheXferWorker::PROCESSING_RESPONSE;
        processResponse();
      }
      break; // case AWAIT_RESPONSE

    case CacheXferWorker::PROCESSING_RESPONSE:
      if (event == CacheXferWorker::RESPONSE_OK)
      {
        state = CacheXferWorker::FINISHED;
        status = CacheXferWorker::STATUS_OK;
        cleanUp();
      }
      else if (event == CacheXferWorker::RESPONSE_PARTIAL)
      {
        //we still have few bits and pieces missing.
        state = CacheXferWorker::AWAIT_RESPONSE;
      }
      //In case we recieve some more bits for our initial response.
      else if (event == CacheXferWorker::RESPONSE_RECVD)
      {
        processResponse();
      }

      else if (event == CacheXferWorker::STOPPED)
      {
        state = CacheXferWorker::FINISHED;
        status = CacheXferWorker::STATUS_TIMEDOUT_PARTIAL;
        cleanUp();
      }
      break; //case PROCESSING_RESPONSE   
  }
}*/

void CacheXferWorker::processResponse()
{
  if (queue.size() < 0)
    return; //no new message.

  //TODO: Current way of recognizing message type is very crude. 
  //We need to fix it e.g using 'type' attribute of AbstractMessage.h
  info(logger, "processing response");

  while (queue.size() != 0) //consume all message
  {
    MessageNotification *msgNotify
               = dynamic_cast<MessageNotification*>(queue.dequeueNotification());
                             
    if (msgNotify == NULL)
    {
      error(logger, "Message type casting failed");
      return;
    }

    MsgCacheXferReply * reply = 
	    dynamic_cast<MsgCacheXferReply*>(msgNotify -> message);

    if (reply == NULL)
    {
      error(logger, "Message type notified is not MessageCacheXferReply");
      return;
    }  

    debug(logger, "Response node size is "
		    + Util::toString(reply -> getResponseSize() ) );
    debug(logger, "Response node count is "
		    + Util::toString(reply -> getNodeCount() ) );

    //copying all the nodes from the response to result-set.
    NodeSet::Iter iter(reply -> getReplyNodes());
    while (iter)
    {
       RemoteNode *rn = (RemoteNode*) iter -> key;        
       //debug(logger, "Added node = " + rn -> toString());
       //TODO Shouldn't copy them
       result.insert(rn);
       iter++;
    }    

    if (reply -> getResponseSize() == reply -> getNodeCount())
    {
      debug(logger, " Response is OK");
      state = CacheXferWorker::RESPONSE_OK;
    }
    else
    {
      debug(logger, "Response is partial");
      state = CacheXferWorker::RESPONSE_PARTIAL;
    }    
  }
}

void CacheXferWorker::processRequest()
{
  //Create reply object to be send against a given request.
  debug(logger, "Calculating response for node " + this -> contactNode -> toString());
  if (this -> node == NULL)
  {
    error(logger, "CacheXfer-Worker cann't process request as 'node cache' object is not valid");
    return;
  }

  int startLevel = xferRequest -> getStartLevel();
  int endLevel = xferRequest -> getEndLevel();
  int type = xferRequest -> getCacheType();

  debug(logger, "Start level = " + toString(startLevel) + " End level =" +
     toString(endLevel) + " cache = " + toString(type));
 
  int responseSize = 0;

  //checking bounds on startLevel and endLevel.
  if ((endLevel - startLevel) <= 0)  
    return;

  if (type != PREFIX && type != SUFFIX)
    return;
  
  NodeSet responseNodes;
  for (int i=startLevel; i<=endLevel; i++)
  { 
    NodeSet tmp;   
    int respCount = this -> node -> getNodes(i, tmp, (unsigned int)type);
    if (respCount > 1 )
    {
      NodeSet::Iter iter(tmp);
      while (iter)
      {
        responseNodes.insert(iter->key);
        iter++;
      }
    }     
  }

  info(logger, "Response size = " + toString(responseNodes.treeSize));
  
  if (responseNodes.treeSize < 1)
    return;

  //TODO: this cast 'responseNodes.treeSize' is dangerous.
  MsgCacheXferReply reply(xferRequest -> getSourceCompName(), this -> threadName, 
    xferRequest -> getMessageID(), (int) responseNodes.treeSize, (int) responseNodes.treeSize,
    responseNodes);

  try
  { 
    //break the event loop if msg send fails
    string temp;
    int status = reply.getString(temp);

    //TODO: Throw error log here
    if (status != 0)
      return;

    int payloadSize = temp.length();
    MessageHeader msgHeader(MessageHeader::HEADER_VERSION, payloadSize, MessageTypeData::CACHE_XFER_REP);
    int stat = contactNode -> sendMessage(&msgHeader, &reply);
    if (stat < 0)
    {
      status = STATUS_CONNECT_FAILED;
      cleanUp();
      return;
    }
  }
  catch (Poco::Exception &e)
  {
    string ermsg = e.what();
    error(logger, "Failed to send response for transfer request " +ermsg);
  }   
}

bool CacheXferWorker::isStopped()
{
  return this -> stopped;
}

NodeSet CacheXferWorker::getNodes()
{
  return result;
}

int CacheXferWorker::getStatus()
{
  return status;
}

int CacheXferWorker::getTaskType()
{
  return taskType;
}

void CacheXferWorker::onMessage(const AutoPtr<MessageNotification>& notification)
{
  MessageNotification *notificationObj = const_cast<MessageNotification*>(notification.get());
  queue.enqueueNotification(notificationObj);
  state = CacheXferWorker::RESPONSE_RECVD;
}

} // namespace App
} // namespace Tourist
