/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/App/CacheXfer.h"
#include "Tourist/App/CacheXferWorker.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Util/Config.h"
#include "Poco/Thread.h"
#include <ctime>

using Tourist::Message::MsgCacheXferRequest;

using namespace Tourist::Util;
using Poco::Thread;

namespace Tourist {
namespace App {

int CacheXfer::STATUS_OK = 0;

int CacheXfer::STATUS_PARTIAL = 1;

int CacheXfer::STATUS_NO_RESPONSE = 2;

int CacheXfer::STATUS_FAILED = -1;


CacheXfer::CacheXfer(Config *config) : threadPool("CacheXfer"),
          logger(Logger::get("Tourist.App.CacheXfer"))
{
  poolSize = threadPool.capacity();
  this -> config = config;   

  // config object was being populated after calling the cachexfer
  // contructor. Thus was giving problem.
  //msgDispatcher = this -> config -> getMessageBus();   

  isStopped = false;
  info(logger, "Created threadPool with size " + toString(poolSize));
}

CacheXfer::~CacheXfer()
{
  deregisterForRemoteReqs();
  debug(logger, "CacheXfer expired");
  threadPool.stopAll();
}

void CacheXfer::registerForRemoteReqs()
{ 
  if (config -> getMessageBus() != NULL) 
  {
    config -> getMessageBus() -> registerNotification("CacheXfer", 
      NObserver<CacheXfer, MessageNotification>(*this, &CacheXfer::onMessage));
   debug(logger, "Register CacheXfer for receiving xfer requests");
  }
  else
    error(logger, "Failed to register CacheXfer component with message bus");
}

void CacheXfer::deregisterForRemoteReqs()
{
  if (config -> getMessageBus() != NULL)
  {
    config -> getMessageBus() -> deregisterNotification("CacheXfer", 
          NObserver<CacheXfer, MessageNotification>(*this, &CacheXfer::onMessage));
    debug(logger, "de-Register CacheXfer for receiving xfer requests");
  }
}

void CacheXfer::run()
{
  
  if (config -> getMessageBus() == NULL)
  {
    error(logger, "Message dispatcher is NULL, CacheXfer will exit now");
    return;
  }

  //register for incoming messages.
  registerForRemoteReqs();
  
  //now lets wait for the message.  
  while (!isStopped)
  {  
    //if we don't have enought threads left in the thread pool or
    //we don't have any notification to process just go to sleep.  
    if (queue.size() > 0 && threadPool.available() > 0) 
    {
      MsgCacheXferRequest *request = NULL;
      MessageNotification *notification = dynamic_cast<MessageNotification*> 
                          (queue.dequeueNotification());
      
      if (notification != NULL)
        request = dynamic_cast<MsgCacheXferRequest*> 
                                 (notification -> message);

      if (request != NULL)
      {
        string workerID = "to-be-assigned-by-worker"; //TODO : Change the id to some random number.
        AutoPtr<CacheXferWorker>  worker(new CacheXferWorker(notification -> sender, workerID, request, 
                     this -> config -> getMessageBus(), this -> config -> getLocalNode())); 
        workers.push_back(worker);
        worker -> duplicate();
        threadPool.start(*worker);          

        debug(logger, "Created worker thread to handle xfer request");
      } 
    }
    Thread::sleep(5000);
    //clean-up worker
    vector< AutoPtr<CacheXferWorker> >::iterator it = workers.begin();
    int totalWorkers = workers.size();
    while (it < workers.end())
    {
      info(logger, "Removing stopped threads, remove this message");
      CacheXferWorker *w = *it;
      if (w == NULL)
      {
        continue;
      }
     
      if (w -> isStopped())
      {  
        workers.erase(it);
        w -> release();
      }
      it++;
    }
    debug(logger, "Total CacheXfer workers = " + toString(totalWorkers) + " removed = " +
       toString(totalWorkers - workers.size()));
  }  
}

int CacheXfer::getRemoteCache (RemoteNode* contactNode, int type, int startLevel,
       int endLevel, int timeout, NodeSet &result)
{   
  if (threadPool.available() == 0)
  {
    //We might want to wait here for a while 
    error(logger, "CacheXfer overloaded poolSize = " + toString(threadPool.capacity()) 
                     + " poolAvailable = " + toString(threadPool.available()));
    return -1;               
  }  
  
  /* It will spawn a worker thread to let it do it work
   * (sending request to remote node, waiting to recieve a 
   * remote connection). 
   * Then, we also need to monitor worker threads to enforce timeout.
   */
  string workerID = "to-be-assigned-by-worker"; //TODO : Change the id to some random number.
  MsgCacheXferRequest msgreq("CacheXfer", workerID, 0, 5, PREFIX);
  CacheXferWorker worker(contactNode, workerID, &msgreq, this -> config -> getMessageBus(), timeout); 
  threadPool.start(worker);
  ///time_t start = time(NULL);

  //we are going to wait for either the timeout to expire or 
  //worker thread to stop.
  //debug(logger, "Worker timeout value is " + toString(timeout));
  //time_t now = time(NULL);
  debug(logger, "CacheXfer should wait for cache xfer to stop!");
  while (! (worker.isStopped()))
  {
	  debug(logger, "CacheXfer is waiting for worker to finish");
	  Poco::Thread::sleep(500);
  }

  debug(logger, "Checking worker result");
  result = worker.getNodes();   

  return worker.getStatus();         	
}


void CacheXfer::onMessage(const AutoPtr<MessageNotification>& notification)
{
  info(logger, "Received notification for cache transfer");
  MessageNotification *notificationObj = const_cast<MessageNotification*>(notification.get());
  queue.enqueueNotification(notificationObj);
}

} // namespace App
} // namespace Tourist
