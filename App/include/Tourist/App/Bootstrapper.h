#ifndef BOOTSTRAPPER_H_
#define BOOTSTRAPPER_H_

#include "Tourist/Message/MessageNotification.h"
#include "Poco/Runnable.h"
#include "Poco/NotificationQueue.h"
#include "Poco/AutoPtr.h"
#include "Poco/Logger.h"


using Tourist::Message::MessageNotification;
using Poco::Runnable;
using Poco::NotificationQueue;
using Poco::AutoPtr;
using Poco::Logger;

namespace Tourist {
namespace Util {
  class Config;
}
namespace App {

class Application;

class Bootstrapper : public Runnable
{
public:
  Bootstrapper(Tourist::App::Application *app);

  ~Bootstrapper();
  
  void run();

  void onMessage(const AutoPtr<MessageNotification>& notification);

  void stop();

  bool getIsStopped();

  /**
   * Call the app-> getConfig() method to assign the 
   * local config object. But, ensure that app is not NULL
   */
  void setConfig(Application *app);
  
private:

  void processQueue();

  Tourist::App::Application *parentApp;

  NotificationQueue queue;

  bool shouldBeRunning, isStopped;

  Tourist::Util::Config * config;

  Logger &logger;
  
};

} //namespace App
} //namespace Tourist

#endif /*BOOTSTRAPPER_H_ */
