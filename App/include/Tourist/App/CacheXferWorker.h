#ifndef CACHEEXPORTERWORKER_
#define CACHEEXPORTERWORKER_

#include "Tourist/LocalNode.h"
#include "Tourist/Util/Util.h"
#include "Tourist/AvlCache.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/MessageDispatcher.h"
#include "Tourist/Message/MessageNotification.h"
#include "Poco/Runnable.h"
#include "Poco/AutoPtr.h"
#include "Poco/NotificationQueue.h"

using Tourist::Message::RemoteNode;
using Tourist::Message::MessageDispatcher;
using Tourist::Message::MessageNotification;

using Poco::Runnable;
using Poco::NotificationQueue;
using Poco::AutoPtr;

namespace Tourist {
//forwar declaration of LocalNode
  class LocalNode;
namespace Message {
  class MsgCacheXferRequest;
}
}

namespace Tourist {
namespace App {

class CacheXferWorker : public Runnable
{
public:

  /**
   * Creates a worker thread.
   *
   * @param contactNode Remote node to ask for nodes.
   * @param threadName Name of the thread
   * @param request Encapsulate the request parameters.
   * @param dispatcher Instance of message bus to use for message communication
   */
  CacheXferWorker (RemoteNode *contactNode, string threadName,
       Tourist::Message::MsgCacheXferRequest *request, MessageDispatcher *dispatcher,
       int timeout = 5);

  CacheXferWorker (RemoteNode *contactNode, string threadName, \
     Tourist::Message::MsgCacheXferRequest *remoteRequest, MessageDispatcher *dispatcher,
      LocalNode *localnode, int timeout = 5);
       
  ~CacheXferWorker();       
  
 //void changeState(int event);

  void stopWorker();
  
  void startWorker();

  bool isStopped();
  
  //call by thread
  void run();
  
  //Return the container holding the result of cache exchange.
  NodeSet getNodes();
  
  //Return the status of operation performed by this worker thread.
  int getStatus();
  
  /**
   * Callback method from the message bus (MessageDispatcher)
   * This method enquee the message object as delivered by dispatcher
   * thread this is to ensure the no delay for message delivery task.  
   */
  void onMessage(const AutoPtr<MessageNotification>& notification);
    
  int getTaskType();
    
  static int TASK_REMOTE_TO_LOCAL;//Transfer routing cache from remote node to local.  
  static int TASK_LOCAL_TO_REMOTE;//Transfer routing cache from local node to remote.
  
  static int STATUS_UNKNOWN; //Initial transfer status
  static int STATUS_TIMEDOUT;// Transfer failed as operation timedout. Usually this means
                             // an error in communication.
  static int STATUS_TIMEDOUT_PARTIAL; //Transfer failed as operation timedout. Usually this means
                         // that there was some partial transfer of cache.
  static int STATUS_CONNECT_FAILED; //indicates that remote node is not connected.                         
  static int STATUS_OK;
  
  //< state codes declaration > //
  const static int AWAIT_RESPONSE; //indicates that worker is waiting for the transfer to finish.
  const static int PROCESSING_RESPONSE; //indicates that worker is/was processing response
  
  //<event code>//
  static int FINISHED;
  static int RESPONSE_RECVD;
  static int RESPONSE_OK;
  static int RESPONSE_PARTIAL;
  static int STOPPED; //indicate that controller asked for stopping this thread

  void duplicate();
 
  void release();

private:

  //called to clean-up any resources used by this
  //thread.  
  void cleanUp();
  
  void processResponse();

  void processRequest();
  
  RemoteNode *contactNode;
  
  //suggest if this worker thread should run or not.
  //No gurantte is made that if the thread will actually be running
  //or not
  bool shouldbeRunning;
  
  //Indicates whether the thread is actually stopped or not
  bool stopped;
  
  //Name of the worker thread
  string threadName;
  
  Logger &logger;
  
  //Result of worker threads are put in this set to let controller
  //fetch it.
  NodeSet result;
  
  //status are mentioned in the controller class 'CacheXfer'
  int state, status;

  int timeout;
  
  //Notificaiton
  NotificationQueue queue;
  
  //Type of work this worker thread is going to do. Currently
  //these tasks includes (a) Asking a remote node to transfer its cache
  //with a specified cache type and range of levels. (b) To transfer
  //cache of LocalNode to remote node.
  int taskType;
  
  Tourist::Message::MsgCacheXferRequest *xferRequest;
  
  MessageDispatcher *dispatcher;

  Tourist::LocalNode *node;

  //for reference counting
  int rc;
  static int ref_count;
};
  
} // namespace App
} // namespace Tourist


#endif /*CACHEEXPORTERWORKER_*/
