#ifndef CACHEEXPORTER_
#define CACHEEXPORTER_

#include "Tourist/AvlCache.h"
#include "Tourist/LocalNode.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/MessageNotification.h"
#include "Tourist/Message/MessageDispatcher.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"
#include "Poco/AutoPtr.h"
#include "Poco/Runnable.h"
#include "Poco/ThreadPool.h"
#include "Poco/NotificationQueue.h"

using Tourist::Message::MessageNotification;
using Tourist::Message::MessageDispatcher;
using Tourist::Message::RemoteNode;

using Poco::Runnable;
using Poco::NObserver;
using Poco::Observer;
using Poco::AutoPtr;
using Poco::ThreadPool;
using Poco::NotificationQueue;

//forward declaration

namespace Tourist {
namespace Util {
class Config;
}
}

namespace Tourist {
namespace App {

class CacheXferWorker;
  
//typedef NodeCompSetTemplate <RemoteNode, CmpOrd<Node> > NodeSet;

/*
 * CacheXfer is responsible for handling transfer of nodes' cache
 * (a.k.a routing table) between different peers. It make use of thread 
 * pools for efficienty handling multiple requests.
 */

class CacheXfer : public Runnable
{
public:
  /**
   * Creats a CacheExport with default thread pool size
   */
  CacheXfer(Tourist::Util::Config *config);

  ~CacheXfer();

   /**
   * Handles synchronous transfer of node's cache.
   *
   * Request is handle synchronously and 'timeout' value in seconds
   * is used to control the wait period.
   *
   * Nodes transfered can be configured by specifying 'type' - allowed
   * values are PREFIX or SUFFIX.
   *
   * @param contactNode Node to ask for transfering its cache.
   * @param type Desired type of node (PREFIX | SUFFIX)
   * @param startLevel starting range of the cache
   * @param endLevel end range of the level
   * @param timeout (given in miliseconds) method will return when this 'timeout' is expired
   * @param result Transfer nodes will be copied into this container.
   */
  int getRemoteCache (RemoteNode* contactNode, int type, int startLevel,
       int endLevel, int timeout, Tourist::NodeSet &result);

  /**
   * Call back method
   */
  void onMessage(const AutoPtr<MessageNotification>& notification);

  void run();
  
  /* Status Message */
  static int STATUS_OK; //Transfer was successful
  
  static int STATUS_PARTIAL; //Transfer timeout prematurely.
    
  static int STATUS_NO_RESPONSE; //No response was obtained from remote node.
  
  static int STATUS_FAILED; //Some unforseen error occured.
  
private:

  void registerForRemoteReqs();

  void deregisterForRemoteReqs();

  MessageDispatcher *msgDispatcher;

  bool isStopped;

  int poolSize;

  ThreadPool threadPool;
  
  Logger &logger;

  vector< AutoPtr<CacheXferWorker> > workers;

  //queu for CacheXferWorker is different from this one, as it contains the
  // raw notification object and the worker's queue holds only the remote message
  // object.
  NotificationQueue queue;

  Tourist::Util::Config *config;
}; //class CacheXfer

}// namespace App
} //namespace Tourist

#endif /*CACHEEXPORTER_*/
