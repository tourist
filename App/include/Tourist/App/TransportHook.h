#ifndef PEERJOININGHANDLER_H_
#define PEERJOININGHANDLER_H_

#include "Tourist/Net/TransportFactory.h"
#include "Tourist/AbstractMessage.h"
#include "Poco/Mutex.h"
#include <map>

using Tourist::AbstractMessage;
using Tourist::Net::TransportFactory;

namespace Tourist {
namespace App {

/**
 * Starts network, manages the incoming stream of data from client and parse
 * the header information, extract pay-load and pass the message
 * to application.
 * 
 * Header information is processed based on the message-ID in the header and 
 * associated handler registered earlier by the application.
 */

class TransportHook {  
public:  
  TransportHook();
  
  ~TransportHook();
  
  /**
   * Register a message handler against a message-ID. This id is
   * used by this transport hook to decide what kind of message to 
   * use to process the message. 
   */
  int registerMessageHandler(int messageID, AbstractMessage* msg);
  
  /**
   * Unregister a message handler for a given message-ID.
   */
  int unregisterMessageHandler(int messageID);
  
private:
  
  void init();
  
  TransportFactory factory;
  typedef std::map<int, AbstractMessage*> HandlerMap;
  
  HandlerMap hanlders;
  mutable FastMutex mutex;
};

} // namespace App 
} // namespace Tourist

#endif /*PEERJOININGHANDLER_H_*/
