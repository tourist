#ifndef MULTICAST_H_
#define MULTICAST_H_

#include "Tourist/Message/RemoteNode.h"
#include "Tourist/LocalNode.h"
#include "Tourist/App/Application.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/MsgMulticast.h"
#include "Tourist/Message/MessageHeader.h"
#include "Poco/Runnable.h"
#include "Poco/NotificationQueue.h"


using Tourist::Message::RemoteNode;
using Tourist::Message::MessageDispatcher;
using Tourist::Message::MessageTypeData;
using Tourist::Message::MessageHeader;
using Tourist::Message::MsgMulticast;
using Tourist::App::Application;
using Poco::Runnable;
using Poco::NotificationQueue;

using Tourist::Message::MessageTypeData;


namespace Tourist {
namespace App {
	
class Multicast : public Runnable {
public:
	Multicast(Application *_app, int objCount);
	~Multicast();

	void run();
	void stop();

	
	//void levelChangeEvent(int newLevel, LocalNode *localNode);
	void levelChangeEvent(int newLevel, Node *localNode);
	int sendEvent(Node *source, Node *target, int event_type, int step);
	void receiveMulticastMsg();
	void onMulticastMsg(const AutoPtr<MessageNotification>& notification);
	void multicastRouting(Node* source, int event_type, int step);

private:
	void processQueue();

	bool shouldBeRunning, isStopped;
	Application *app;
	Logger &logger;
	NotificationQueue queue;

	//Used for Debug purpose
	int objCount; 
	const static bool debugFlag = true;
};

}
}

#endif /*MULTICAST_H_*/
