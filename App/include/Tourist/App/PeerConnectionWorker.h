#ifndef PEERCONNECTIONWORKER_H
#define PEERCONNECTIONWORKER_H


#include "Tourist/AbstractPipe.h"
#include "Tourist/Message/MessageProcessor.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Util/CallbackInterface.h"
#include "Poco/Runnable.h"
#include "Poco/Thread.h"
#include "Poco/Logger.h"
#include "Poco/Mutex.h"
#include "Poco/NotificationQueue.h"

using Tourist::Message::MessageProcessor;
using Tourist::Message::RemoteNode;
using Tourist::AbstractPipe;

using Poco::Runnable;
using Poco::Thread;
using Poco::FastMutex;
using Poco::NotificationQueue;

//forward declaration
namespace Tourist {
namespace Message {
}
}

namespace Tourist {
namespace App {

class Application;

/**
 * Handle peer handshake in a new thread. Part of thread pool
 * in Application class. 
*/

class PeerConnectionWorker : public Tourist::Util::CallbackInterface,
       public Runnable
{
 
public:
  PeerConnectionWorker (Application *app, AbstractPipe *pipe, RemoteNode &response,
          bool isActive, int timeout=5);


  ~PeerConnectionWorker();

  bool isStopped();

  bool isStatusOK();

  void stopWorker();

  //called upon the recept of MsgPeerAnnouncement message.
  void handleHandshake();

  //un-register pipe connect/disconnect handler
  void cleanUp();

  Tourist::Message::RemoteNode* getResult();

  void run();

  void callback(void *);

  //reference counting 
  void duplicate();

  void release();


private:  

  void handleHandshake(AbstractMessage *message);

  int sendResponse(bool isAck);

  Tourist::Message::MessageProcessor messageProcessor;

  Tourist::Message::MessageTypeData msgTypeData;

  AbstractPipe *pipe;
  
  //true value indicates that we are initiating peer handshake
  bool isActive;

  bool shouldBeRunning;
 
  bool stopped;
  //Set if we successfuly resolved the peer info
  bool statusOK;

  //peer connection should be resolved before this timeout value.
  int timeout;

  //Information of the connected node. 
  Tourist::Message::RemoteNode *result;  

  Logger &logger;

  static FastMutex mutex;

  //The node that should be sent as a response in a handshake.
  RemoteNode response;

  Application *parent;

  NotificationQueue queue;

  //for reference counting purpose.
  int rc;

  static int ref_count;
 
}; //class PeerConnectionWorker

} //namespace Application
} //namespace Tourist

#endif /*PEERCONNECTIONWORKER_H*/

