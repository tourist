#ifndef PEERJOINING_H_
#define PEERJOINING_H_

#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/MessageDispatcher.h"
#include "Tourist/LocalNode.h"

using Tourist::Message::RemoteNode;
using Tourist::Message::MessageDispatcher;

namespace Tourit {
namespace Util {
class Config;
}
namespace Net {
class TransportFactory;
}
}

namespace Tourist {
namespace App {

//typedef NodeCompSetTemplate <RemoteNode, CmpOrd<Node> > NodeSet; 

/**
 * Handles peer handshake by exchange peer announcement method.
 */

class PeerProtocol
{
public:  
  PeerProtocol(Config* config, TransportFactory* transport);
  
  ~PeerProtocol();

  RemoteNode* doPeerProtocol(string host, int port, int protocol=TCP);
  
private:
  
  //helper class for configuring tourist engine.
  int init();

  Config *config;

  TransportFactory *transport
  
};

} // namespace App 
} // namespace Tourist

#endif /*PEERJOINING_H_*/
