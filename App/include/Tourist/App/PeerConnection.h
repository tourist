#ifndef PEERCONNECTION_H
#define PEERCONNECTION_H

#include "Tourist/AbstractPipe.h"
#include "Tourist/Util/CallbackInterface.h"
#include "Poco/ThreadPool.h"
#include "Poco/Runnable.h"
#include "Poco/Logger.h"
#include "Poco/NotificationQueue.h"

using Tourist::AbstractPipe;
using Poco::ThreadPool;
using Poco::Logger;
using Poco::Runnable;
using Poco::NotificationQueue;

namespace Tourist {
namespace Net {
class TransportFactory;
}

namespace Message {
class RemoteNode;
}
namespace Util {
class Config;
}
}

namespace Tourist {
namespace App {

/**
 * Handles connection to/from remote peers.
 *
 * Other components use its methods to connect to remote peers.
 * Request for a request is passed on to a worker thread that 
 *   initiates a peer handshake (exchanging peer information).
 */

class PeerConnection : public Runnable, Tourist::Util::CallbackInterface
{
public :
  PeerConnection(Tourist::Net::TransportFactory *transport, Tourist::Message::RemoteNode *node);

  ~PeerConnection();

  int connectionTo(string host, int port, int protocol,
      Tourist::Message::RemoteNode *response, int timeout=500);  

  void run();

  void callback(void *data);

private: 
  
  //reply object for remoteNode.
  Tourist::Message::RemoteNode *localNodeInfo;

  Tourist::Net::TransportFactory *transport;
  
  int poolSize;

  bool shouldBeRunning;

  ThreadPool threadPool;

  Logger &logger;

  NotificationQueue queue;

};

}
}

#endif
