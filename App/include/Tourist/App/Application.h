#ifndef APP_H_
#define APP_H_

#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/MessageDispatcher.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Net/TransportFactory.h"
#include "Tourist/LocalNode.h"
#include "Tourist/Util/Config.h"
#include "Tourist/App/CacheXfer.h"
#include "Poco/Runnable.h"
#include "Poco/NotificationQueue.h"
#include "Poco/ThreadPool.h"
#include <vector>

using Tourist::Message::RemoteNode;
using Tourist::Message::MessageDispatcher;
using Tourist::Message::MessageTypeData;
using Tourist::Net::TransportFactory;
using Tourist::Util::Config;

using Poco::Runnable;
using Poco::ThreadPool;
using Poco::NotificationQueue;

namespace Tourist {
namespace App {

class PeerConnectionWorker;

class Bootstrap;

class Bootstrapper;

//typedef NodeCompSetTemplate <RemoteNode, CmpOrd<Node> > NodeSet; 

/**
 * Main entry point into the application. Starts by reading configuration
 * file, fire up the underlying transport and handles connection requests from
 * the transport factory. The logic for handling request is done through remote-node
 */

//TODO The current callback interface don't use any specialized structure for notifying
// connection notification. This makes it impossible to use along with the pipe notifications.
//
struct TransportInfo {
  string host;
  int port;
  int type; //TCP or UDP or
};

class Application : public CallbackInterface, public Runnable
{
public:  
   Application();

   Application(Config conf);
  
  ~Application();

  int connectionTo(string host, int port, int protocol, RemoteNode **response,
           int timeout=500);

  int getRemoteCache(RemoteNode * contactNode, int cacheType, int startLevel, int endLevel,
           int timeout, Tourist::NodeSet &result);

  bool searchRemoteLinks(Node& recipient, NodeSet& result,
       unsigned int flags=HIGHER_LEVELS|EQUAL_LEVELS|LOWER_LEVELS|
                          FIND_SUFFIXES|FIND_PREFIXES|ONE_LEVEL_LOWER);

	Node* getNextHop(NodeSet &audienceSet, Node *source, int step, int type);

	void setLevel(int newLevel);	

	
	//Interface (CallbackInterface) method for connection call back
  void callback(void *pipe); 

  void addNewNode(RemoteNode *node);

  int getNodeCount(int level, unsigned int flags=PREFIX|SUFFIX);

  bool getTopNode(Node &query, Node **result, int type);

  bool getForwardingNode(Node& query, Node **result, int type);

  bool getConnection(RemoteNode query, RemoteNode **rConnection);

  void getLocalNodeInfo(RemoteNode &rn);

  //Returns structure of type transport for all currently available transports.
  vector<TransportInfo> getEnabledTransports();

  Config* getConfig();

  //Should be called to initialize the Application.
  int init();

  void run();

  void stopApp();

  //Bandwidth this peer is consuming.
  int getBwUsage();
  
private:
  void updatePipesCache(RemoteNode &rn, AbstractPipe *pipe);
  
  //Application main configuration object.
  Config config;  
  
  //main system bus for message exchange.
  MessageDispatcher messageBus;

  //handles the message types
  MessageTypeData messageTypeData;
  
  //cache for remote pipes
  map <RemoteNode, AbstractPipe*> networkPipes;
  
  Logger &logger;
  
  //We need to keep reference to those nodes who haven't join our network
  //or we waiting to receive more information about them.
  Tourist::NodeSet nodesInTransaction;
    
  TransportFactory transport;

  bool shouldBeRunning;

  //handle peer handshake requests/responses.
  vector< AutoPtr<PeerConnectionWorker> > workers;

  ThreadPool threadPool;

  //All tourist related manipulation (routes, nodes etc) 
  //will be handled by this cache object.
  LocalNode nodeCache;

  CacheXfer cacheXfer;

  NotificationQueue queue;

  Poco::Thread cacheRun;

  Poco::Thread bootRun;

  Bootstrap *bootstrap;

  Bootstrapper *bootstrapper;

  //time when this application was initiated
  //This will be used for measuring bandwidth
  //usage by this peer.
  time_t startTime;
  
};

} // namespace App 
} // namespace Tourist

#endif /*APP_H_*/
