#ifndef BOOTSTRAP_H_
#define BOOTSTRAP_H_

#include "Tourist/Message/MessageNotification.h"
#include "Poco/Runnable.h"
#include "Poco/NotificationQueue.h"
#include "Tourist/LocalNode.h"
#include "Poco/AutoPtr.h"
#include "Poco/Logger.h"

using Tourist::Message::MessageNotification;
using Poco::Runnable;
using Poco::NotificationQueue;
using Poco::AutoPtr;
using Poco::Logger;

//forward declaration
namespace Tourist {
namespace Message {
  class RemoteNode;
}
namespace Util {
  class Config;
}
namespace App {

class Application;

class Bootstrap : public Runnable
{
public:
  Bootstrap(Tourist::App::Application *app);

  ~Bootstrap();
  
  void run();

  void onMessage(const AutoPtr<MessageNotification>& notification);

  void stop();

  bool getIsStopped();

  /**
   * Call the app-> getConfig() method to assign the 
   * local config object. But, ensure that app is not NULL
   */
  void setConfig(Application *app);

  const static int BOOT_INIT; //bootstrap is initialized

  const static int BOOT_OK; // boot is ok

  const static int BOOT_TIMEOUT; //timeout occured before we
                                 //could handle anything
  
private:

  void processQueue();

  void postProcessBoot();

  Tourist::App::Application *parentApp;

  NotificationQueue queue;

  bool shouldBeRunning, isStopped;

  int topPrefixStatus, topSuffixStatus;

  //top prefix and suffix node as resolves
  //through this boot process. These are referenced
  //once a top prefix and/or top suffix node is located
  Tourist::Message::RemoteNode *topPrefixNode, *topSuffixNode;

  //Boot status, set at different stages during
  //the whole boot process
  int status;

  //prefix routing cache received from top
  //prefix node
  Tourist::NodeSet prefixCache;

  //suffix routing cache received from top
  //suffix node
  Tourist::NodeSet suffixCache;

  //the new level that will be calculated based on the
  //current level and bandwidth usage of boot node that
  //we contacted.
  int newLevel;

  int rNodeBWUsage;

  Tourist::Util::Config * config;

  Logger &logger;
  
};

} //namespace App
} //namespace Tourist

#endif /*BOOTSTRAP_H_ */
