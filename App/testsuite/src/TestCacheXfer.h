#ifndef TESTCACHEXFER_H_
#define TESTCACHEXFER_H_

#include "CppUnit/TestCase.h"

class TestCacheXfer: public CppUnit::TestCase
{
public:
  TestCacheXfer(const std::string& name);
  ~TestCacheXfer();

  void setUp();
  void tearDown();
  void testGetRemoteCache();
  static CppUnit::Test* suite();
};

#endif /*TESTCACHEXFER_H_*/
