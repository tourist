#ifndef TESTPEERHANDSHAKE_H_
#define TESTPEERHANDSHAKE_H_

#include "CppUnit/TestCase.h"

class TestPeerHandshake: public CppUnit::TestCase
{
public:
  TestPeerHandshake(const std::string& name);

  ~TestPeerHandshake();

  void setUp();

  void tearDown();

  void testHandshake();

  static CppUnit::Test* suite();
};

#endif /*TESTPEERHANDSHAKE_H_*/
