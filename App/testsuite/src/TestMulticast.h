#ifndef TESTMULTICAST_H_
#define TESTMULTICAST_H_

#include "CppUnit/TestCase.h"

class TestMulticast: public CppUnit::TestCase
{
public:
  TestMulticast(const std::string& name);

  ~TestMulticast();

  void setUp();

  void tearDown();

  void testSendEvent();
  
	void testCompleteMulticast();

  static CppUnit::Test* suite();
};

#endif /*TESTMULTICAST_H_*/
