/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestCacheXfer.h"
#include "Tourist/Id.h"
#include "Tourist/TouristExceptions.h"
#include "Tourist/App/Application.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"

using Tourist::Id;
using Tourist::InvalidArgumentException;
using Tourist::App::Application;
using Poco::Thread;

using namespace Tourist::App;

TestCacheXfer::TestCacheXfer(const std::string& name): CppUnit::TestCase(name)
{
}

TestCacheXfer::~TestCacheXfer()
{
}

void TestCacheXfer::setUp()
{
}

void TestCacheXfer::tearDown()
{
}

void TestCacheXfer::testGetRemoteCache()
{
  
  Config config_1;
  config_1.setTCPEnable(true);
  config_1.setTCPPort(5000);
  Application app1(config_1);


  Config config_2;
  config_2.setTCPEnable(true);
  config_2.setTCPPort(5010);
  Application app2(config_2);

  assert (app1.init() == 0);
  assert (app2.init() == 0);

  Thread t1, t2;
  t1.start(app1);
  t2.start(app2);

  Thread::sleep(20);
  //connect to remote-node of application-1 with application-2
  RemoteNode *rn = NULL;
  vector<TransportInfo> transportInfo;
  transportInfo = app1.getEnabledTransports();

  assert (transportInfo.size() != 0);

  TransportInfo tInfo = transportInfo.at(0);
  int status = app2.connectionTo(tInfo.host, tInfo.port, tInfo.type, &rn, 10);
  assert (status == 0);
  assert (rn != NULL);
  assert (rn -> getHost() == tInfo.host);
  assert (rn -> getPort() == tInfo.port);
  assert (rn -> getProtocol() == tInfo.type);
  assert (rn -> getPipe() != NULL);

  //add the nodes to app1
  RemoteNode rn1, rn2, rn3;
  app1.addNewNode(&rn1);
  app1.addNewNode(&rn2);
  app1.addNewNode(&rn3);

  Tourist::NodeSet result;
  status = app2.getRemoteCache(rn, PREFIX, 0, 5, 10, result);
  assert(status == 0);
  assert(result.treeSize == 4);

  app1.stopApp();
  app2.stopApp();

  t1.join();
  t2.join();
}

CppUnit::Test* TestCacheXfer::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestCacheXfer");
  CppUnit_addTest(pSuite, TestCacheXfer, testGetRemoteCache);

  return pSuite;
}
