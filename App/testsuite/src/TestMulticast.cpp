#include "TestMulticast.h"
#include "Tourist/App/Application.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "Poco/Thread.h"
#include "Tourist/App/Multicast.h"
#include "Tourist/LocalNode.h"

using Tourist::App::Application;
using Poco::Thread;
using Tourist::LocalNode; //remove it

using namespace Tourist::App;

TestMulticast::TestMulticast(const std::string& name): CppUnit::TestCase(name)
{
}

TestMulticast::~TestMulticast()
{
}

void TestMulticast::setUp()
{
}

void TestMulticast::tearDown()
{
}

void TestMulticast::testSendEvent()
{
  
	Config config_1;
  config_1.setTCPEnable(true);
  config_1.setTCPPort(5000);
  
  Application app1(config_1);
  assert(app1.init() == 0);

  Config config_2;
  config_2.setTCPEnable(true);
  config_2.setTCPPort(5010);
  Application app2(config_2);

  assert (app2.init() == 0);

	//---> Prob here . NEED IMMEDIATE FIX
	//app1.setLevel(0);
	//app2.setLevel(2);


	Thread t1, t2;
  t1.start(app1);
  t2.start(app2);
  
  Thread::sleep(20); 
  
	int i=0;

	RemoteNode app1Node, app2Node;
	app1.getLocalNodeInfo(app1Node);
	app2.getLocalNodeInfo(app2Node);
	std::cout<<"\nApp1 Node Info: " << app1Node.toString() << "\n";
	std::cout<<"\nApp2 Node Info: " << app2Node.toString() << "\n";
	
	app2.addNewNode(&app1Node);
	
	Multicast multicast(&app1, 1);	
	Multicast multicast2(&app2, 2);

	Thread tm1, tm2;
  tm1.start(multicast);
  tm2.start(multicast2);

	multicast2.levelChangeEvent(3, &app2Node);

  Thread::sleep(100); 
	
  app1.stopApp();
  app2.stopApp();

  //wait for all the threads to finish
  t1.join();
  t2.join();

}

void TestMulticast::testCompleteMulticast()
{
	Config config_1;
  config_1.setTCPEnable(true);
  config_1.setTCPPort(5000);
  
  Application app1(config_1);
  assert(app1.init() == 0);

  Config config_2;
  config_2.setTCPEnable(true);
  config_2.setTCPPort(5010);
  Application app2(config_2);

  assert (app2.init() == 0);
	
	//app1.setLevel(0);
	//app2.setLevel(2);


	Config config_3;
  config_3.setTCPEnable(true);
  config_3.setTCPPort(5020);
  
  Application app3(config_3);
  assert(app3.init() == 0);


	Config config_4;
  config_4.setTCPEnable(true);
  config_4.setTCPPort(5030);
  
  Application app4(config_4);
  assert(app4.init() == 0);


	Thread t1, t2, t3, t4;
  t1.start(app1);
  t2.start(app2);
	t3.start(app3);
	t4.start(app4);
  

  Thread::sleep(20); 
  
	int i=0;

	
	RemoteNode app1Node, app2Node, app3Node, app4Node;
	app1.getLocalNodeInfo(app1Node);
	app2.getLocalNodeInfo(app2Node);
	app3.getLocalNodeInfo(app3Node);
	app4.getLocalNodeInfo(app4Node);

	std::cout<<"\nApp1 Node Info: " << app1Node.toString() << "\n";
	std::cout<<"\nApp2 Node Info: " << app2Node.toString() << "\n";
	std::cout<<"\nApp3 Node Info: " << app3Node.toString() << "\n";
	std::cout<<"\nApp4 Node Info: " << app4Node.toString() << "\n";
	
	app2.addNewNode(&app1Node);
	app1.addNewNode(&app3Node);
	app1.addNewNode(&app4Node);
	app1.addNewNode(&app2Node);
	
	Multicast multicast(&app1, 1);	
	Multicast multicast2(&app2, 2);
	Multicast multicast3(&app3, 3);	
	Multicast multicast4(&app4, 4);


	Thread tm1, tm2, tm3, tm4;
  tm1.start(multicast);
  tm2.start(multicast2);
  tm3.start(multicast3);
  tm4.start(multicast4);

	multicast2.levelChangeEvent(3, &app2Node);

	
  Thread::sleep(5000);

	app1.stopApp();
  app2.stopApp();
  app3.stopApp();
  app4.stopApp();

  //wait for all the threads to finish
  t1.join();
  t2.join();
	t3.join();
	t4.join();
}

CppUnit::Test* TestMulticast::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestMulticast");
  //CppUnit_addTest(pSuite, TestMulticast, testSendEvent);
  CppUnit_addTest(pSuite, TestMulticast, testCompleteMulticast);
	

  return pSuite;
}
