#ifndef MESSAGENOTIFICATION_H_
#define MESSAGENOTIFICATION_H_

#include "Tourist/Message/RemoteNode.h"
#include "Tourist/AbstractMessage.h"
#include "Poco/Notification.h"

using Poco::Notification;

namespace Tourist {
namespace Message {

class RemoteNode;

/**
 * Represents a notification message within Tourist.
 */
class MessageNotification : public Notification
{
public:
  MessageNotification()
  {
  }

  MessageNotification(AbstractMessage* msg, RemoteNode *sender)
  {
    this->message = msg;
    this->sender  = sender;
  }

  ~MessageNotification()
  {   
    delete message;
    //delete sender;
  }

  AbstractMessage *message;

  RemoteNode *sender;
};

} // namespace Message
} // namespace Tourist

#endif /*MESSAGENOTIFICATION_H_*/
