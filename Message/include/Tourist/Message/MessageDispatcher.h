#ifndef MESSAGENOTIFIER_
#define MESSAGENOTIFIER_

#include "Tourist/AbstractMessage.h"
#include "Tourist/Message/RemoteNode.h"
#include "Poco/AbstractObserver.h"
#include "Poco/NotificationCenter.h"
#include "Poco/Logger.h"
#include "Poco/Mutex.h"
#include <vector>
#include <set>

using Poco::AbstractObserver;
using Poco::NotificationCenter;
using Poco::FastMutex;
using Poco::Logger;

///Dispatcher mechanism is based on what Poco::SocketReactor
///used.

namespace Tourist {
namespace Message {

class MessageNotification;

class MessageNotifier
{
public:
  MessageNotifier();

  ~MessageNotifier();

  void addObserver(MessageNotification *msgNotification, const Poco::AbstractObserver& observer);

  void removeObserver(MessageNotification *msgNotification, const Poco::AbstractObserver& observer);

  void dispatch(MessageNotification* msgNotification);

  bool hasObservers() const;

private:

  //typedef std::multiset<MessageNotification*> MsgSet;

  //MsgSet _set;

  Logger &logger;

  Poco::NotificationCenter _nc;
};

/**
 * A messaging backbone for Tourist.
 *
 * Different componets among Tourist register
 */
class MessageDispatcher
{
public:
  
  MessageDispatcher();
  
  ~MessageDispatcher();

  /**
   * Register call back method for a particular components.
   *
   * \param component Name of the compoenent.
   * \param observer Call back method.
   */
  void registerNotification(string component, const Poco::AbstractObserver& observer);

  /**
   * Register call back method for a list of components.
   *
   * \param components List of components.
   * \param observer Callback method.
   */
  void registerNotifications(const vector<string> components, const Poco::AbstractObserver& observer);

  /**
   * Sends the Message Notification object to destined component(s)
   *
   * The destination is decided based on the information encoded inside the
   * notification object (component name)
   *
   * \param msg
   */
  void dispatchEvent(MessageNotification *msg);

  /**
   * Remove the component from registration queue.
   *
   * \param component Name of the component.
   */

   void deregisterNotification(string component, const Poco::AbstractObserver& observer);

   /**
    * Removes registration for multiple componets
    */

   void deregisterNotifications(const vector<string> components, const Poco::AbstractObserver& observer);

   /**
    * Number of registered call backs
    */
   int getEventCount();

private:
  typedef std::map<string, MessageNotifier*> EventHandlerMap;
  static MessageDispatcher* _instance;
  EventHandlerMap _handlers;
  static FastMutex _mutex;
  static Logger &logger;
};

} //namespace Message
} //namespace Tourist

#endif /*MESSAGENOTIFIER_*/
