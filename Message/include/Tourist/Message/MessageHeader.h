#ifndef MESSAGEHEADER_H_
#define MESSAGEHEADER_H_

#include "hessian_input.h"
#include "Poco/Logger.h"

namespace Tourist {
namespace Message {

/**
 * Encapsulate fields that are part of header of each message
 */
class MessageHeader
{  
public:
  MessageHeader();
  
  MessageHeader(hessian::hessian_input &hi);
  
  /**
   * Creates a new message header object with given information
   * 
   * @param version Version of header or client
   * @param payloadSize size of payload
   * @param payloadType type of payload, usualy it is used to identify
   *         what kind of object is in the payload.
   */
  MessageHeader(int version, int payloadSize, int payloadType);
  
  ~MessageHeader();
  
  int getString(string &serialized);
  
  bool isInErrorState();
  
  int setData(char *data, int num);
  
  int getVersion();
  
  int getPayloadSize();
  
  int getPayloadType();
  
  const static int HEADER_SIZE;
  
  const static int HEADER_VERSION;
  
private:
  bool inError;  

  int version, payloadSize, payloadType;
  
  Poco::Logger &logger;
  
  void doDeserialize(hessian::hessian_input& hi);

};

} // namespace Message
} // namespace Tourist

#endif /*MESSAGEHEADER_H_*/

