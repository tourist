#ifndef MSGPEERANNOUNCEMENT_H_
#define MSGPEERANNOUNCEMENT_H_

#include "Tourist/AbstractMessage.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"
#include "RemoteNode.h"
#include "Poco/Logger.h"

using Tourist::AbstractMessage;
using Poco::Logger;

using namespace hessian;

namespace Tourist {
namespace Message {

/**
 * Specify the peer-information send at a start of handshake. More 
 * formally,  this class is part of a peer announcement protocol by which
 * a peer tells its information (128bit id, level, ip, hostname) to another node.
 * If the remote node accepts the connection then the MsgPeerAnnouncment is 
 * sent back with the ack field set to true
 */

class MsgPeerAnnouncement : public AbstractMessage 
{  
public:

  MsgPeerAnnouncement();
  
  MsgPeerAnnouncement(string compName, string sourceCompName, RemoteNode sender);
  
  MsgPeerAnnouncement(hessian_input &hi);
  
  ~MsgPeerAnnouncement();
  
  int getString(string &serialized);
  
  int getType();
  
  string getComponentName();
 
  void setComponentName(string &component);

  string getSourceCompName();

  void setSourceCompName(string &sourceComp);

  int setData(char *data, int length);
   
  bool isInErrorState();

  RemoteNode& getSendingPeer();
  
  void setSendingPeer(RemoteNode& rn);

  bool getIsAck();

  void setIsAck(bool flag);
  
private:

  void doDeserialize(hessian_input &hi);
  
  RemoteNode senderPeer;   
  
  string componentName, sourceCompName;

  bool errorState, isAck;

  Logger &logger;  
    
};

} //namespace Message
} //namespace Tourist

#endif /*MSGPEERANNOUNCEMENT_H_*/
