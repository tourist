#ifndef MESSAGEPROCESSOR_H_
#define MESSAGEPROCESSOR_H_

#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/MessageHeader.h"
#include "Poco/Logger.h"
#include <queue>

using Poco::Logger;

namespace Tourist {
namespace Message {

/**
 * Dynamically creates message objects via the byte
 * read from a stream. It is done by reading the header bytes
 * , identifying the type of the message and instantiating desired
 * objects. 
 */

class MessageProcessor 
{
public:
  MessageProcessor();

  MessageProcessor(MessageTypeData *messageTypeHandler);

  ~MessageProcessor();

  void addBytes(char *bytes, int len);

  AbstractMessage* popMessage();

  void setMsgTypeHandler(MessageTypeData *messageTypeHandler);

private:

  void processHeader();

  void processData();
  //Queue for holding message objects
  std::queue<AbstractMessage*> messages;  

  MessageTypeData *msgTypeHandler;

  //pointer to header based on which we are processing data.
  MessageHeader *currentHeader;

  /* Buffer to hold incomplete bytes */
  char* buffer;

  /* Number of bytes currently inside the buffer */
  int bufferlen;   

  /** Indicates that we have received header for current
   *  conversation and should treating incoming bytes as payload     
   */   
  bool waitingForHeader;

  Logger &logger;
};

}
}

#endif
