#ifndef PIPENOTIFICATION_H_
#define PIPENOTIFICATION_H_

#include "Tourist/AbstractPipe.h"
#include "Poco/Notification.h"

using Tourist::AbstractPipe;
using Poco::Notification;

namespace Tourist {
namespace Message {

/**
 * Delivers a pipe object as a notification.
 */
class PeerNotification : public Notification
{
public:
  PeerNotification()
  {
  }

  PeerNotification(AbstractPipe *pipe)
  {
    //the ownership of the pipe will be taken over by the notified object.
    this -> pipe = pipe;
  }

  ~PeerNotification()
  {     
  }

  AbstractPipe *pipe;

};

} // namespace Message
} // namespace Tourist

#endif /*MESSAGENOTIFICATION_H_*/
