#ifndef MSGBOOTSTRAPREPLY_
#define MSGBOOTSTRAPREPLY__

#include "Tourist/Message/RemoteNode.h"
#include <string>
#include "hessian_input.h"
#include "hessian_output.h"
#include "Poco/Logger.h"

using Tourist::Message::RemoteNode;
using Poco::Logger;

using namespace hessian;

namespace Tourist {
namespace Message {

/**
 * Donates a bootstrap request, asking a remote peer for
 *   requestee top level prefix and suffix node
 */
class MsgBootstrapReply : public AbstractMessage
{

public:
  MsgBootstrapReply();

  MsgBootstrapReply(string compName, string sourceCom, int sTpP, RemoteNode &tp, 
      int sTpS, RemoteNode &ts);

  MsgBootstrapReply(hessian_input &hi);

  ~MsgBootstrapReply();

  int getString(string &name);

  int getType();

  string getComponentName();

  bool isInErrorState();

  void setComponentName(string compName);

  string getSourceCompName();

  void setSourceCompName(string comp);

  int setData(char *data, int size);

  int getMessageID();

  void setMessageID(int msgID);

  int getStatusTopPrefix();

  void setStatusTopPrefix(int t);

  RemoteNode& getTopPrefix();

  void setTopPrefix(RemoteNode& tp);

  int getStatusTopSuffix();

  void setStatusTopSuffix(int t);

  RemoteNode& getTopSuffix();

  void setTopSuffix(RemoteNode& ts);

  const static int INCLUDED;
  const static int FORWARDED;
  const static int NONE;

private:

  void doDeserialize(hessian_input &hi);
  
  string componentName;
  
  string sourceCompName;

  RemoteNode topPrefix, topSuffix;

  //type of request top_prefix, top_suffix, both

  bool errorState;

  /*
   *  Indicates whether the top prefix/suffix nodes are 
   *  included in the response (or) a rdirect request has been 
   *  sent to another node (or) top prefix cann't be resolves at 
   *  this point
   */
  int statusTopPrefix, statusTopSuffix;

  int messageID;

  Logger &logger;
};

}
}

#endif /*MSGBOOTSTRAPREPLY_H_ */
