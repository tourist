#ifndef MSGMULTICAST_H_
#define MSGMULTICAST_H_

#include "Tourist/AbstractMessage.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"
#include "RemoteNode.h"
#include "Tourist/Node.h"
#include "Poco/Logger.h"

using Tourist::AbstractMessage;
using Poco::Logger;

using namespace hessian;

namespace Tourist {
namespace Message {
//TODO: Change Description
/**
 * Specify the peer-information send at a start of handshake. More 
 * formally,  this class is part of a peer announcement protocol by which
 * a peer tells its information (128bit id, level, ip, hostname) to another node.
 * If the remote node accepts the connection then the MsgPeerAnnouncment is 
 * sent back with the ack field set to true
 */

class MsgMulticast : public AbstractMessage 
{  
public:

  MsgMulticast();
  
  MsgMulticast(string compName, string sourceCompName, RemoteNode sender, int eventType, int step);
  
  MsgMulticast(hessian_input &hi);
  
  ~MsgMulticast();
  
  int getString(string &serialized);
  
  int getType();
  
  string getComponentName();
 
  void setComponentName(string &component);

  string getSourceCompName();

  void setSourceCompName(string &sourceComp);

  int setData(char *data, int length);
	
	bool isInErrorState();
   

  RemoteNode getInitiatingPeer();
  
  void setInitiatingPeer(RemoteNode rn);

	void setEventType(int eventType);

	int getEventType();

	int getStep();

	void setStep(int step);

  
private:

  void doDeserialize(hessian_input &hi);
  
  RemoteNode initiatingPeer;
  
  string componentName, sourceCompName;

	int eventType;

	int step;

  bool errorState;

	Logger &logger;  
    
};

} //namespace Message
} //namespace Tourist

#endif /*MSGPEERANNOUNCEMENT_H_*/
