#ifndef MESSAGETYPEDATA_
#define MESSAGETYPEDATA_

#include "Tourist/AbstractMessage.h"
#include "Poco/DynamicFactory.h"
#include "Poco/Instantiator.h"

using Tourist::AbstractMessage;
using Poco::DynamicFactory;
using Poco::AbstractInstantiator;

namespace Tourist {
namespace Message {

/**
 * Handles mapping between message type and actual object that
 * contains logic for serialization and deserialization for that
 * object.
 */

class MessageTypeData 
{
public:
  MessageTypeData();
  
  ~MessageTypeData();
  
  typedef AbstractInstantiator<AbstractMessage> MessageInstantiator;
  
  int registerMessageObj(int type, MessageInstantiator * instance);
  
  AbstractMessage* createInstance(int type);
  
  // cache transfer reuqest object id
  const static int CACHE_XFER_REQ;
  
  // cache transfer reply object id
  const static int CACHE_XFER_REP;

  //boot strap request method id
  const static int BOOTSTRAP_REQ;

  const static int BOOTSTRAP_REP;

  const static int PEER_ANNOUNCE;
	
	const static int MULTICAST;
  
private:
  DynamicFactory<AbstractMessage> dynFactory;
  
  //register builtin message objects. 
  void registerBuiltIns();
  
  
};
  
} // namespace Message
} // namespace Tourist

#endif /*MESSAGETYPEDATA_*/
