#ifndef REMOTENODE_H_
#define REMOTENODE_H_

#include "Tourist/Node.h"
#include "Tourist/AbstractPipe.h"
#include "Tourist/AbstractMessage.h"
#include "Tourist/Util/CallbackInterface.h"
#include "Tourist/Message/MessageDispatcher.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/MessageProcessor.h"
#include "Poco/Logger.h"
#include "Poco/Mutex.h"

using Tourist::AbstractPipe;
using Tourist::Node;
using Tourist::AbstractMessage;

using Poco::FastMutex;
using Poco::Logger;

/**
 * Represent a remote peer.
 */

namespace Tourist {
namespace Net{

class BufferNotification;

}

namespace Util {
class Config;
}

}


namespace Tourist {
namespace Message {

class MessageTypeData;
class MessageDispatcher;

class RemoteNode: public Node, public Tourist::Util::CallbackInterface
{
public:
  /**
   * Create a new remote node with zero-id
   */
  RemoteNode();
  
  RemoteNode(Tourist::Util::Config *config);

  /**
   * Creates a new remote node with given id and level
   * \param id String representation of 128 bit hex number
   * \param level
   */
  RemoteNode(Tourist::Util::Config *config, string id, int level);

  /**
   * Create a new remote node.
   * \param id String representation of 128 bit hex number
   * \param level
   * \param host Host to which this node belongs
   * \param port for this node.
   */

  RemoteNode(Tourist::Util::Config *config, string id, int level, string host,
      int port, int protocol=TCP);
  
  /**
   * Copy Constructor
   */
  RemoteNode(const RemoteNode& rn);

  ~RemoteNode();
  
  RemoteNode& operator=(const RemoteNode& rn);

  void setConfig(Tourist::Util::Config *config);

  /**
   * Send the message to the host presented by this object.
   *
   * \param message Implementation of AbstractMessage object.
   * \return >0 for success <0 for failure.
   */
  const int sendMessage(MessageHeader *header, AbstractMessage *message);

  void setPipe(AbstractPipe *pipe);

  AbstractPipe* getPipe();

  void setHost(string hostName);

  string getHost();

  int getProtocol();

  void setProtocol(int proto);

  int getPort();

  void setPort(int port);

  /**
   * Returns a string representation of this node.
   * Format of the output:
   *   name-id:level-host:port
   */
  string toString();
  
  //For receiving notification for data and disconnect.
  void callback(void *data);

  int getStatus();

  ///Returns the current number of bytes going in and out 
  /// of this node.
  int getBwUsage();

  void setBwUsage(int bw);

  const static int STATUS_NOOP;

  const static int STATUS_CONNECTED;

  const static int STATUS_DISCONNECTED;

private:

  MessageTypeData *messageTypeHandler;
  
  MessageDispatcher *messageBus;

  MessageProcessor messageProcessor;
  
  //bw cost for this node.
  string host;

  int bwUsage, port, protocol;

  int status;

  AbstractPipe *pipe;  
  
  Logger &logger;
  
  static FastMutex mutex;  

  
  void init(Tourist::Util::Config *config);   
};

}//namespace Message
}//namespace Tourist


#endif /*REMOTENODE_H_*/
