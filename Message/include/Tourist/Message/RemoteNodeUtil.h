#ifndef REMOTENODEUTIL_H_
#define REMOTENODEUTIL_H_

#include "Tourist/Message/RemoteNode.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"
#include <string>

using namespace std;
using namespace hessian;

namespace Tourist {
namespace Message {

// Id + Transport information about node sent with each msg.
/// Throws Poco::Exception
string getNodeHeader(hessian_output &ho, string &rpc, RemoteNode &node);

/// Read the message for sender's information.
/// Throws Poco::Exception
void nodeFromHeader(hessian_input &hi, RemoteNode &rn);

} // namespace Message {
} // namespace Tourist

#endif /*REMOTENODEUTIL_H_*/
