#ifndef MSGCACHETRANSFERREQUEST_
#define MSGCACHETRANSFERREQUEST_

#include "Tourist/AbstractMessage.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"
#include "Poco/Logger.h"

using Tourist::AbstractMessage;
using Poco::Logger;

using namespace hessian;

namespace Tourist
{
namespace Message
{

/**
 * Specify the semantics of a cache transfer request. The
 * significant attributes includes:
 * message ID (msgID: int)
 * start level (startLevel: int)
 * end level (endLevel: int)
 * owner (RemoteNode)
 * 
 * The serialization/deserialization is achieved by encapsulating
 * the hessian_in or hessian_out objects.
 */

class MsgCacheXferRequest : public AbstractMessage
{
public:
	/**
	 * Create an empty trnasfer request object
	 */
  MsgCacheXferRequest();

	/**
	 * Create a request object with given attributes
	 */
  MsgCacheXferRequest(string componentName, string sourceComp, int sLevel,
		  int endLevel, int cacheType);

  /**
   * Extract the message fields from the hessian stream.
   */
  MsgCacheXferRequest(hessian_input &hi);

  ~MsgCacheXferRequest();

  /**
   * Returns the serialized form of this message (generated via hessian).	 * 
   */
  int getString(string &name);

  /**
   * Type of the underlying format (Hessian in this case).
   */

  int getType();

  /**
   * Name of the Tourist component to which this message is addressed
   */
  string getComponentName();
  
  bool isInErrorState();
  
  void setComponentName(string component);

  string getSourceCompName();

  void setSourceCompName(string component);
  
  int setData(char* data, int size);

  /**
   * Type of remote node cache (prefix, suffix etc).
   */
  int getCacheType();

  void setCacheType(int type);

  int getStartLevel();

  void setStartLevel(int sLevel);

  int getEndLevel();

  void setEndLevel(int eLevel);

  int getMessageID();
  
  void setMessageID(int msgID);

private:

  string componentName;

  //Name of the component that is sending request.
  //It is used to identify by a node where to send the reply
  //for a particular request message.
  string sourceCompName;

  int startLevel;
  int endLevel;

  int cacheType;

  //message id is more like a sequence number used by sender and/or receiver to match
  //request with reply or vice versa.
  int messageID;

  bool errorState;	
  Logger &logger;

  void doDeserialize(hessian_input &hi);

};

} //namespace Message
} //namespace Tourist

#endif /*MSGCACHETRANSFERREQUEST_*/
