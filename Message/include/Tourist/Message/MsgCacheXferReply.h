#ifndef MSGCACHETRANSFERREPLY_
#define MSGCACHETRANSFERREPLY_

#include "Tourist/AbstractMessage.h"
//Only had to insert for NodeSet defination.. to bad
#include "Tourist/LocalNode.h" 
#include "Tourist/AvlCache.h"
#include "Tourist/Node.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"
#include "Poco/Logger.h"
#include <string>

using Tourist::AbstractMessage;
using Poco::Logger;

using namespace hessian;

namespace Tourist {
namespace Message {

class RemoteNode;

/**
 * Specify semantics of a cache transfer request.
 * The significant attributes includes:
 * message ID (msgID : int)
 * start level (startLevel: int)
 * end level (int)
 * Total nodes (Number of nodes satisying earlier query - that
 *        generated this response.
 * response size (Number of nodes part of this reply
 * node pointers(node-id, level, ip, port)
 */

class MsgCacheXferReply : public AbstractMessage
{
public:
  MsgCacheXferReply();

  MsgCacheXferReply(string componentName, string sourceCompName, int msgID, 
          int nodeCount, int respSize, Tourist::NodeSet &nodesToSend);

  MsgCacheXferReply(hessian_input& hi);

  ~MsgCacheXferReply();

  /**
   * Returns the serialized form of this message (generated via hessian).  * 
   */
  int getString(string &name);

  /**
   * Type of the underlying format (Hessian in this case).
   */
  int getType(); //TODO: We should replace "HESSIAN" with some more specific type.

  /**
   * Name of the Tourist component to which this message is addressed
   */
  string getComponentName();

  bool isInErrorState();

  int setData(char *data, int num);

  void setComponentName(string &component);	

  string getSourceCompName();

  void setSourceCompName(string component);

  int getMessageID();

  void setMessageID(int msgID);

  int getNodeCount();

  void setNodeCount(int size);

  int getResponseSize();

  void setResponseSize(int size);

  //Routing peers for whom we need to send pointer
  //to requesting nodes
  void setReplyNodes(NodeSet& nodes);

  NodeSet& getReplyNodes();

private:
  
  bool errorState;
  
  //Numeric id matching the request id.
  int messageID;

  //Name of the remote component we intend to send this object.	
  string componentName;

  //Name of the component which is sending the message.
  string sourceCompName;

  //Number of nodes matching the client query.
  int nodeCount;

  //Number of nodes that are included in the response.
  //This could be a subset of the total response generated
  //by the client query.
  int responseSize;

  NodeSet replyNodes;

  Logger &logger;

  void doDeserialize(hessian_input &hi);
};

} // namespace Message 
} // namespace Tourist

#endif /*MSGCACHETRANSFERREPLY_*/
