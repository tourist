#ifndef MSGBOOTSTRAPREQUEST_
#define MSGBOOTSTRAPREQUEST_

#include "Tourist/Message/RemoteNode.h"
#include <string>
#include "hessian_input.h"
#include "hessian_output.h"
#include "Poco/Logger.h"

using Tourist::Message::RemoteNode;
using Poco::Logger;

using namespace hessian;

namespace Tourist {
namespace Message {

/**
 * Donates a bootstrap request, asking a remote peer for
 *   requestee top level prefix and suffix node
 */
class MsgBootstrapRequest : public AbstractMessage
{

public:
  MsgBootstrapRequest();

  MsgBootstrapRequest(string compName, string sourceCom, RemoteNode &requestee, 
      int type = MsgBootstrapRequest::ALL);

  MsgBootstrapRequest(hessian_input &hi);

  ~MsgBootstrapRequest();

  int getString(string &name);

  int getType();

  string getComponentName();

  bool isInErrorState();

  void setComponentName(string compName);

  string getSourceCompName();

  void setSourceCompName(string comp);

  int setData(char *data, int size);

  int getMessageID();

  void setMessageID(int msgID);

  RemoteNode& getRequestee();

  void setRequestee(RemoteNode& requestee);

  int getRequestType();

  void setRequestType(int type);

  //request for top prefix node(s) only
  const static int TOP_PREFIX;

  //request for top suffix node(s) only
  const static int TOP_SUFFIX;

  //request for all type of nodes.
  const static int ALL;

private:

  void doDeserialize(hessian_input &hi);
  
  string componentName;
  
  string sourceCompName;

  Tourist::Message::RemoteNode requestee;

  //type of request top_prefix, top_suffix, both
  int type;

  bool errorState;

  int messageID;

  Logger &logger;
};

}
}

#endif
