/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MessageDispatcher.h"
#include "Tourist/Message/MessageNotification.h"
#include "Tourist/Util/Util.h"
#include "Tourist/TouristExceptions.h"
#include <sys/time.h>

//using namespace Tourist::Util;

using Tourist::ComponentNotFoundException;

namespace Tourist {
namespace Message {

Logger &MessageDispatcher::logger = Logger::get("Tourist.Message.MessageDispatcher");

FastMutex MessageDispatcher::_mutex;

MessageDispatcher::MessageDispatcher()
{
}

MessageDispatcher::~MessageDispatcher()
{
}

void MessageDispatcher::dispatchEvent(MessageNotification *notification)
{
  FastMutex::ScopedLock lock(_mutex);
  time_t start;

  if (logger.getLevel() == Poco::Message::PRIO_DEBUG)
    start = time(NULL);

  //can't continue if the passed message is NULL.
  //TODO: We may want to introduce an exception here.
  AbstractMessage *msg = notification -> message;
  if (msg == NULL)
  {
    error(logger, "Couldn't dispatch notification as message object was null");
    return;        
  }
  
  if (msg -> isInErrorState()) 
  {
    error(logger, "Recieved message object in error. Not processing it further");
    return;
  }
  
  string component = msg -> getComponentName();
  info(logger, "Dispatching event to " + component);
  debug(logger, "Node = " + notification->sender->toString()); 

  MessageNotifier *notifier;
  EventHandlerMap::iterator it = _handlers.find(component);

  if (it != _handlers.end())
    notifier = it -> second;
  else
  {
    string emsg = "No matching component for " + component;
    error(logger, emsg);
    return;
    //TODO handle error in the class that calls this method
    //throw ComponentNotFoundException(emsg);
  }

  notifier -> dispatch(notification);
  if (logger.getLevel() == Poco::Message::PRIO_DEBUG)
  {
    int delay = time(NULL) - start; //time passed
    debug(logger, "Message dispatched in " + Util::toString(delay) + " seconds");
  }
}

//TODO: Indication of success or failure status.
void MessageDispatcher::registerNotification(string methodName, const Poco::AbstractObserver& observer)
{
  FastMutex::ScopedLock lock(_mutex);

  info(logger, "Registering component for call-back " + methodName);
  MessageNotifier *notifier;
  MessageNotification *notification = new MessageNotification();
  EventHandlerMap::iterator it = _handlers.find(methodName);
  if (it == _handlers.end())
  {
    notifier = new MessageNotifier();
    _handlers[methodName] = notifier;
  }
  else
  {
    notifier = it->second;
    debug(logger, "Re-registration of component " + methodName);
  }
  notifier->addObserver(notification, observer);
}

void MessageDispatcher::registerNotifications(const vector<string> components, const Poco::AbstractObserver& observer)
/// Register a single call back method (observer) against multiple component.
{
  FastMutex::ScopedLock lock(_mutex);

  MessageNotifier *notifier;
  MessageNotification *notification = new MessageNotification();

  for (int i=0; i<components.size(); i++)
  {
    info(logger, "Registering component " + components[i]);
    EventHandlerMap::iterator it = _handlers.find(components[i]);
    if (it == _handlers.end())
    {
      notifier = new MessageNotifier();
      _handlers[components[i]] = notifier;
    }
    else
    {
       notifier = it->second;
       debug(logger, "Re-registration request for coponent " + components[i]);
    }
    notifier->addObserver(notification, observer);
  }
}

void MessageDispatcher::deregisterNotification(string component, const Poco::AbstractObserver& observer)
{
  FastMutex::ScopedLock lock(_mutex);

  EventHandlerMap::iterator it = _handlers.find(component);
  if (it != _handlers.end())
  {
    info(logger, "De-registering component " + component);
    MessageNotifier *notifier = it->second;
    notifier -> removeObserver(new MessageNotification(), observer);
    if (!notifier -> hasObservers())
      _handlers.erase(it);
  }
  else
  {
    debug(logger, "Failed attempte to remove component " + component);
  }
}
void MessageDispatcher::deregisterNotifications(const vector<string> components, const Poco::AbstractObserver& observer)
{
  FastMutex::ScopedLock lock(_mutex);

  for (int i=0; i<components.size(); i++)
  {
    info(logger, "De-registering component " + components[i]);
    EventHandlerMap::iterator it = _handlers.find(components[i]);
    if (it != _handlers.end())
    {
      MessageNotifier *notifier = it->second;
      notifier->removeObserver(new MessageNotification(), observer);
      if (!notifier -> hasObservers())
        _handlers.erase(it);
    }
    else
    {
      debug(logger, "Failed attempt to remove non-existent component " + components[i]);
    }
  }
}

int MessageDispatcher::getEventCount()
{
  return _handlers.size();
}

MessageNotifier::MessageNotifier() :
         logger(Logger::get("Tourist.Message.MessageNotifier"))
{
}

MessageNotifier::~MessageNotifier()
{
}

void MessageNotifier::removeObserver(MessageNotification *msgNotification, const Poco::AbstractObserver& observer)
{
  _nc.removeObserver(observer);

  /*MsgSet::iterator it = _set.end();
  if (observer.accepts(msgNotification))
    it = _set.find(msgNotification);
  if (it != _set.end())
    _set.erase(it);*/
}

void MessageNotifier::dispatch(MessageNotification* msgNotification)
{
  msgNotification->duplicate();

  try
  {
    _nc.postNotification(msgNotification);
  }
  catch (Exception &e)
  {
    string emsg = e.what();
    error(logger, "Couldn't dispatch notification " + emsg);
    error(logger, "Failed notification details: Node = " + msgNotification->sender->toString());
    string component = msgNotification -> message -> getComponentName();
    error(logger, " Component = " + component);
  }
}

void MessageNotifier::addObserver(MessageNotification *msgNotification, const Poco::AbstractObserver& observer)
{
  _nc.addObserver(observer);
 // if (observer.accepts(msgNotification))
    //_set.insert(msgNotification);
}

bool MessageNotifier::hasObservers() const
{
  return _nc.hasObservers();
}

} //namespace Message
} //namespace Tourist
