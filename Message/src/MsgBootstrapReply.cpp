/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MsgBootstrapReply.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/RemoteNodeUtil.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"


namespace Tourist {
namespace Message {


const int MsgBootstrapReply::INCLUDED = 1;
const int MsgBootstrapReply::FORWARDED = 2;
const int MsgBootstrapReply::NONE = 3;

MsgBootstrapReply::MsgBootstrapReply():
  logger(Logger::get("Tourist.Message.MsgBootstrapReply"))
{
}

MsgBootstrapReply::MsgBootstrapReply(string comp, string sourceComp, int sTpP,
   RemoteNode &tp, int sTpS, RemoteNode &ts) : 
   logger(Logger::get("Tourist.Message.MsgBootstrapReply"))
{
  this -> componentName = comp;
  this -> sourceCompName = sourceComp;

  this -> topPrefix = tp;
  this -> topSuffix = ts;

  setStatusTopPrefix(sTpP);  
  setStatusTopSuffix(sTpS);

  this -> errorState = false;
}

MsgBootstrapReply::MsgBootstrapReply(hessian_input &hi) :
  logger(Logger::get("Tourist.Message.MsgBootstrapReply"))
{
  doDeserialize(hi);
}

MsgBootstrapReply::~MsgBootstrapReply()
{
  debug(logger, "MsgBootstrap Destroyed");
}

int MsgBootstrapReply::getString(string &rpc)
{
  debug(logger, "Serializing MsgBootstrapReply");
  try
  {
    hessian_output ho;
    rpc = ho.start_call(this -> getComponentName());
    debug(logger, "Component = " + this -> getComponentName());

    rpc = ho.set_parameter(rpc, new String(this -> getSourceCompName()));
    debug(logger, "Source comp = " + this -> getSourceCompName());
   
    rpc = ho.set_parameter(rpc, new Integer(this -> getStatusTopPrefix()));
    debug(logger, "isTopPrefix = " + Util::toString(this -> getStatusTopPrefix()));

    rpc = ho.set_parameter(rpc, new Integer(this -> getStatusTopSuffix()));
    debug(logger, "isTopSuffix = " + Util::toString(this -> getStatusTopSuffix()));

    rpc = getNodeHeader(ho, rpc, this -> topPrefix);
    debug(logger, "Top Prefix = " + this -> getTopPrefix().toString());

    rpc = getNodeHeader(ho, rpc, this -> topSuffix);
    debug(logger, "Top Suffix = " + this -> getTopSuffix().toString());

  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, emsg);
    errorState = true;
    return -1;
  }
 
  return 0;
}

int MsgBootstrapReply::getType()
{
  return MessageTypeData::BOOTSTRAP_REQ;
}

string MsgBootstrapReply::getComponentName()
{
 return this -> componentName;
}

bool MsgBootstrapReply::isInErrorState()
{
  return this -> errorState;
}

void MsgBootstrapReply::setComponentName(string component)
{
  this -> componentName = component;
}

string MsgBootstrapReply::getSourceCompName()
{
  return this -> sourceCompName;
}

void MsgBootstrapReply::setSourceCompName(string component)
{
  this -> sourceCompName = component;
}

int MsgBootstrapReply::setData(char *data, int size)
{
  if (data == NULL)
  {
    errorState = true;
    return -1;
  }
  
  try
  {
    auto_ptr<input_stream> is(new char_input_stream((string::size_type)size, data));
    hessian_input hi(is);
    doDeserialize(hi);
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, emsg);  
    errorState = true;
    return -1;
  }

  return 0;
}

int MsgBootstrapReply::getMessageID()
{
  return messageID;
}

void MsgBootstrapReply::setMessageID(int msgID)
{
  this -> messageID = msgID;
}

int MsgBootstrapReply::getStatusTopPrefix()
{
  return this -> statusTopPrefix;
}

void MsgBootstrapReply::setStatusTopPrefix(int t)
{
  if (t == MsgBootstrapReply::INCLUDED ||
        t == MsgBootstrapReply::FORWARDED ||
        t == MsgBootstrapReply::NONE)
     this -> statusTopPrefix = t;

  else
    this -> statusTopPrefix = MsgBootstrapReply::NONE;
}

int MsgBootstrapReply::getStatusTopSuffix()
{
  return this -> statusTopSuffix;
}

void MsgBootstrapReply::setStatusTopSuffix(int t)
{
   if (t == MsgBootstrapReply::INCLUDED ||
        t == MsgBootstrapReply::FORWARDED ||
        t == MsgBootstrapReply::NONE)
     this -> statusTopSuffix = t;

  else
    this -> statusTopSuffix = MsgBootstrapReply::NONE;
}


RemoteNode& MsgBootstrapReply::getTopPrefix()
{
  return this -> topPrefix;
}

void MsgBootstrapReply::setTopPrefix(RemoteNode &rn)
{
  this -> topPrefix = rn;
}

RemoteNode& MsgBootstrapReply::getTopSuffix()
{
  return this -> topSuffix;
}

void MsgBootstrapReply::setTopSuffix(RemoteNode &rn)
{
  this -> topSuffix = rn;
}

void MsgBootstrapReply::doDeserialize(hessian_input &hi)
{
  errorState = true;
  try
  {
    this -> componentName = hi.start_call() -> value();
    debug(logger, "Component = " + this -> getComponentName());

    this -> sourceCompName = hi.read_string(hi.get_tag());
    debug(logger, "Source component = " + this -> getSourceCompName());

    int temp = hi.read_int(hi.get_tag());
    setStatusTopPrefix(temp);
    debug(logger, "isTopPrefix = " + Util::toString(this -> getStatusTopPrefix()));

    temp = hi.read_int(hi.get_tag());
    setStatusTopSuffix(temp);
    debug(logger, "isTopSuffix = " + Util::toString(this -> getStatusTopSuffix()));

    nodeFromHeader(hi, this -> topPrefix);
    debug(logger, "Top Prefix = " + this -> getTopPrefix().toString());

    nodeFromHeader(hi, this -> topSuffix);
    debug(logger, "Top Suffix = " + this -> getTopSuffix().toString());

  } 
  catch (hessian::exceptions::io_exception &e)
  {
    string emsg = e.what();
    error(logger, emsg);
    errorState = true;
  }
  errorState = false;
}

}//namespace Message
}//namespace Tourist
