/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "Tourist/Message/MsgCacheXferReply.h"
#include "Tourist/Message/MsgPeerAnnouncement.h"
#include "Tourist/Message/MsgBootstrapRequest.h"
#include "Tourist/Message/MsgBootstrapReply.h"
#include "Tourist/Message/MsgMulticast.h"
#include "Tourist/Util/Util.h"
#include "Poco/Exception.h"
#include "Poco/Instantiator.h"

using Poco::Instantiator;

namespace Tourist {
namespace Message {

const int MessageTypeData::PEER_ANNOUNCE  = 0;
const int MessageTypeData::CACHE_XFER_REQ = 1;
const int MessageTypeData::CACHE_XFER_REP = 2;
const int MessageTypeData::BOOTSTRAP_REQ = 3;
const int MessageTypeData::BOOTSTRAP_REP = 4;
const int MessageTypeData::MULTICAST = 5;

MessageTypeData::MessageTypeData()
{
  registerBuiltIns();
}

MessageTypeData::~MessageTypeData()
{
  
}

int MessageTypeData::registerMessageObj(int type, MessageInstantiator *instance)
{
  int status = 0;
  try 
  {
    dynFactory.registerClass(Util::toString(type), instance);
  }
  catch (Poco::ExistsException &e)
  {
    status = -1;
  }
  return status;
}
  
AbstractMessage* MessageTypeData::createInstance(int type)
{
  AbstractMessage * instance = NULL;
  try
  {
    instance = dynFactory.createInstance(Util::toString(type));
  }
  catch (Poco::NotFoundException &e)
  {
    //TODO: throw log
  }
  return instance;
}

void MessageTypeData::registerBuiltIns()
{
  this -> registerMessageObj(PEER_ANNOUNCE, 
      new Instantiator<MsgPeerAnnouncement, AbstractMessage>);

  this -> registerMessageObj(CACHE_XFER_REQ,
      new Instantiator<MsgCacheXferRequest, AbstractMessage>);
  
  this -> registerMessageObj(CACHE_XFER_REP,
        new Instantiator<MsgCacheXferReply, AbstractMessage>);

  this -> registerMessageObj(BOOTSTRAP_REQ,
        new Instantiator<MsgBootstrapRequest, AbstractMessage>);

  this -> registerMessageObj(BOOTSTRAP_REP,
        new Instantiator<MsgBootstrapReply, AbstractMessage>);

  
  this -> registerMessageObj(MULTICAST,
        new Instantiator<MsgMulticast, AbstractMessage>);
}

} // namespace Message
} // namespace Tourist
