#include "Tourist/Message/MsgMulticast.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/RemoteNodeUtil.h"
#include "Tourist/Util/Util.h"

//using namespace Tourist::Util;

namespace Tourist {
namespace Message {
  
MsgMulticast::MsgMulticast() :
       logger(Logger::get("Tourist.Message.MsgMulticast"))
{
	errorState = false;
}

MsgMulticast::MsgMulticast(string compName, string sourceComp, 
       RemoteNode sender, int eventType, int step):
       logger(Logger::get("Tourist.Message.MsgMulticast"))
{
  this -> componentName = compName;
  this -> sourceCompName = sourceComp;
  this -> initiatingPeer = sender;
	this -> eventType = eventType;
	this -> step = step;
	errorState = false;
}

MsgMulticast::MsgMulticast(hessian_input &hi) :
             logger(Logger::get("Tourist.Message.MsgMulticast"))
{
  doDeserialize(hi);
}

MsgMulticast::~MsgMulticast()
{

}

string MsgMulticast::getComponentName() 
{
  return this -> componentName;
}

void MsgMulticast::setComponentName(string &component)
{
  this -> componentName = component;
}

string MsgMulticast::getSourceCompName()
{
  return this -> sourceCompName;
}

void MsgMulticast::setSourceCompName(string &source)
{
  this -> sourceCompName = source;
}

bool MsgMulticast::isInErrorState()
{
  return this -> errorState;
}

int MsgMulticast::getString(string &rpc)
{
  debug(logger, "Serializing MsgMulticast");
  try 
  {
    hessian_output ho;

    rpc = ho.start_call(this -> getComponentName());
    debug(logger, "Component = " + this -> getComponentName());

    rpc = ho.set_parameter(rpc, new String(this -> getSourceCompName()));
    debug(logger, "Source component = " + this -> getSourceCompName());

    rpc = ho.set_parameter(rpc, new Integer(this -> eventType));
    debug(logger, "Event Type = " + Util::toString(this -> eventType));

    rpc = ho.set_parameter(rpc, new Integer(this -> step));
    debug(logger, "Step = " + Util::toString(this -> step));

		rpc = getNodeHeader(ho, rpc, initiatingPeer);
    debug(logger, "Sender peer = " + this -> initiatingPeer.toString());
  } 
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, emsg);
    return -1;  
  }
  return 0;
}

int MsgMulticast::getType()
{
  return MessageTypeData::MULTICAST;
}

int MsgMulticast::setData(char *data, int size)
{
  if (data == NULL)
  {
    return -1;
  }

  try 
  {
    auto_ptr<input_stream> is(new char_input_stream((string::size_type)size, data));
    hessian_input hi(is);
    doDeserialize(hi);
  }
  catch (hessian::exceptions::io_exception &e)
  {
    return -1;
  }
}

RemoteNode MsgMulticast::getInitiatingPeer()
{
  return this->initiatingPeer;
}

void MsgMulticast::setInitiatingPeer (RemoteNode node)
{
  initiatingPeer = node;
}

int MsgMulticast::getStep()
{
	return step;
}

void MsgMulticast::setStep(int step)
{
	this -> step = step;
}


int MsgMulticast::getEventType()
{
	return eventType;
}

void MsgMulticast::setEventType(int eventType)
{
	this -> eventType = eventType;
}



void MsgMulticast::doDeserialize(hessian_input &hi)
{
  try
  {
    this -> componentName = hi.start_call() -> value();
    debug(logger, "Component = " + this -> getComponentName());

    this -> sourceCompName = hi.read_string(hi.get_tag());
    debug(logger, "Source componnet = " + this -> sourceCompName);
	
    this -> eventType = hi.read_int(hi.get_tag());
		debug(logger, "Event Type = " + Util::toString(this -> eventType));

    this -> step = hi.read_int(hi.get_tag());
		debug(logger, "Step = " + Util::toString(this -> step));

		nodeFromHeader (hi, initiatingPeer );
    debug(logger, "Sender peer = " + this -> initiatingPeer.toString());	
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, "Failed to deSerialize Multicast announcemnet message " + emsg);
  }
}
  
} // namespace Message
} // namespace Tourist
