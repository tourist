/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Constants.h"
#include "Tourist/Util/Util.h"

namespace Tourist {
namespace Message {

MsgCacheXferRequest::MsgCacheXferRequest() :
  logger(Logger::get("Tourist.Message.MsgCacheXferRequest"))
{
  this -> errorState = false;
}

MsgCacheXferRequest::~MsgCacheXferRequest()
{
}

MsgCacheXferRequest::MsgCacheXferRequest(string componentName,
    string sourceComp, int sLevel, int eLevel, int cacheType) :
  logger(Logger::get("Tourist.Message.MsgCacheXferRequest"))
{
  this -> componentName = componentName;
  this -> sourceCompName = sourceComp;
  this -> startLevel = sLevel;
  this -> endLevel = eLevel;
  this -> cacheType = cacheType;
  this -> errorState = false;
}

MsgCacheXferRequest::MsgCacheXferRequest(hessian_input &hi) :
  logger(Logger::get("Tourist.Message.MsgCacheXferRequest"))
{
  doDeserialize(hi);
}

int MsgCacheXferRequest::getString(string &rpc)
{
  //serialized form of this message
  debug(logger, "Serializing MsgCacheRequest");
  try
  {
    hessian_output ho;

    rpc = ho.start_call( this-> getComponentName());
    debug(logger, "Component = " + this -> getComponentName());

    rpc = ho.set_parameter(rpc, new String(this -> getSourceCompName()));
    debug(logger, "Source component = " + this -> getSourceCompName());

    rpc = ho.set_parameter(rpc, new Integer(this -> getStartLevel()));
    debug(logger, "Start level = " + Util::toString( this -> getStartLevel() ) );

    rpc = ho.set_parameter(rpc, new Integer(this -> getEndLevel()));
    debug(logger, "End level = " + Util::toString ( this -> getEndLevel() ) );

    rpc = ho.set_parameter(rpc, new Integer(this -> getCacheType()));
    debug(logger, "Cache type = " + Util::toString ( this -> getCacheType() ) );
  
    rpc = ho.complete_call(rpc);
    debug(logger, "Size = " + Util::toString(rpc.length()));
    
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();    
    error(logger, "Error serializing cache transfer request message due to");
    this -> errorState = false;
    return -1;
  }
  
  return 0;
}

int MsgCacheXferRequest::getType()
{
  return MessageTypeData::CACHE_XFER_REQ;
}

string MsgCacheXferRequest::getComponentName()
{
  return this -> componentName;
}

bool MsgCacheXferRequest::isInErrorState()
{
  return this -> errorState;
}

void MsgCacheXferRequest::setComponentName(string component)
{
  this -> componentName = component;
}

string MsgCacheXferRequest::getSourceCompName()
{
  return this -> sourceCompName;  
}

void MsgCacheXferRequest::setSourceCompName(string compName)
{
  this -> sourceCompName = compName;
}

int MsgCacheXferRequest::setData(char *data, int size)
{
  if (data == NULL)
  {
    this -> errorState = true;
    return -1;
  }

  try
  {
    auto_ptr<input_stream> is(new char_input_stream((string::size_type)size, data));
    hessian_input hi(is);
    doDeserialize(hi);
  }
  catch (hessian::exceptions::io_exception &e)
  {
    this -> errorState = true;
    return -1;
  }
}
int MsgCacheXferRequest::getCacheType()
{
  return this -> cacheType;
}

void MsgCacheXferRequest::setCacheType(int cType)
{
  this -> cacheType = cType;
}

int MsgCacheXferRequest::getStartLevel()
{
  return this -> startLevel;
}

void MsgCacheXferRequest::setStartLevel(int sLevel)
{
  this -> startLevel = sLevel;
}

int MsgCacheXferRequest::getEndLevel()
{
  return this -> endLevel;
}

void MsgCacheXferRequest::setEndLevel(int eLevel)
{
  this -> endLevel = eLevel;
}

int MsgCacheXferRequest::getMessageID()
{
  return this -> messageID;
}

void MsgCacheXferRequest::setMessageID(int msgID)
{
  this -> messageID = msgID;
} 

void MsgCacheXferRequest::doDeserialize(hessian_input &hi)
{
  //derializing message
  debug(logger, "de-Serializing MsgCacheRequest");
  this -> errorState = false;
  
  try
  {
    this -> componentName = hi.start_call() -> value();
    debug(logger, "Component = " + this -> getComponentName());

    this -> sourceCompName = hi.read_string(hi.get_tag());
    debug(logger, "Source component = " + this -> getSourceCompName());

    this -> startLevel = hi.read_int(hi.get_tag());
    debug(logger, "Start level = " + Util::toString( this -> getStartLevel() ) );

    this -> endLevel = hi.read_int(hi.get_tag());
    debug(logger, "End level = " + Util::toString ( this -> getEndLevel() ) );

    this -> cacheType = hi.read_int(hi.get_tag());
    debug(logger, "Cache type = " + Util::toString ( this -> getCacheType() ) );
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();    
    error(logger, "Error in deserializing cache transfer request message due to ");
    this -> errorState = true;
  }
}

} // namespace Message
} // namespace Tourist
