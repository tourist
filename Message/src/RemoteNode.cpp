/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/TouristExceptions.h"
#include "Tourist/Message/MessageNotification.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/MsgPeerAnnouncement.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Net/PipeNotifications.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Util/common.h"
#include "Tourist/Util/Config.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"
#include <string>
#include <sstream>

using Tourist::NullPointerException;
using Tourist::Net::PipeNotification;
using Tourist::Net::BufferNotification;
using Tourist::Message::MessageNotification;
using Tourist::Message::MessageTypeData;
using Tourist::Message::MessageHeader;
using Tourist::Message::MsgPeerAnnouncement;
using Tourist::Util::Config;

using namespace hessian;

namespace Tourist {
namespace Message {

const int RemoteNode::STATUS_NOOP = 0;
const int RemoteNode::STATUS_CONNECTED = 1;
const int RemoteNode::STATUS_DISCONNECTED = 2;

FastMutex RemoteNode::mutex;

RemoteNode::RemoteNode(): Node(), logger(Logger::get("Tourist.Message.RemoteNode")),
      messageProcessor()       
{
  init(NULL);
}

RemoteNode::RemoteNode(Config *config): Node(), logger(Logger::get("Tourist.Message.RemoteNode")),
     messageProcessor((config == NULL) ? NULL :config -> getMessageTypeHandler())
{
  init(config);
}

RemoteNode::RemoteNode(Config *config, string id, int level):   Node(id, level),
     logger(Logger::get("Tourist.Message.RemoteNode")),
     messageProcessor((config == NULL) ? NULL : config -> getMessageTypeHandler())
{
  init(config);
}

RemoteNode::RemoteNode(Config *config, string id, int level, string host, int port, int protocol):
     Node(id, level),     
     logger(Logger::get("Tourist.Message.RemoteNode")),
     messageProcessor((config == NULL) ? NULL : config -> getMessageTypeHandler())
{
  this->host = host;
  this->port = port;
  this->protocol = protocol;
  init(config);
}
 
RemoteNode::RemoteNode(const RemoteNode& rn) : 
   logger(Logger::get("Tourist.Message.RemoteNode")),
   messageProcessor(NULL)
{
  *this = rn;
}

RemoteNode::~RemoteNode()
{
  //if (buffer != NULL)
    //delete buffer;
}
 
void RemoteNode::setConfig(Config *config)
{
  if (config != NULL)
  {
    messageTypeHandler = config -> getMessageTypeHandler();
    messageBus = config -> getMessageBus();
    messageProcessor.setMsgTypeHandler(messageTypeHandler);
  }
}

void RemoteNode::init(Config *config)
{
  if (config != NULL)
  {
    if (config -> getMessageBus() != NULL)
      messageBus = config -> getMessageBus();
    else
      error(logger, "MessageBus is NULL! remoteNode = " + this -> toString());
    
    if (config -> getMessageTypeHandler() != NULL)
      messageTypeHandler = config -> getMessageTypeHandler();
    else
      error(logger, "MessageTypeHandler is NULL! remoteNode = " + this -> toString());
  }
  else
  {
    debug(logger, "Configuration object is NULL for remoteNode " + this -> toString());
    messageBus = NULL;
    messageTypeHandler = NULL;
  }  
  
  pipe = NULL;

  status = STATUS_NOOP;
}

RemoteNode& RemoteNode::operator=(const RemoteNode& rn)
{
  if (this == &rn)
    return *this;

  //this -> bwUsage = rn.bwUsage;
  this -> host = rn.host;
  this -> pipe = rn.pipe;
  this -> port = rn.port;
  this -> protocol = rn.protocol;
  this -> bwUsage = rn.bwUsage;
  this -> messageBus = rn.messageBus;
  this -> messageTypeHandler = rn.messageTypeHandler;

  this -> setID(rn.getID());

  return *this;
}

const int RemoteNode::sendMessage(MessageHeader *header, AbstractMessage *message)
{
  int resp = -1;
  int bufferSize = 0;

  //In case pipe is still null.  
  if (pipe == NULL)
  {
    error(logger, "Message sent failed as the netowrk pipe is NULL for "
        + this->toString());
    return -1;
  }

  char *buffer;
  string str;
  int status = message -> getString(str);

  if (status != 0)
  {
    error(logger, "Failed to send message as we are unable to extract payload bytes");
    return -1;
  }

  if (header != NULL)
  {
    //header is not null, its a usual scenerio of sending a message packaged
    //with tourist header to a remote node.
    string header_str;    
    int status = header -> getString(header_str);
    if (status != 0)
    {
      error(logger, "Failed to send message as message parsing failed "
          + this -> toString());
      return -1;
    }
    
    bufferSize = MessageHeader::HEADER_SIZE + str.length();
    buffer = new char[bufferSize];   
    memcpy(buffer, header_str.c_str(), MessageHeader::HEADER_SIZE);
    memcpy(buffer + MessageHeader::HEADER_SIZE, str.c_str(), str.length());    
  }
  else
  {
    //no header just extract bytes from payload.
    buffer = const_cast<char*> (str.c_str());
  }
 
  resp = pipe -> sendMessage(buffer, bufferSize);
  return resp;
}

void  RemoteNode::setPipe(AbstractPipe* p)
{
  debug(logger, "RemoteNode::setPipe()");

  //TODO Don't throw exception, just log the anomoly.
  if (p == NULL)
    throw NullPointerException("Pipe is null for " + this -> toString());
  
  this -> pipe = p;
  this -> pipe -> registerDataNotification((CallbackInterface*) this);
  this -> pipe -> registerDisconnectNotification((CallbackInterface*) this);

  info(logger, "Registered data/disconnect notification with the pipe");
  
}

AbstractPipe* RemoteNode::getPipe()
{
  return this->pipe;
}

void RemoteNode::setHost(string hostName)
{
  this->host = hostName;
}

string RemoteNode::getHost()
{
  return this->host;
}

int RemoteNode::getProtocol()
{
  return this->protocol;
}

void RemoteNode::setProtocol(int proto)
{
  this->protocol = proto;
}

int RemoteNode::getPort()
{
  return this->port;
}

void RemoteNode::setPort(int port)
{
  this->port = port;
}

int RemoteNode::getStatus()
{
  return this -> status;
}

void RemoteNode::callback(void *data)
{ 
  FastMutex::ScopedLock lock(mutex);
  
  if (data == NULL)
  {
    error(logger, "Call back received by rn " + this -> toString() + " was null");
    return;
  }
  //this node takes the ownership of the data.
  PipeNotification *pipeNotify = (PipeNotification*) data;
  switch (pipeNotify -> type)
  {
    case PipeNotification::BUFFER_ARRIVED: 
    { // braces to ensure scope of buffNotify
      info(logger, "Received data buffer");
      BufferNotification *buffNotify = (BufferNotification*) pipeNotify;
      messageProcessor.addBytes(buffNotify -> buffer, buffNotify -> len);
      AbstractMessage *msg = NULL;
      while ((msg = messageProcessor.popMessage()) != NULL) 
      {
        MessageNotification *notification = new MessageNotification();
        notification -> sender = this;
        notification -> message = msg;
        info(logger, "Message retrieved, dispatching..!");
        messageBus -> dispatchEvent(notification); 
        debug(logger, "Message source component = " + msg -> getComponentName()); 
        debug(logger, "Message type = " + Util::toString(msg -> getType()));
      }
      break;
    }
    case PipeNotification::DISCONNECT:
    {
      info(logger, "Received disconnect notification for rn " + this -> toString());
      status = STATUS_DISCONNECTED;
      break;
    }
  }    
}

int RemoteNode::getBwUsage()
{
  return this -> bwUsage;
}

void RemoteNode::setBwUsage(int bw)
{
  this -> bwUsage = bw;
}

string RemoteNode::toString()
{
  ostringstream outstream;
  outstream<<getName()<<"-"<<hexstring()<<":"<<
         getLevel()<<"-"<<getHost()<<":"<<getPort();

  return outstream.str();
}

}//namespace App
}//namespace Tourist
