/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MsgBootstrapRequest.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/RemoteNodeUtil.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"


namespace Tourist {
namespace Message {

const int MsgBootstrapRequest::TOP_PREFIX = 1;
const int MsgBootstrapRequest::TOP_SUFFIX = 2;
const int MsgBootstrapRequest::ALL = 3;

MsgBootstrapRequest::MsgBootstrapRequest():
  logger(Logger::get("Tourist.Message.MsgBootstrapRequest"))
{
}

MsgBootstrapRequest::MsgBootstrapRequest(string comp, string sourceComp, RemoteNode &req, int type)
  :logger(Logger::get("Tourist.Message.MsgBootstrapRequest"))
{
  this -> componentName = comp;
  this -> sourceCompName = sourceComp;
  this -> requestee = req;
  this -> errorState = false;

  //make sure the type is correctly specified
  setRequestType(type);
}

MsgBootstrapRequest::MsgBootstrapRequest(hessian_input &hi) :
  logger(Logger::get("Tourist.Message.MsgBootstrapRequest"))
{
  doDeserialize(hi);
}

MsgBootstrapRequest::~MsgBootstrapRequest()
{
  debug(logger, "MsgBootstrapRequest destroyed!");
}

int MsgBootstrapRequest::getString(string &rpc)
{
  debug(logger, "Serializing MsgBootstrapRequest");
  try
  {
    hessian_output ho;
    rpc = ho.start_call(this -> getComponentName());
    debug(logger, "Component = " + this -> getComponentName());

    rpc = ho.set_parameter(rpc, new String(this -> getSourceCompName()));
    debug(logger, "Source comp = " + this -> getSourceCompName());
   
    rpc = ho.set_parameter(rpc, new Integer(this -> getRequestType()));
    debug(logger, "Type = " + Util::toString(this -> getRequestType()));

    rpc = getNodeHeader(ho, rpc, requestee);
    debug(logger, "Requestee = " + requestee.toString());
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, emsg);
    errorState = true;
    return -1;
  }
 
  return 0;
}

int MsgBootstrapRequest::getType()
{
  return MessageTypeData::BOOTSTRAP_REQ;
}

string MsgBootstrapRequest::getComponentName()
{
 return this -> componentName;
}

bool MsgBootstrapRequest::isInErrorState()
{
  return this -> errorState;
}

void MsgBootstrapRequest::setComponentName(string component)
{
  this -> componentName = component;
}

string MsgBootstrapRequest::getSourceCompName()
{
  return this -> sourceCompName;
}

void MsgBootstrapRequest::setSourceCompName(string component)
{
  this -> sourceCompName = component;
}

int MsgBootstrapRequest::setData(char *data, int size)
{
  if (data == NULL)
  {
    errorState = true;
    return -1;
  }
  
  try
  {
    auto_ptr<input_stream> is(new char_input_stream((string::size_type)size, data));
    hessian_input hi(is);
    doDeserialize(hi);
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, emsg);  
    errorState = true;
    return -1;
  }

  return 0;
}

int MsgBootstrapRequest::getMessageID()
{
  return messageID;
}

void MsgBootstrapRequest::setMessageID(int msgID)
{
  this -> messageID = msgID;
}

RemoteNode& MsgBootstrapRequest::getRequestee()
{
  return this -> requestee;
}

void MsgBootstrapRequest::setRequestee(RemoteNode &rn)
{
  this -> requestee = rn;
}

void MsgBootstrapRequest::setRequestType(int type)
{
  if (type == MsgBootstrapRequest::TOP_PREFIX ||
        type == MsgBootstrapRequest::TOP_SUFFIX ||
        type == MsgBootstrapRequest::ALL )
    this -> type = type;
  else
    this -> type = MsgBootstrapRequest::ALL;
}

int MsgBootstrapRequest::getRequestType()
{
  return this -> type;
}

void MsgBootstrapRequest::doDeserialize(hessian_input &hi)
{
  errorState = true;
  try
  {
    this -> componentName = hi.start_call() -> value();
    debug(logger, "Component = " + this -> getComponentName());

    this -> sourceCompName = hi.read_string(hi.get_tag());
    debug(logger, "Source component = " + this -> getSourceCompName());

    int type = hi.read_int(hi.get_tag());
    setRequestType(type);
    debug(logger, "Type = " + Util::toString(this -> getRequestType()));

    nodeFromHeader(hi, this -> requestee);
    debug(logger, "Requestee = " + this -> requestee.toString());
  } 
  catch (hessian::exceptions::io_exception &e)
  {
    string emsg = e.what();
    error(logger, emsg);
    errorState = true;
  }
  errorState = false;
}

}//namespace Message
}//namespace Tourist
