/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MessageProcessor.h"
#include "Tourist/Util/Util.h"
#include <stdio.h>

using namespace Tourist::Util;

namespace Tourist {
namespace Message {

MessageProcessor::MessageProcessor() :
  logger(Logger::get("Tourist.Message.MessageProcessor"))
{
  this -> msgTypeHandler = NULL;
  this -> waitingForHeader = true;
  this -> bufferlen = 0;
  this -> currentHeader = NULL;
}

MessageProcessor::MessageProcessor(MessageTypeData* typeHandler) :
  logger(Logger::get("Tourist.Message.MessageProcessor"))
{
  this -> msgTypeHandler = typeHandler;  
  this -> waitingForHeader = true;
  this -> bufferlen = 0;
  this -> currentHeader = NULL;
}

MessageProcessor::~MessageProcessor()
{
  if (currentHeader != NULL)
   delete currentHeader;
}

void MessageProcessor::setMsgTypeHandler(MessageTypeData* typeHandler)
{
  this -> msgTypeHandler = typeHandler;
}

AbstractMessage* MessageProcessor::popMessage()
{
  AbstractMessage* response = NULL;
  if (messages.size() > 0)
  {
    response = messages.front(); 
    messages.pop();
  }

  return response;
}

void MessageProcessor::addBytes(char *bytes, int len)
{
    
  info(logger, "Adding bytes of length " + toString(len));
  info(logger, "Bytes still remaining to process " + toString(this -> bufferlen));

  if (this -> bufferlen != 0)
  {
    char *temp = new char[this -> bufferlen + len];
    memcpy(temp, this -> buffer, this -> bufferlen);
    memcpy(temp + this -> bufferlen, bytes, len);
    
    this -> buffer = temp;
    this -> bufferlen = this -> bufferlen + len;
  }
  else
  {    
    this -> buffer = new char[len];
    memcpy(this -> buffer, bytes, len);
    this -> bufferlen = len;
  }
  
  if (this -> waitingForHeader)
    processHeader();
  else
    processData();
}

void MessageProcessor::processHeader()
{
 
  debug(logger, "Processing header with number of bytes in pool " 
    + toString(this -> bufferlen));

  if (this -> bufferlen <  MessageHeader::HEADER_SIZE)
  { 
    //size of received bytes is less than typical header size.
    //copy the current bytes into buffer;
    //memcpy(buffer, temp, templen);
    //bufferlen = templen;
  }
  else 
  {    
    //size of processable bytes is more than the typical header size.
    //process the header, buffer the auxiliary bytes.
      
    currentHeader = new MessageHeader();
    currentHeader -> setData(buffer, MessageHeader::HEADER_SIZE);
    debug(logger, "Header extracted Version = " + toString(currentHeader -> getVersion()) +
                  " Payload size = " + toString(currentHeader -> getPayloadSize()) +
                   " Payload type = " + toString(currentHeader -> getPayloadType()));
    
    if (this -> bufferlen > MessageHeader::HEADER_SIZE)
    {
      //copying auxiliary bytes.
      int remainingBytes = this -> bufferlen - MessageHeader::HEADER_SIZE;
      char *temp = new char[remainingBytes];
      memcpy(temp, this -> buffer + MessageHeader::HEADER_SIZE, remainingBytes);
      this -> buffer = temp;
      this -> bufferlen = remainingBytes;
    }
    else
      this -> bufferlen = 0;
    
    //We are no longer waiting for header
    this -> waitingForHeader = false; 
  }
  
  //if there are still bytes in teh pipeline but we
  //have got our header it means we need to process payload.
  if (bufferlen != 0 && !waitingForHeader)
    processData(); 
}


void MessageProcessor::processData()
{
  
  debug(logger, "Processing data with number of bytes in pool " 
       + toString(this -> bufferlen));  
  
  if (currentHeader == NULL)
  {
    error(logger, "Received data while waiting for header");
    return;
  }
  
  if (msgTypeHandler == NULL)
  {
    error(logger, "Not capable of processing header  as msg type handler is null");
    return;
  }
  int payloadSize = currentHeader -> getPayloadSize();
  //type of message 
  int payloadType = currentHeader -> getPayloadType();
  
  if (this -> bufferlen < payloadSize)
  { 
    //size of received bytes is less than payload size.
    //copy the current bytes into buffer;
    //memcpy(buffer, temp, templen);
    //bufferlen = templen;
  }
  else
  {
    //It means we got enough bytes to process the payload  
    debug(logger, "Creating message instance payload type = " + toString(payloadType));
    AbstractMessage *msg = msgTypeHandler -> createInstance(payloadType);    
    if (msg == NULL)    
    {
      debug(logger, "Failed to process payload message");    
    }
    else
    {
      msg -> setData(this -> buffer, payloadSize);      
      debug(logger, "Message created: Component = " + msg -> getComponentName());
      messages.push(msg); 
    }
    
    //We have processed the given payload, let's move
    //to process header or wait if there isn't any more data.
    this -> waitingForHeader = true;
    delete currentHeader;
    currentHeader = NULL;
    
    if (this -> bufferlen > payloadSize)
    {
      //copying auxiliary bytes.
      int remainingBytes = this -> bufferlen - payloadSize;
      char *temp = new char[remainingBytes];
      memcpy(temp, this -> buffer + payloadSize, remainingBytes);
      this -> buffer = temp;
      this -> bufferlen = remainingBytes;           
    }
    else
      this -> bufferlen = 0; //reset the buffer          
  }
 
  //if there are more bytes in pipe and we have consumed all
  //message bytes it means we have header in pipeline.
  if (bufferlen != 0 && waitingForHeader)
    processHeader();
}

}
}
