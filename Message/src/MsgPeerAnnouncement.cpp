/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MsgPeerAnnouncement.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/RemoteNodeUtil.h"
#include "Tourist/Util/Util.h"

//using namespace Tourist::Util;

namespace Tourist {
namespace Message {
  
MsgPeerAnnouncement::MsgPeerAnnouncement() :
       logger(Logger::get("Tourist.Message.MsgPeerAnnouncment"))
{
  errorState = false;
}

MsgPeerAnnouncement::MsgPeerAnnouncement(string compName, string sourceComp, 
       RemoteNode sender):
       logger(Logger::get("Tourist.Message.MsgPeerAnnouncement"))
{
  this -> componentName = compName;
  this -> sourceCompName = sourceComp;
  this -> senderPeer = sender;
  errorState = false;
}

MsgPeerAnnouncement::MsgPeerAnnouncement(hessian_input &hi) :
             logger(Logger::get("Tourist.Message.MsgPeerAnnouncement"))
{
  doDeserialize(hi);
}

MsgPeerAnnouncement::~MsgPeerAnnouncement()
{

}

string MsgPeerAnnouncement::getComponentName() 
{
  return this -> componentName;
}

void MsgPeerAnnouncement::setComponentName(string &component)
{
  this -> componentName = component;
}

string MsgPeerAnnouncement::getSourceCompName()
{
  return this -> sourceCompName;
}

void MsgPeerAnnouncement::setSourceCompName(string &source)
{
  this -> sourceCompName = source;
}

bool MsgPeerAnnouncement::isInErrorState()
{
  return this -> errorState;
}

int MsgPeerAnnouncement::getString(string &rpc)
{
  debug(logger, "Serializing MsgPeerAnnouncement");
  try 
  {
    hessian_output ho;

    rpc = ho.start_call(this -> getComponentName());
    debug(logger, "Component = " + this -> getComponentName());

    rpc = ho.set_parameter(rpc, new String(this -> getSourceCompName()));
    debug(logger, "Source component = " + this -> getSourceCompName());

    rpc = ho.set_parameter(rpc, new Boolean(this -> getIsAck()));
    debug(logger, "IsAck = " + string( (this -> getIsAck() ? "true" : "false"))	);

    rpc = getNodeHeader(ho, rpc, this -> senderPeer);
    debug(logger, "Sender peer = " + this -> senderPeer.toString());
  } 
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, emsg);
    errorState = true;
    return -1;  
  }
  return 0;
}

int MsgPeerAnnouncement::getType()
{
  return MessageTypeData::PEER_ANNOUNCE;
}

int MsgPeerAnnouncement::setData(char *data, int size)
{
  if (data == NULL)
  {
    errorState = true;
    return -1;
  }

  try 
  {
    auto_ptr<input_stream> is(new char_input_stream((string::size_type)size, data));
    hessian_input hi(is);
    doDeserialize(hi);
  }
  catch (hessian::exceptions::io_exception &e)
  {
    errorState = true;
    return -1;
  }
}

RemoteNode& MsgPeerAnnouncement::getSendingPeer()
{
  return this->senderPeer;
}

void MsgPeerAnnouncement::setSendingPeer (RemoteNode& node)
{
  senderPeer = node;
}

void MsgPeerAnnouncement::setIsAck(bool flag)
{
  this -> isAck = flag;
}

bool MsgPeerAnnouncement::getIsAck()
{
  return this -> isAck;
}

void MsgPeerAnnouncement::doDeserialize(hessian_input &hi)
{
  errorState = false;
  try
  {
    this -> componentName = hi.start_call() -> value();
    debug(logger, "Component = " + this -> getComponentName());

    this -> sourceCompName = hi.read_string(hi.get_tag());
    debug(logger, "Source componnet = " + this -> sourceCompName);

    this -> isAck = hi.read_boolean(hi.get_tag());
    debug(logger, "IsAck = " + string( (this -> getIsAck() ? "true" : "false"))	);

    nodeFromHeader (hi, this -> senderPeer );
    debug(logger, "Sender peer = " + this -> senderPeer.toString());	
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, "Failed to deSerialize peer announcemnet message " + emsg);
    errorState = true;
  }
}
  
} // namespace Message
} // namespace Tourist
