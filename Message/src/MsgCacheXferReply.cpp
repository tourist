/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MsgCacheXferReply.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/RemoteNodeUtil.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"

//using namespace Tourist::Util;

namespace Tourist {
namespace Message {

MsgCacheXferReply::MsgCacheXferReply() : 
   logger(Logger::get("Tourist.Message.MsgCacheXferReply"))
{
  this -> errorState = false;
}

MsgCacheXferReply::MsgCacheXferReply(string componentName, string sourceComp, int msgID, 
      int nodeCount, int respSize,  NodeSet &nodesToSend)
     : logger(Logger::get("Tourist.Message.MsgCacheTransferReply"))
{
  this -> componentName = componentName;
  this -> sourceCompName = sourceComp;
  this -> messageID = msgID;
  this -> nodeCount = nodeCount;
  this -> responseSize = respSize;	
  this -> replyNodes = nodesToSend;
  this -> errorState = false; 
}

MsgCacheXferReply::MsgCacheXferReply(hessian_input& hi) :
  logger(Logger::get("Tourist.Message.MsgCacheTransferRequest"))
{
  doDeserialize(hi);
}

MsgCacheXferReply::~MsgCacheXferReply()
{
}

int MsgCacheXferReply::getString(string &name)
{   
  debug(logger, "Serializing MsgCacheTransferReply");
  try
  {
    hessian_output ho;
    
    string rpc = ho.start_call(this -> getComponentName());
    debug(logger, "Component = " + this -> getComponentName());

    rpc = ho.set_parameter(rpc, new String( this -> getSourceCompName()));
    debug(logger, "Source component = " + this -> getSourceCompName());
    
    rpc = ho.set_parameter(rpc, new Integer( this -> getMessageID() ) );
    debug(logger, "Message ID = " + Util::toString ( this -> getMessageID() ) );
    
    rpc = ho.set_parameter(rpc, new Integer( this -> getNodeCount() ) );
    debug(logger, "Node count = " + Util::toString ( this -> getNodeCount() ));
    
    rpc = ho.set_parameter(rpc, new Integer( this -> getResponseSize() ));
    debug(logger, "Response size = " + Util::toString( this -> getResponseSize() ) );
    
    Tourist::NodeSet::Iter iter(replyNodes);
    while (iter)
    {
      RemoteNode *rn = (RemoteNode*)(iter -> key);
      rpc = getNodeHeader(ho, rpc, *rn);
      debug(logger, "Added node = " + rn -> toString());
      iter++;
    }   
    name = rpc;
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, emsg);
    errorState = true;
    return -1;
  }
  return 0;
}

int MsgCacheXferReply::getType()
{
  return MessageTypeData::CACHE_XFER_REP;
}
  
string MsgCacheXferReply::getComponentName()
{
   return this -> componentName;
}

bool MsgCacheXferReply::isInErrorState()
{
  return this -> errorState;
}

void MsgCacheXferReply::setComponentName(string &component)
{
  this -> componentName = component;
}

string MsgCacheXferReply::getSourceCompName()
{
  return this -> sourceCompName;
}

void MsgCacheXferReply::setSourceCompName(string component)
{
  this -> sourceCompName = component;
}

int MsgCacheXferReply::setData(char *data, int size)
{
  if (data == NULL)
  {
    errorState = true;
    return -1;
  }

  try
  {
    auto_ptr<input_stream> is(new char_input_stream((string::size_type)size, data));
    hessian_input hi(is);
    doDeserialize(hi);
  }
  catch (hessian::exceptions::io_exception &e)
  {
    errorState = true;
    return -1;
  }

  return 0;
}

int MsgCacheXferReply::getMessageID()
{
  return this -> messageID;
}

void MsgCacheXferReply::setMessageID(int ID)
{
  this -> messageID = ID;
}

int MsgCacheXferReply::getNodeCount()
{
  return this -> nodeCount;
}

void MsgCacheXferReply::setNodeCount(int size)
{
  this -> nodeCount = size;
}

int MsgCacheXferReply::getResponseSize()
{
  return this -> responseSize;
}

void MsgCacheXferReply::setResponseSize(int size)
{
  this -> responseSize = size;
}

void MsgCacheXferReply::setReplyNodes(NodeSet& nodes)
{
  this -> replyNodes = nodes;
}

NodeSet& MsgCacheXferReply::getReplyNodes()
{
  return this -> replyNodes; 
}

void MsgCacheXferReply::doDeserialize(hessian_input &hi)
{
  errorState = false;
  try
  {
    this -> componentName = hi.start_call() -> value();
    debug(logger, "Component = " + this -> getComponentName());

    this -> sourceCompName = hi.read_string(hi.get_tag());
    debug(logger, "Source component = " + this -> sourceCompName);

    this -> messageID = hi.read_int( hi.get_tag() );
    debug(logger, "Message ID = " + Util::toString ( this -> getMessageID() ) );

    this -> nodeCount = hi.read_int( hi.get_tag() );
    debug(logger, "Node count = " + Util::toString ( this -> getNodeCount() ) );

    this -> responseSize = hi.read_int( hi.get_tag() );
    debug(logger, "Response size = " + Util::toString( this -> getResponseSize() ) );

    int i;
    for (i=0; i< responseSize; i++)
    {
      RemoteNode *rn = new RemoteNode();
      nodeFromHeader(hi, *rn);
      this -> replyNodes.insert(rn);
      debug(logger, "Added node " + rn -> toString());
    }
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, "Failed to deserialize cache transfer reply message due to " + emsg);
    errorState = true;
  }
}

} // namespace Message {
} // namespace Tourist
