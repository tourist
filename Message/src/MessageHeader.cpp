/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/RemoteNodeUtil.h"
#include "Tourist/Util/Util.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"

//using namespace Tourist::Util;

namespace Tourist {
namespace Message {

const int MessageHeader::HEADER_SIZE = 28;

const int MessageHeader::HEADER_VERSION = 1;

MessageHeader::MessageHeader() :
  logger(Logger::get("Tourist.Message.MessageHeader.cpp"))
{
  inError = false;
}

MessageHeader::MessageHeader(hessian_input &hi) :
  logger(Logger::get("Tourist.Message.MessageHeader"))
{
  doDeserialize(hi);
}

MessageHeader::MessageHeader(int version, int payloadSize, int payloadType) :
  logger(Logger::get("Tourist.Message.MessageHeader"))
{
  this -> version = version;
  this -> payloadSize = payloadSize;
  this -> payloadType = payloadType;
  inError = false;
}

MessageHeader::~MessageHeader()
{
  
}

int MessageHeader::getString(string &serialized)
{
  debug(logger, "Serializing MessageHeader");
  try
  {
    hessian_output ho;
    
    serialized = ho.start_call("header");
    
    serialized = ho.set_parameter(serialized, new Integer(this -> getVersion()));
    debug(logger, "Version = " + Util::toString(this -> getVersion()));
    
    serialized = ho.set_parameter(serialized, new Integer(this -> getPayloadSize()));
    debug(logger, "Payload size = " + Util::toString(this -> getPayloadSize()));
    
    serialized = ho.set_parameter(serialized, new Integer(this -> getPayloadType()));
    debug(logger, "Payload type = " + Util::toString(this -> getPayloadType()));
    
    serialized = ho.complete_call(serialized);
    debug(logger, "Size = " + Util::toString(serialized.length()));
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, "Serialization failed " + emsg);
    inError = true;
    return -1;
  }
  
  return 0;
}

int MessageHeader::setData(char* data, int size)
{
  if (data == NULL)
  {
    inError = true;
    return -1;
  }
    
  try 
  {
    auto_ptr<input_stream> is(new char_input_stream((string::size_type)size, data));
    hessian_input hi(is);  
    doDeserialize(hi);  
  }
  catch (hessian::exceptions::io_exception &e)
  {
    inError = true;
    return -1;
  }
  
  return 0;
}

bool MessageHeader::isInErrorState()
{
  return this -> inError;
}

int MessageHeader::getVersion()
{
  return this -> version;
}

int MessageHeader::getPayloadSize()
{
  return this -> payloadSize;
}

int MessageHeader::getPayloadType()
{
  return this -> payloadType;
}

void MessageHeader::doDeserialize(hessian_input &hi)
{
  debug(logger, "Deserializing MessageHeader");
  inError = false;
  try
  {
    string componentName = hi.start_call() -> value();
    debug(logger, "Component = " + componentName);

    //just to be sure that we don't 
    if (componentName != "header")
    {
      error(logger, "Component is not header. Rejecting!");
      inError = true;
      return;
    }

    this -> version = hi.read_int(hi.get_tag());
    debug(logger, "version = " + Util::toString(this -> getVersion()));

    this -> payloadSize = hi.read_int(hi.get_tag());
    debug(logger, "payload size = " + Util::toString(this -> getPayloadSize()));

    this -> payloadType = hi.read_int(hi.get_tag());
    debug(logger, "payload type = " + Util::toString(this -> getPayloadType()));

  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    error(logger, "Failed to deserialize header due to " + emsg);
    inError = true;
  }
}

} //namespace Message
} //namespace Tourist
