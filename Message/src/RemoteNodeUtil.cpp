/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef REMOTENODEUTIL_CPP_
#define REMOTENODEUTIL_CPP_

#include "Tourist/Message/RemoteNodeUtil.h"

namespace Tourist {
namespace Message {
  
string getNodeHeader(hessian_output &ho, string &rpc, RemoteNode &node)
{
  try
  {
    rpc = ho.set_parameter(rpc, new String (node.hexstring())); //node id
    rpc = ho.set_parameter(rpc, new Integer(node.getLevel())); //node level
    rpc = ho.set_parameter(rpc, new String (node.getName()));// node name
    rpc = ho.set_parameter(rpc, new String (node.getHost())); //host
    rpc = ho.set_parameter(rpc, new Integer(node.getPort())); //port
    rpc = ho.set_parameter(rpc, new Integer(node.getProtocol())); //port
    rpc = ho.set_parameter(rpc, new Integer(node.getBwUsage())); //bandwidth
  }
  catch (hessian::exceptions::io_exception& e)
  {
    throw e;
  }

  return rpc;
}

void nodeFromHeader(hessian_input &hi, RemoteNode &rn)
{
  try
  {
    string id = hi.read_string(hi.get_tag());
    int level = hi.read_int(hi.get_tag());
    string name = hi.read_string(hi.get_tag());
    string host = hi.read_string(hi.get_tag());
    int port = hi.read_int(hi.get_tag());
    int protocol = hi.read_int(hi.get_tag());
    int bwUsage = hi.read_int(hi.get_tag());

    RemoteNode temp(NULL, id, level, host, port, protocol);
    rn = temp;
    rn.setName(name);
    rn.setBwUsage(bwUsage);
  }
  catch (hessian::exceptions::io_exception& e)
  {
    throw e;
  }
}

} // namespace Message
} // namespace Tourist

#endif /*REMOTENODEUTIL_CPP_*/
