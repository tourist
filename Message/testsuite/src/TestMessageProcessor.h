#ifndef TESTMESSAGEPROCESSOR_H_
#define TESTMESSAGEPROCESSOR_H_

#include "CppUnit/TestCase.h"

class TestMessageProcessor : public CppUnit::TestCase
{
public:
  TestMessageProcessor(const std::string& name);

  ~TestMessageProcessor();
  
  void setUp();
  
  void tearDown();
  
  /**
   * Simulate the reception of bytes from the wire and pass it on to the
   * MessageProcessor for extraction of message objects
   */
  void testMessageProcessing();
  
  static CppUnit::Test* suite();
  
};

#endif /*TESTMESSAGEHEADER_H_*/
