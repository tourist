/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestMessageTypeData.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "Poco/Instantiator.h"

using Tourist::Message::MessageTypeData;
using Tourist::Message::MsgCacheXferRequest;
using Poco::Instantiator;

TestMessageTypeData::TestMessageTypeData(const std::string& name):
   CppUnit::TestCase(name)
{  
}

TestMessageTypeData::~TestMessageTypeData()
{  
}

void TestMessageTypeData::setUp()
{  
}

void TestMessageTypeData::tearDown()
{  
}

void TestMessageTypeData::testMessageRegistration()
{
  MessageTypeData msgTypeData;
  
  MsgCacheXferRequest request("Test", "Source", 1, 3, 7);
  string rpc;
  int stat =request.getString(rpc);
  assert(stat == 0);
  assert(request.isInErrorState() == false);
  
  stat = msgTypeData.registerMessageObj(999, 
      new Instantiator<MsgCacheXferRequest, AbstractMessage>);
  assert (stat == 0);
  
  AbstractMessage *msg = msgTypeData.createInstance(999);
  assert(msg != NULL);
  assert(dynamic_cast<MsgCacheXferRequest*>(msg) != 0);
  msg -> setData(const_cast<char*>(rpc.c_str()), rpc.length());
  
  //The interested component should know the mapping of the message
  //to do a dynamic cast Or at can check for NULL for each possible 
  //cast to find out the right match - a tedious job
  MsgCacheXferRequest *dyncReq = dynamic_cast<MsgCacheXferRequest*>(msg);  
  assert (dyncReq != NULL);
  assert(dyncReq -> isInErrorState() == false);
  assert(dyncReq -> getComponentName() == "Test");
  assert(dyncReq -> getStartLevel() == 1);
  assert(dyncReq -> getEndLevel() == 3);
  assert(dyncReq -> getCacheType() == 7);
}

CppUnit::Test* TestMessageTypeData::suite()
{
  CppUnit::TestSuite * pSuite = new CppUnit::TestSuite("TestMessageHeader");
  CppUnit_addTest(pSuite, TestMessageTypeData, testMessageRegistration);
    
  return pSuite;
}
