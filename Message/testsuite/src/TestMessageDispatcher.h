#ifndef TESTMESSAGEDISPATCHER_H_
#define TESTMESSAGEDISPATCHER_H_

#include "CppUnit/TestCase.h"
#include "Tourist/Message/MessageDispatcher.h"
#include "Tourist/Message/MessageNotification.h"
#include "Poco/AutoPtr.h"

using Tourist::Message::MessageDispatcher;
using Tourist::Message::MessageNotification;
using Poco::AutoPtr;

class TestMessageDispatcher: public CppUnit::TestCase
{
public:
  TestMessageDispatcher(const std::string& name);
  ~TestMessageDispatcher();

  void setUp();
  void tearDown();
  /**
   * A test case for one event notification
   */
  void testSingleEventDispatch();

  /**
   * Test for multiple event notification
   */
  void testMultiEventDispatch();

  /**
   * Single event multiple observers(or say it listeners)
   */
  void testMultiMethodDispatch();

  /**
   * Test to see how many messages per seconds we can get
   */

  void testDispatcherPerformance();

  static CppUnit::Test* suite();

private:
  MessageDispatcher dispatcher;
  void onMessage(const AutoPtr<MessageNotification> &notification);
  void onMessage2(const AutoPtr<MessageNotification> &notification);
  void onMessage3(const AutoPtr<MessageNotification> &notification);
  int notificationCount, notificationCount2;
};

#endif /*TESTMESSAGEDISPATCHER_H_*/

