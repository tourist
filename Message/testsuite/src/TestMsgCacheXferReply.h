#ifndef TESTMSGCACHETRANSFERREQUEST_H_
#define TESTMSGCACHETRANSFERREQUEST_H_

#include "CppUnit/TestCase.h"

class TestMsgCacheXferReply : public CppUnit::TestCase
{
public:  
  TestMsgCacheXferReply(const std::string& name);
  
  ~TestMsgCacheXferReply();
  
  void setUp();
  
  void tearDown();
  
  //Test the features of MessageHeader object alone.
  void testMsgCacheXferRepSerialize();
  
  //Test the header with an attached message object.
  void testMsgCacheXferRepDeserialize();
  
  static CppUnit::Test* suite();
  
};

#endif /*TESTMSGCACHETRANSFERREQUEST_H_*/
