/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestRemoteNode.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Message/MessageDispatcher.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "Tourist/AbstractMessage.h"
#include "Tourist/Net/TransportFactory.h"
#include "Tourist/Net/TCPPipe.h"
#include "Tourist/Util/Config.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"
#include "Poco/Thread.h"
#include "Poco/Net/SocketAddress.h"

using Tourist::Message::RemoteNode;
using Tourist::AbstractMessage;
using Tourist::Net::TCPPipe;
using Tourist::Net::TransportFactory;
using Tourist::Message::MessageDispatcher;
using Tourist::Message::MessageHeader;
using Tourist::Message::MessageTypeData;
using Tourist::Message::MsgCacheXferRequest;
using Tourist::Message::MessageNotification;

using Poco::NObserver;
using Poco::Observer;
using Poco::Thread;
using Poco::Net::SocketAddress;

namespace
{

class TransportCallback : public CallbackInterface
{
public:

  TransportCallback() :
    isNotified(false), pipe(NULL)
  {
  }

  void callback(void *_pipe)
  {
    isNotified = true;

    if (_pipe != NULL)
     pipe = (AbstractPipe*) _pipe;
  }

  AbstractPipe *pipe;

  //Set to true if the callback is called at least once.
  //Used to see if our notification are being passed correctly.
  bool isNotified;
};

}//namespace

TestRemoteNode::TestRemoteNode(const std::string& name):
   CppUnit::TestCase(name)
{  
}

TestRemoteNode::~TestRemoteNode()
{  
}

void TestRemoteNode::setUp()
{  
}

void TestRemoteNode::tearDown()
{  
}

void TestRemoteNode::testMessageExchange()
{
  //start twho seprate network systems for this test
  //Start the trasnport-1 TCP
  TransportFactory transport1;
  Config config1;
  try
  {
    config1.setTCPEnable(true);
    config1.setTCPPort(5000);

    transport1.initServers(config1);
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  }

  assert(transport1.getTCP() == true);
  assert(transport1.getTCPPort() == 5000);

  //Start the trasnport-2 TCP
  TransportFactory transport2;
  Config config2;
  try
  {
    config2.setTCPEnable(true);
    config2.setTCPPort(5001);

    transport2.initServers(config2);
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  }
  
  //getPipe from transport-1 
  //and pipe callback for transport 2 to build link
  
  assert(transport1.getTCP() == true);
  assert(transport2.getTCPPort() == 5001);

  TransportCallback callbackFrom1, callbackFrom2;

  assert(callbackFrom1.isNotified == false);
  assert(callbackFrom2.isNotified == false);

  transport1.registerConnectionNotification(&callbackFrom1);
  transport2.registerConnectionNotification(&callbackFrom2);

  //These represent pipes which we will get from the transport
  //one directly and the other via a callback as transport-1 cnonects
  //with the transport-2 
  AbstractPipe *remotePipeFrom1= NULL, *remotePipeFrom2= NULL;

  try
  {
    SocketAddress address(transport2.getHostName(), transport2.getTCPPort());
    remotePipeFrom1 = transport1.getPipe(address, TCP);

    Thread::sleep(100);
    remotePipeFrom2 = callbackFrom2.pipe;

    //created pipe shouldn't be null
    assert(remotePipeFrom1 != NULL);
    assert(remotePipeFrom2 != NULL);
    //type cast
    assert(dynamic_cast<TCPPipe*> (remotePipeFrom1) != 0);
    assert(dynamic_cast<TCPPipe*> (remotePipeFrom2) != 0);
  }
  catch (Poco::Exception &e)
  {
    fail(e.what());
  }
  
  MessageDispatcher dispatcher;  
  MessageTypeData typeData;
  dispatcher.registerNotification("Test", NObserver<TestRemoteNode, MessageNotification>(*this, &TestRemoteNode::onMessage));
  
  config1.setMessageTypeHandler(&typeData);
  config1.setMessageBus(&dispatcher);
  
  config2.setMessageTypeHandler(&typeData);
  config2.setMessageBus(&dispatcher);
  
  RemoteNode remote1(&config1);
  RemoteNode remote2(&config2);
  
  remote1.setPipe(remotePipeFrom1);
  remote2.setPipe(remotePipeFrom2);
  
  //Our two test nodes are connected.
  //Its time to stream messages to them to
  this -> messageDispatchCount = 0;
  MsgCacheXferRequest req[10];
  
  //populating req objects
  for (int i=0; i<10; i++)
  {
    req[i].setStartLevel(0);
    req[i].setEndLevel(7);
    req[i].setCacheType(1);
    req[i].setComponentName("Test");
  }
  
  string serialized;
  int status = req[0].getString(serialized);
  assert (status == 0);
  
  MessageHeader header(MessageHeader::HEADER_VERSION, serialized.length(), req[0].getType());
  
  //sending message objects.
  for (int i=0; i<10; i++)
  {
    int status = remote2.sendMessage(&header, &req[i]);
    assert (status > 0);
  }
  
  Thread::sleep(500);
 
  fprintf(stderr, "Dispatch %d ", messageDispatchCount); 
  assert (this -> messageDispatchCount == 10);
}

void TestRemoteNode::onMessage(const AutoPtr<MessageNotification> &notification)
{  
  fprintf(stderr, "Got a dispatch");
  this -> messageDispatchCount++;
}

CppUnit::Test* TestRemoteNode::suite()
{
  CppUnit::TestSuite * pSuite = new CppUnit::TestSuite("TestRemoteNode");
  CppUnit_addTest(pSuite, TestRemoteNode, testMessageExchange);  
  
  return pSuite;
}
