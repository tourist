#ifndef TESTMESSAGETYPEDATA_H_
#define TESTMESSAGETYPEDATA_H_

#include "CppUnit/TestCase.h"

class TestMessageTypeData : public CppUnit::TestCase
{
public:
  TestMessageTypeData(const std::string& name);
  
  ~TestMessageTypeData();
  
  void setUp();
  
  void tearDown();
  
  //Tests the ability to register custom message against given type
  //an object which our header processing mechanism could process.
  void testMessageRegistration();
  
   static CppUnit::Test* suite();
  
};

#endif /*TESTMESSAGETYPEDATA_H_*/
