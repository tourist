/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestMessageProcessor.h"
#include "Tourist/Message/MessageProcessor.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "Tourist/Message/MessageTypeData.h"
#include "Tourist/Util/Config.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include <time.h>

using Tourist::Message::MessageProcessor;
using Tourist::Message::MessageHeader;
using Tourist::Message::MsgCacheXferRequest;
using Tourist::Message::MessageTypeData;
using Tourist::Util::Config;

TestMessageProcessor::TestMessageProcessor(const std::string& name):
   CppUnit::TestCase(name)
{  
}

TestMessageProcessor::~TestMessageProcessor()
{  
}

void TestMessageProcessor::setUp()
{  
}

void TestMessageProcessor::tearDown()
{  
}

void TestMessageProcessor::testMessageProcessing()
{
  Config config;
  MsgCacheXferRequest testMsgObj_1("Test-1", "Source", 1, 7, 3);
  assert (testMsgObj_1.isInErrorState() == false);  
  string rpc_payload_1;
  int stat = testMsgObj_1.getString(rpc_payload_1);
  assert(stat == 0);

  MsgCacheXferRequest testMsgObj_2("Test-2", "Source", 1, 7, 3);
  assert (testMsgObj_2.isInErrorState() == false);
  string rpc_payload_2;
  stat = testMsgObj_2.getString(rpc_payload_2);
  assert(stat == 0);

  MessageHeader messageHeader_1(1, rpc_payload_1.length(), 1);
  assert(messageHeader_1.isInErrorState() == false);
  string rpc_header_1;
  stat = messageHeader_1.getString(rpc_header_1);
  assert(stat == 0);

  MessageHeader messageHeader_2(1, rpc_payload_2.length(), 1);
  assert(messageHeader_2.isInErrorState() == false);
  string rpc_header_2;
  stat = messageHeader_2.getString(rpc_header_2);
  assert(stat == 0);

  const char *b_rpc_header_1 = rpc_header_1.c_str();
  const char *b_rpc_header_2 = rpc_header_2.c_str();
  const char *b_rpc_payload_1 = rpc_payload_1.c_str();
  const char *b_rpc_payload_2 = rpc_payload_2.c_str();

  int len_1a = rpc_header_1.length();
  int len_1b = rpc_payload_1.length();
  int len_2a = rpc_header_2.length();
  int len_2b = rpc_payload_1.length();

  int simulated_stream_len = len_1a + len_1b + len_2a + len_2b;

  srand(time(NULL));  

  //We will divide the sequence of bytes [header + message + header + message] at
  //random positions and add them to MessageProcessor class to see if we are able
  //to recover message irrespective of number of bytes received at a particular instance.
  int rand1 = 0;
  do 
  {
    rand1 = rand() % (simulated_stream_len);
  } while (rand1 < 0);

  int len1 = rand1;
  int len2 = simulated_stream_len - len1;

  char *simulated_stream = new char[simulated_stream_len];
  memcpy(simulated_stream, b_rpc_header_1, len_1a);
  memcpy(simulated_stream + len_1a, b_rpc_payload_1, len_1b);
  memcpy(simulated_stream + (len_1a + len_1b), b_rpc_header_2, len_2a);
  memcpy(simulated_stream + (len_1a + len_1b + len_2a), b_rpc_payload_2, len_2b);

  /*FILE *file2;    
  file2 = fopen("/tmp/1out.txt", "a");
  for (int i=0; i<simulated_stream_len; i++)
    fprintf(file2, "%c", simulated_stream[i]);
  */

  MessageTypeData typeHandler;
  MessageProcessor processor(&typeHandler);
  
  char *stream_1 = new char[len1];
  memcpy(stream_1, simulated_stream, len1);

  FILE *file2;  
  file2 = fopen("/tmp/out1.txt", "a");
  for (int i=0; i<len1; i++)
    fprintf(file2, "%c", stream_1[i]);

  processor.addBytes(stream_1, len1);

  stream_1 = new char[len2];
  memcpy(stream_1, simulated_stream + len1, len2);
  processor.addBytes(stream_1, len2);

  AbstractMessage *message1 = processor.popMessage();
  assert(message1 != NULL);
  MsgCacheXferRequest *req = dynamic_cast<MsgCacheXferRequest*>(message1);
  assert(req != NULL);
  assert(req -> isInErrorState() == false);
  assert(req -> getComponentName() == "Test-1");
  assert(req -> getStartLevel() == 1);
  assert(req -> getEndLevel() == 7);
  assert(req -> getCacheType() == 3);

  //for test object 2
  message1 = processor.popMessage();
  assert(message1 != NULL);
  req = dynamic_cast<MsgCacheXferRequest*>(message1);
  assert(req != NULL);
  assert(req -> isInErrorState() == false);
  assert(req -> getComponentName() == "Test-2");
  assert(req -> getStartLevel() == 1);
  assert(req -> getEndLevel() == 7);
  assert(req -> getCacheType() == 3);    
}

CppUnit::Test* TestMessageProcessor::suite()
{
  CppUnit::TestSuite *pSuite = new CppUnit::TestSuite("TestMessageProcessor");
  CppUnit_addTest(pSuite, TestMessageProcessor, testMessageProcessing);
  
  return pSuite;
}
