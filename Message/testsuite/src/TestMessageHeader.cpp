/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestMessageHeader.h"
#include "Tourist/Message/MessageHeader.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "Tourist/Util/Config.h"
#include "Tourist/Util/common.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"

using Tourist::Message::MessageHeader;
using Tourist::Message::MsgCacheXferRequest;
using Tourist::Util::Config;

using namespace hessian;

TestMessageHeader::TestMessageHeader(const std::string& name):
   CppUnit::TestCase(name)
{  
}

TestMessageHeader::~TestMessageHeader()
{  
}

void TestMessageHeader::setUp()
{  
}

void TestMessageHeader::tearDown()
{  
}

void TestMessageHeader::testMessageSerialize()
{
  //serialize
  MessageHeader messageHeader(1, 10, 15);
  assert (messageHeader.isInErrorState() == false);
  
  string serialized;
  int status = messageHeader.getString(serialized);
  assert (status == 0);
  assert (messageHeader.isInErrorState() == false);
  
  try
  {
    auto_ptr<input_stream> is(new string_input_stream(serialized));
    hessian_input hi(is);
    MessageHeader messageHeader2(hi);

    assert(messageHeader2.isInErrorState() == false);
    assert(messageHeader2.getVersion() == 1);
    assert(messageHeader2.getPayloadSize() == 10);
    assert(messageHeader2.getPayloadType() == 15);
  }
  catch (hessian::exceptions::io_exception &e)
  {
    fail(e.what());
  }
}

void TestMessageHeader::testMessageDeserialize()
{
  hessian_output ho;
  string rpc;
  try 
  {
    rpc = ho.start_call("header");
    rpc = ho.set_parameter(rpc, new Integer(1));
    rpc = ho.set_parameter(rpc, new Integer(10));
    rpc = ho.set_parameter(rpc, new Integer(15));
    rpc = ho.complete_call(rpc);
    
    auto_ptr<input_stream> is(new string_input_stream(rpc));
    hessian_input hi(is);

    MessageHeader messageHeader(hi);
    assert(messageHeader.isInErrorState() == false);
    assert(messageHeader.getVersion() == 1);
    assert(messageHeader.getPayloadSize() == 10);
    assert(messageHeader.getPayloadType() == 15);  
  }
  catch (hessian::exceptions::io_exception &e)
  {
    fail(e.what());
  }  
}

void TestMessageHeader::testMessageExtraction()
{
  Config conf;
  //createing the message we are interested in sending
  MsgCacheXferRequest request("Test", "Source", 1, 7, 3);
  assert (request.isInErrorState() == false);

  string rpc_payload;
  int stat = request.getString(rpc_payload);
  assert (stat == 0);
  
  MessageHeader messageHeader(1, rpc_payload.length(), 1);
  assert (messageHeader.isInErrorState() == false);
  
  string rpc_header;
  stat = messageHeader.getString(rpc_header); 
  assert (stat == 0);  
  assert (messageHeader.isInErrorState() == false);
  
  //Appending header on top of our payload
  const char *b_rpc_header = rpc_header.c_str();
  const char *b_rpc_payload = rpc_payload.c_str();
  
  char header_payload[rpc_header.length() + rpc_payload.length()];
  memcpy(header_payload, b_rpc_header, rpc_header.length());
  memcpy(header_payload + rpc_header.length(), b_rpc_payload, 
      rpc_payload.length());

  FILE *out;
  out = fopen("/tmp/2out.txt", "a");
  for (int i=0; i< rpc_header.length() + rpc_payload.length(); i++)
    fprintf(out, "%c", header_payload[i]); 

//The header_payload should now fly to remote end and
  //we should be able to extract header from the payload.
  
  char header[MessageHeader::HEADER_SIZE];
  memcpy(header, header_payload, MessageHeader::HEADER_SIZE);
    
  try
  {
    auto_ptr<input_stream> is(new char_input_stream(
                     (string::size_type)MessageHeader::HEADER_SIZE, header));
    hessian_input hi(is);

    MessageHeader messageHeader(hi);
    assert(messageHeader.isInErrorState() == false);
    assert(messageHeader.getVersion() == 1);
    assert(messageHeader.getPayloadSize() == rpc_payload.length());
    assert(messageHeader.getPayloadType() == 1);
    
    int payloadSize = messageHeader.getPayloadSize();
    char payload[payloadSize];
    memcpy(payload, header_payload + MessageHeader::HEADER_SIZE, payloadSize);
    
    auto_ptr<input_stream> is2(new char_input_stream(
               (string::size_type)payloadSize, payload));
    hessian_input hi2(is2);

    MsgCacheXferRequest request2(hi2);
    assert(request2.isInErrorState() == false);
    assert(request2.getComponentName() == "Test");
    assert(request2.getStartLevel() == 1);
    assert(request2.getEndLevel() == 7);
    assert(request2.getCacheType() == 3);
    
  }
  catch (hessian::exceptions::io_exception &e)
  {
    fail(e.what());
  }  
}

CppUnit::Test* TestMessageHeader::suite()
{
  CppUnit::TestSuite * pSuite = new CppUnit::TestSuite("TestMessageHeader");
  CppUnit_addTest(pSuite, TestMessageHeader, testMessageSerialize);
  CppUnit_addTest(pSuite, TestMessageHeader, testMessageDeserialize);
  CppUnit_addTest(pSuite, TestMessageHeader, testMessageExtraction);
  
  return pSuite;
}
