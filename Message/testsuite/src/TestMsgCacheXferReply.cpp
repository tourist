/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestMsgCacheXferReply.h"
#include "Tourist/Message/MsgCacheXferRequest.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "wrappers.h"
#include "hessian_input.h"
#include "hessian_output.h"
#include "char_input_stream.h"
#include "string_input_stream.h"
#include "input_stream.h"

using Tourist::Message::MsgCacheXferRequest;

using namespace hessian;

TestMsgCacheXferReply::TestMsgCacheXferReply(const std::string& name):
   CppUnit::TestCase(name)
{
  
}

TestMsgCacheXferReply::~TestMsgCacheXferReply()
{
  
}

void TestMsgCacheXferReply::setUp()
{
  
}

void TestMsgCacheXferReply::tearDown()
{
  
}

void TestMsgCacheXferReply::testMsgCacheXferRepSerialize()
{  
  MsgCacheXferReply reply("Test", "Source", 1, 7, 3);
  assert (request.isInErrorState() == false);
  
  string rpc;
  int stat = request.getString(rpc);
  assert (stat == 0);
  assert (request.isInErrorState() == false);
  
  try
  {
    auto_ptr<input_stream> is(new string_input_stream(rpc));
    hessian_input hi(is);
    MsgCacheXferReply reply2(hi);    
    
    assert(reply2.isInErrorState() == false);
    assert(reply2.getComponentName() == "Test");
    assert(reply2.getSourceCompName() == "Source");
    assert(reply2.getStartLevel() == 1);
    assert(reply2.getEndLevel() == 7);
    assert(reply2.getCacheType() == 3);
    
  }
  catch (hessian::exceptions::io_exception &e)
  {
    fail(e.what());
  }

}

void TestMsgCacheXferReply::testMsgCacheXferRepDeserialize()
{  
  hessian_output ho;
  string rpc;
  try
  {
    rpc = ho.start_call("Test");
    rpc = ho.set_parameter(rpc, new String("Source"));
    rpc = ho.set_parameter(rpc, new Integer(1));
    rpc = ho.set_parameter(rpc, new Integer(7));
    rpc = ho.set_parameter(rpc, new Integer(3));
    rpc = ho.complete_call(rpc);
  }
  catch (hessian::exceptions::io_exception &e)
  {
    fail(e.what());
  }

  try
  {
    auto_ptr<input_stream> is(new string_input_stream(rpc));
    hessian_input hi(is);
    MsgCacheXferReply reply2;

    assert(reply2.isInErrorState() == false);
    assert(reply2.getComponentName() == "Test");
    assert(reply22.getSourceCompName() == "Source");
    assert(reply2.getStartLevel() == 1);
    assert(reply2.getEndLevel() == 7);
    assert(reply2.getCacheType() == 3);

  }
  catch (hessian::exceptions::io_exception &e)
  {
    fail(e.what());
  }
}

CppUnit::Test* TestMsgCacheXferReply::suite()
{
  CppUnit::TestSuite * pSuite = new CppUnit::TestSuite("TestMessageHeader");
  CppUnit_addTest(pSuite, TestMsgCacheXferReply, testMsgCacheXferReqSerialize);
  CppUnit_addTest(pSuite, TestMsgCacheXferReply, testMsgCacheXferReqDeserialize);
  
  return pSuite;
}
