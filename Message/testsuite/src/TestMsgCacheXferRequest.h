#ifndef TESTMSGCACHETRANSFERREQUEST_H_
#define TESTMSGCACHETRANSFERREQUEST_H_

#include "CppUnit/TestCase.h"

class TestMsgCacheXferRequest : public CppUnit::TestCase
{
public:  
  TestMsgCacheXferRequest(const std::string& name);
  
  ~TestMsgCacheXferRequest();
  
  void setUp();
  
  void tearDown();
  
  //Test the features of MessageHeader object alone.
  void testMsgCacheXferReqSerialize();
  
  //Test the header with an attached message object.
  void testMsgCacheXferReqDeserialize();
  
  static CppUnit::Test* suite();
  
};

#endif /*TESTMSGCACHETRANSFERREQUEST_H_*/
