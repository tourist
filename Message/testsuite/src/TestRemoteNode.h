#ifndef TESTREMOTENODE_H_
#define TESTREMOTENODE_H_

#include "CppUnit/TestCase.h"
#include "Poco/AutoPtr.h"
#include "Tourist/Message/MessageNotification.h"

class TestRemoteNode : public CppUnit::TestCase
{
public:  
  TestRemoteNode(const std::string& name);
  
  ~TestRemoteNode();
  
  void setUp();
  
  void tearDown();
  
  //Tests: 
  // a) reception of bytes from network
  // b) extraction of header an payload
  // c) dispatching the message object.
  void testMessageExchange();
  
  static CppUnit::Test* suite();

private:
  void onMessage(const Poco::AutoPtr<Tourist::Message::MessageNotification> &notification);
  
  //number of messages received by onMessage.
  int messageDispatchCount;
};

#endif /*TESTREMOTENODE_H_*/
