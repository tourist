/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestMessageDispatcher.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/AbstractMessage.h"
#include "Tourist/TouristExceptions.h"
#include "Tourist/Util/Config.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "Poco/NObserver.h"
#include <sys/time.h>

using Tourist::Message::RemoteNode;
using Tourist::AbstractMessage;
using Tourist::Util::Config;
using Poco::NObserver;

namespace {
  
  //A message notification carrier for tests.
  //provides implementation for all the abstract messages
  //in the 
  class DummyMessage : public AbstractMessage
  {
    public: 
    
    DummyMessage(string _component, string _message)
    {
      component = _component;
      message = _message;    
    }
    
    string getComponentName()
    {
      return component;          
    }

    string getSourceCompName()
    {
      return sourceComp;      
    }
    
    int getString(string &s)
    {      
      s = message;
      return 0;
    }
    
    int getType()
    {
      return HESSIAN;
    }
    
    bool isInErrorState()
    {
      return false;    
    }
    
    int setData(char* data, int size)
    {
      return -1;
    }
    
    int type;
    string component, message, sourceComp;
    
  };
}

TestMessageDispatcher::TestMessageDispatcher(const std::string& name):
              CppUnit::TestCase(name)
{
}

TestMessageDispatcher::~TestMessageDispatcher()
{
}

void TestMessageDispatcher::setUp()
{
  try
  {
    //Initialization of conf is required to enable the logging.
    Config conf;    
  }
  catch (Exception &e)
  {
    string emsg = e.what();
    fail("Couldn't get instance of MessageDispatcher " + emsg);
  }
}

void TestMessageDispatcher::tearDown()
{  
}

void TestMessageDispatcher::testSingleEventDispatch()
{
  try
  {
    dispatcher.registerNotification("testCallback",
      NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage));

    assert(dispatcher.getEventCount() == 1);
    notificationCount = 0;
    RemoteNode rn(NULL, "0123456789abcdef0123456789abcdef", 0);
    MessageNotification *notification = new MessageNotification();
    notification -> sender = &rn;
    notification -> message = new DummyMessage("testCallback", "test");
    dispatcher.dispatchEvent(notification);
    assert(notificationCount == 1);

    dispatcher.deregisterNotification("testCallback",
       NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage));

    assert(dispatcher.getEventCount() == 0);
  }
  catch (Exception &e)
  {
    string emsg = e.what();
    fail(emsg);
  }
}

void TestMessageDispatcher::testMultiEventDispatch()
{
  notificationCount = 0;
  int index = 0;
  vector<string> callbacks;
  callbacks.push_back("callback1");
  callbacks.push_back("callback2");
  callbacks.push_back("callback3");

  assert(dispatcher.getEventCount() == 0);
  try
  {
    //
    dispatcher.registerNotifications(callbacks,
      NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage2));

    assert(dispatcher.getEventCount() == callbacks.size());

    notificationCount = 0;
    RemoteNode rn(NULL, "0123456789abcdef0123456789abcdef", 0);
    MessageNotification *notification = new MessageNotification();

    for (index = 0; index<callbacks.size(); index++)
    {
      notification -> sender = &rn;
      notification -> message = new DummyMessage(callbacks[index], "test");
      dispatcher.dispatchEvent(notification);
    }
    assert(notificationCount == callbacks.size());

    dispatcher.deregisterNotifications(callbacks,
       NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage2));

    assert(dispatcher.getEventCount() == 0);
    
    delete notification;
  }
  catch (Exception &e)
  {
    string emsg = e.what();
    fail(emsg);
  }
}

void TestMessageDispatcher::testMultiMethodDispatch()
{
  notificationCount = notificationCount2 = 0;
  assert(dispatcher.getEventCount() == 0);
  try
  {
    //We will register separate methods for one component name
    //to see if the notification is recieved by them or not.
    dispatcher.registerNotification("Component",
      NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage2));

    dispatcher.registerNotification("Component",
     NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage3));

    assert(dispatcher.getEventCount() == 1);

    RemoteNode rn(NULL, "0123456789abcdef0123456789abcdef", 0);

    //Sending notification for the first method.
    MessageNotification *notification = new MessageNotification();
    notification -> sender = &rn;
    notification -> message = new DummyMessage("Component", "test");
    dispatcher.dispatchEvent(notification);

    assert(notificationCount == 1);
    assert(notificationCount2 == 1);

    //Second notification
    MessageNotification *notification2 = new MessageNotification();
    notification2 -> sender = &rn;
    notification2 -> message = new DummyMessage("Component", "test2");
    dispatcher.dispatchEvent(notification2);

    assert(notificationCount == 2);
    assert(notificationCount2 == 2);

    dispatcher.deregisterNotification("Component",
       NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage2));

    dispatcher.deregisterNotification("Component",
       NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage3));

    assert(dispatcher.getEventCount() == 0);
    
    delete notification;
  }
  catch (Exception &e)
  {
    string emsg = e.what();
    fail(emsg);
  }
}

void TestMessageDispatcher::testDispatcherPerformance()
{
  //We will try to dispatcher events for single component
  //to see how much performance we are getting out of out.
  // (message per seconds)

  notificationCount = 0;
  time_t start = time(NULL);
  int count = 0, totalMsgs = 10000;
  assert(dispatcher.getEventCount() == 0);
  try
  {
    dispatcher.registerNotification("Component",
      NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage2));

    assert(dispatcher.getEventCount() == 1);


    RemoteNode rn(NULL, "0123456789abcdef0123456789abcdef", 0);
    //Sending notification for the first method.
    MessageNotification *notification = new MessageNotification();
    notification -> sender = &rn;
    DummyMessage dummyMsg("Component", "test");    
    notification -> message = new DummyMessage("Component", "test");

    for (count = 0; count<totalMsgs; count++)
    {
      dispatcher.dispatchEvent(notification);
    }
    int delay = time(NULL) - start;

    assert(delay != 0);
    assert(notificationCount == count);

    cout<<"Msgs/sec = "<<(double)(count/delay)<<endl;

    dispatcher.deregisterNotification("Component",
       NObserver<TestMessageDispatcher, MessageNotification>(*this, &TestMessageDispatcher::onMessage2));

    assert(dispatcher.getEventCount() == 0);
    
    //cleanup
    delete notification;    
  }
  catch (Exception &e)
  {
    string emsg = e.what();
    fail(emsg);
  }
}

void TestMessageDispatcher::onMessage(const AutoPtr<MessageNotification>& notification)
{
  notificationCount++;
  assert(notification -> sender -> hexstring() == "0123456789abcdef0123456789abcdef");
  
  string component = notification -> message -> getComponentName();
  
  string message;
  notification -> message -> getString(message); 
  assert(component == "testCallback");
  assert(message == "test");
}

void TestMessageDispatcher::onMessage2(const AutoPtr<MessageNotification>& notification)
{
  notificationCount++;
}

void TestMessageDispatcher::onMessage3(const AutoPtr<MessageNotification>& notification)
{
   notificationCount2++;
}

CppUnit::Test* TestMessageDispatcher::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestMessageDispatcher");
  CppUnit_addTest(pSuite, TestMessageDispatcher, testSingleEventDispatch);
  CppUnit_addTest(pSuite, TestMessageDispatcher, testMultiEventDispatch);
  CppUnit_addTest(pSuite, TestMessageDispatcher, testMultiMethodDispatch);
  //We will use the correct method for testing performance
  //CppUnit_addTest(pSuite, TestMessageDispatcher, testDispatcherPerformance);
  return pSuite;
}
