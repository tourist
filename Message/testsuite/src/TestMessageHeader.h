#ifndef TESTMESSAGEHEADER_H_
#define TESTMESSAGEHEADER_H_

#include "CppUnit/TestCase.h"

class TestMessageHeader : public CppUnit::TestCase
{
public:
  TestMessageHeader(const std::string& name);
  ~TestMessageHeader();
  
  void setUp();
  
  void tearDown();
  
  //Test the features of MessageHeader object alone.
  void testMessageSerialize();
  
  //Test the header with an attached message object.
  void testMessageDeserialize();
  
  //Test message extraction out of the header
  //which goes like this: Read the header find the length of payload
  //separate payload bytes from the header and process them as separte
  //message based on its type
  void testMessageExtraction();
  
  static CppUnit::Test* suite();
  
};

#endif /*TESTMESSAGEHEADER_H_*/
