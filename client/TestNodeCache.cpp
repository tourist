/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/LocalNode.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/AvlCache.h"

using Tourist::LocalNode;
using Tourist::Message::RemoteNode;

int main()
{
  //Tourist::NodeSet set;
  LocalNode nodeCache;

  RemoteNode toSearch;
  RemoteNode rn, rn2, rn3;

  toSearch  = rn;

  nodeCache.addRemote(&rn);
  nodeCache.addRemote(&rn2);
  nodeCache.addRemote(&rn3);

  fprintf(stderr, "Total size of cache %d\n", nodeCache.getNodeCount(0));
  Tourist::NodeSet rs;
  nodeCache.getNodes(1, rs, PREFIX);
 
  fprintf(stderr, "Total fetched %d\n", rs.treeSize);

  Node *result = NULL;
  bool status = nodeCache.findId(toSearch, &result);
  std::cout<<"status = "<<status<<std::endl;

  if (toSearch == rn)
    std::cout<<"THey are equal"<<std::endl;



  return 0;
}
