/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/LocalNode.h"
#include "Tourist/App/Application.h"
#include "Tourist/Message/RemoteNode.h"
#include "Tourist/Util/Util.h"
#include "Poco/Thread.h"

using Tourist::App::Application;
using Tourist::Message::RemoteNode;	
using Tourist::LocalNode;

using Poco::Thread;

using namespace Tourist::Util;

int main(int argc, char **argv)
{
  
  /*LocalNode node1;
  std::cout<<"Node ID = " <<node1.getID()<<endl;

  RemoteNode rNode(NULL, node1.getID(), 0);
  std::cout<<"rNode ID = " <<rNode.toString()<<std::endl;
  std::cout<<"rNode ID = " <<rNode.toString()<<std::endl;*/
  if (argc < 2)
  { 
    fprintf(stderr, "Missing arguments!
");    
    return -1;
  }

  Config config;
  config.setTCPEnable(true);
  config.setTCPPort(5001);  

  Application app(config);   
  app.init();
  
  Thread t;
  t.start(app);
  
  int rPort = atoi(argv[1]);

  RemoteNode *rnode = NULL;
  int status = app.connectionTo("localhost", rPort, TCP, &rnode, 5);
  fprintf(stderr, "Status of remote connection is %d
", status);

  if (rnode != NULL)
  {
    std::cout<<rnode -> toString()<<std::endl;
  }

  while (1)
  {
    Thread::sleep(5000);
  }
}
