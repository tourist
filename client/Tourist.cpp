// Peer test

#include "Poco/Util/ServerApplication.h"
#include "Poco/Task.h"
#include "Poco/TaskManager.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Util/AbstractConfiguration.h"
#include "Poco/Util/IntValidator.h"
#include "Poco/AutoPtr.h"
#include "Poco/Thread.h"
#include <ctime>
#include <sys/time.h>
#include <iostream>
#include <string>

#include "Tourist/App/Application.h"
#include "Tourist/Util/Config.h"


//#include "Poco/FileChannel.h"

using Poco::Thread;
using Poco::Util::Application;

//using Poco::FileChannel;
using Poco::Util::ServerApplication;
using Poco::Task;
using Poco::TaskManager;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;
using Poco::Util::AbstractConfiguration;
using Poco::Util::OptionCallback;
using Poco::AutoPtr;

class TouristClientTask: public Task {
  public:
    TouristClientTask(): Task("TouristClientTask") {
    }

    void runTask()
    {
      fprintf(stdout, "Tourist sequence initiating!!\n");
      Application &app = Application::instance();

      struct timeval tv;
      struct timezone tz;
      //To ensure that we get fairly random Id on
      // same host. 
      if (gettimeofday(&tv, &tz)==-1)
	srand ( time(NULL) );
      else
	srand (tv.tv_usec); // Should be more random than time()

      std::string logfile = app.config().getString("logfile");
      std::string propfile = app.config().getString("propfile");

      printf("Node properties file %s\n", propfile.c_str());
      printf("Logging properties file %s\n", logfile.c_str());

      Tourist::Util::Config config(logfile, propfile);
      Tourist::App::Application app1(config);

      if(app1.init() != 0) 
      {
        fprintf(stderr, "Failed to initialize the application, check log for detail!");
	return;
      }

      Thread t;
      t.start(app1);

      while(1)
        Thread::sleep(500);
    }
};

class TouristClient: public ServerApplication {
  public:
    TouristClient(): _helpRequested(false) {}

  private:
    bool _helpRequested;

  protected:
    void initialize(Application& self) {
      loadConfiguration(); // load default configuration files, if present
      Application::initialize(self);
      // add your own initialization code here
    }

    void uninitialize() {
      // add your own uninitialization code here
      Application::uninitialize();
    }

    void reinitialize(Application& self) {
      Application::reinitialize(self);
      // add your own reinitialization code here
    }

    void defineOptions(OptionSet& options) {
      Application::defineOptions(options);

      options.addOption(
	  Option("help", "h", "display help information on command line arguments")
	  .required(false)
	  .repeatable(false)
	  .callback(OptionCallback<TouristClient>(this, &TouristClient::handleHelp)));

      /*options.addOption(
	  Option("config-file", "f", "load configuration data from a file")
	  .required(false)
	  .repeatable(true)
	  .argument("FILE")
	  .callback(OptionCallback<TouristClient>(this, &TouristClient::handleConfig)));*/

      options.addOption(
	  Option("properties", "l", "lists all configuration properties")
	  .required(false)
	  .repeatable(false)
	  .binding("application.listprops"));

      options.addOption(
	  Option("propfile", "p", "Tourist node properties file")
	  .required(false)
	  .repeatable(false)
	  .argument("FILE")
	  .binding("propfile"));

      options.addOption(
	  Option("logfile", "o", "logging configuration")
	  .required(false)
	  .repeatable(false)
	  .argument("FILE")
	  .binding("logfile"));

    }

    void handleHelp(const std::string& name, const std::string& value) {
      _helpRequested = true;
      displayHelp();
      stopOptionsProcessing();
    }

    void handleConfig(const std::string& name, const std::string& value) {
      std::cout<<"Name : "<<name<<" value: " <<value<<std::endl;
      loadConfiguration(value);
    }

    void displayHelp() {
      HelpFormatter helpFormatter(options());
      helpFormatter.setCommand(commandName());
      helpFormatter.setUsage("OPTIONS");
      helpFormatter.setHeader("A test application for the tourist libraries");
      helpFormatter.format(std::cout);
    }

    void printProperties(const std::string& base) {
      AbstractConfiguration::Keys keys;
      config().keys(base, keys);
      if (keys.empty()) {
	if (config().hasProperty(base)) {
	  std::string msg;
	  msg.append(base);
	  msg.append(" = ");
	  msg.append(config().getString(base));
	  logger().information(msg);
	}
      } else {
	for (AbstractConfiguration::Keys::const_iterator it = keys.begin();
	    it != keys.end(); ++it) {
	  std::string fullKey = base;
	  if (!fullKey.empty()) fullKey += '.';
	  fullKey.append(*it);
	  printProperties(fullKey);
	}
      }
    }

    int main(const std::vector<std::string>& args) {
      if (config().hasProperty("application.listprops") && !_helpRequested){
	printProperties("");
	return Application::EXIT_OK;
      }

      if (_helpRequested) return Application::EXIT_OK;

      TaskManager tm;
      tm.start(new TouristClientTask);
      waitForTerminationRequest();
      tm.cancelAll();
      tm.joinAll();
      return Application::EXIT_OK;
    }
};

int main(int arg, char** argv) 
{
  TouristClient tourist;
  return tourist.run(arg, argv);
}
