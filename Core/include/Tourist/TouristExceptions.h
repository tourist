#ifndef TOURISTEXCEPTIONS_H_
#define TOURISTEXCEPTIONS_H_

#include "Poco/Exception.h"

using Poco::Exception;

/**
 * Define a macro for defining exceptions. This macro
 * was taken from the poco and slightly modified.
 */

#define TOURIST_DECLARE_EXCEPTION(CLS, BASE) \
  namespace Tourist {  \
  class CLS: public BASE                      \
  {                                 \
  public:                               \
    CLS();                              \
    CLS(const std::string& msg);                  \
    CLS(const std::string& msg, const std::string& arg);      \
    CLS(const std::string& msg, const Poco::Exception& exc);    \
    CLS(const CLS& exc);                      \
    ~CLS() throw();                         \
    CLS& operator = (const CLS& exc);               \
    const char* name() const throw();               \
    const char* className() const throw();              \
    Poco::Exception* clone() const;                 \
    void rethrow() const;                     \
  };                                           \
  } /*namespace Tourist*/


#define TOURIST_IMPLEMENT_EXCEPTION(CLS, BASE, NAME) \
  namespace Tourist {                             \
  CLS::CLS()                                      \
  {                                         \
  }                                         \
  CLS::CLS(const std::string& msg): BASE(msg)                     \
  {                                         \
  }                                         \
  CLS::CLS(const std::string& msg, const std::string& arg): BASE(msg, arg)      \
  {                                         \
  }                                         \
  CLS::CLS(const std::string& msg, const Poco::Exception& exc): BASE(msg, exc)    \
  {                                         \
  }                                         \
  CLS::CLS(const CLS& exc): BASE(exc)                         \
  {                                         \
  }                                         \
  CLS::~CLS() throw()                                 \
  {                                         \
  }                                         \
  CLS& CLS::operator = (const CLS& exc)                       \
  {                                         \
    BASE::operator = (exc);                             \
    return *this;                                 \
  }                                         \
  const char* CLS::name() const throw()                       \
  {                                         \
    return NAME;                                  \
  }                                         \
  const char* CLS::className() const throw()                      \
  {                                         \
    return typeid(*this).name();                          \
  }                                         \
  Poco::Exception* CLS::clone() const                         \
  {                                         \
    return new CLS(*this);                              \
  }                                         \
  void CLS::rethrow() const                             \
  {                                         \
    throw *this;                                  \
  }                                                \
  } //Tourist namespace

TOURIST_DECLARE_EXCEPTION(ConfigurationException, Exception)
TOURIST_DECLARE_EXCEPTION(RuntimeException, Exception)
TOURIST_DECLARE_EXCEPTION(MessageException, Exception)

TOURIST_DECLARE_EXCEPTION(InvalidArgumentException, ConfigurationException)
TOURIST_DECLARE_EXCEPTION(InvalidConfigurationException, ConfigurationException)

TOURIST_DECLARE_EXCEPTION(IndexOutOfBoundException, RuntimeException)
TOURIST_DECLARE_EXCEPTION(NullPointerException, RuntimeException)

TOURIST_DECLARE_EXCEPTION(NetworkException, RuntimeException)
TOURIST_DECLARE_EXCEPTION(AlreadyInitializedException, NetworkException)

TOURIST_DECLARE_EXCEPTION(ComponentNotFoundException, MessageException)


#endif /*TOURISTEXCEPTIONS_H_*/
