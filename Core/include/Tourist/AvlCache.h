#ifndef AVLCACHE_H_
#define AVLCACHE_H_

#include "avlimap.h"
#include "avliset.h"

/**
 * Template class used for Id content comparision
 *
 */
template <class Key> class CompSet
{
//private:
//  Key refKey;

public:
  CompSet(){
  }

  ~CompSet(){
  }

  long compare(const Key t1, const Key t2) const
  {
    if (*t1 == *t2)
      return 0;

    if (*t1 < *t2)
      return -1;

    return 1;
  }
};

/**
 * Compares the Key(Node)'s level
 */
template <class Key> class CompLevel
{
public:
  long compare(const Key t1, const Key t2)
  {
    if (t1.level == t2.level)
      return 0;

    if (t1.level < t2.level)
      return -1;

    return 1;
  }
};

template <class Key, class Compare> class NodeCompSetTemplate:
      public AvliSet <Key, Compare >{
public:
  ~NodeCompSetTemplate(){
    this->empty();
  };
};

template <class Key, class Value, class Compare> class IntNodeMapTemplate:
      public AvliMap <Key, Value, Compare >{
public:
  ~IntNodeMapTemplate(){
    //DoutLevel(6,cerr,"~IntNodeMapTemplate="<<(void*)this<<endl);
    this->empty();
  };
};

//void shortId(ostream& os, Node *id, const char* prefix);

#endif /*AVLCACHE_H_*/
