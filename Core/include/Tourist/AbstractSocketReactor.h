#ifndef ABSTRACTSOCKETREACTOR_H_
#define ABSTRACTSOCKETREACTOR_H_

#include "Poco/Net/Socket.h"
#include "Poco/Observer.h"

using Poco::Observer;
using Poco::Net::Socket;

namespace Tourist {

class AbstractSocketReactor {
  /// Super class for TCP and RUDP socket reactor
public:
  virtual void addEventHandler(const Socket& socket, const Poco::AbstractObserver& observer) = 0;
    ///Add Observer on socket

  virtual void removeEventHandler(const Socket& socket, const Poco::AbstractObserver &observer) = 0;
    ///Remove Observer on socket.

  virtual void start() = 0;
     ///Start the thread

  virtual void stop() = 0;
     ///Stop the reactor.

};

} //namespace Tourist

#endif /*ABSTRACTSOCKETREACTOR_H_*/
