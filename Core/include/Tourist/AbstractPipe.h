#ifndef ABSTRACTPIPE_H_
#define ABSTRACTPIPE_H_

#include "AbstractMessage.h"

/**
 * Abstraction for underlying transfer scheme.
 *
 * The purpose of having such abstract pipe was to let peers/nodes
 * communicate using different transfer protocols like udp, tcp, reliable
 * udp.
 *
 * The only significant method is 'sendMessage' - which itself takesa an
 * instance of AbstractMessage - a furhter abstraction to network payload.
 *
 * For concrete implementations look at Tourist::Net::TCPPipe.
 *
 * \Author Faisal Khan
 */
namespace Tourist {

class AbstractPipe
{
public:

  /*AbstractPipe() 
  {
  }
  
  ~AbstractPipe()
  {
  }*/
  /**
   * Sends given abstract message implementation to remote node.
   *
   *  \param buffer Bytes to send
   *  \bufferSize Number of bytes
   *  
   * \return 0 for success <0 for failure.
   */
  virtual int sendMessage(const char *buffer, const unsigned int bufferSize) = 0;

  /**
   * Number of bytes read into the pipe.
   *
   * Measuring bytes transfered at pipe level is not
   * a very accurate solution. The simplest problem is that
   * as soon as the client disconnects, its pipe is destroyed and
   * we lose counts of any number of bytes transfered in and out.
   *
   */
  virtual int getBytesIn() = 0;

  /**
   * Number of bytes read into the pipe.
   */
  virtual int getBytesOut() = 0;
  
  /**
   * The existing way of being notified whenever there
   * is a new data available to read 
   */
   
  virtual int registerDataNotification(void *) = 0;

  virtual int unregisterDataNotification(void *) = 0;
  
  virtual int registerDisconnectNotification(void *) = 0;

  virtual int unregisterDisconnectNotification(void *) = 0;

  /**
   * Should put all the clean-up code at this point
  */

  virtual void close() = 0;

  virtual bool isValid() = 0;

};

}//Tourist namespace

#endif /*ABSTRACTPIPE_H_*/
