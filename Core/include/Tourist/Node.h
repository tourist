// Node class
// (C) 2005 California Institute of Technology
// Author Conrad Steenberg <conrad@hep.caltech.edu>

#ifndef NODE_H_
#define NODE_H_

//#include "common.h"
#include "Id.h"
#include <cstdlib>
#include <string>

using namespace std;

namespace Tourist {

class Node: public Id
{
public:

  /**
   *  Creates a new node with the default values.
   *
   *  It sets the level,prefix and suffix to zero.
   */
  Node();

  /**
   * Creates a new node with the given level.
   *
   * \param level Level at which this node should run.
   */
  Node(unsigned int level);

  /**
   * Creates a new node with the given Id object.
   *
   * \param 128 bit id in hex
   */
  Node(string id);

   /**
   * Creates a new node with the given Id and level.
   *
   * \param 128 bit id in hex
   * \param level Level of this node.
   */
  Node(string id, unsigned int level);


  ~Node(){
  }

  /**
   * Current level of this node
   * \param new_level Level of the node
   */
  void setLevel(unsigned int new_level);

  /**
   * Current level of this node
   * \param current level.
   */
  int getLevel();

  /**
   * Prefix index of this node.
   *
   * There is no set method for prefix index
   * as it is set using setLevel()
   */
  unsigned int getPrefixIndex();

  /**
   * Suffix index of this node.
   *
   * There is no set method for suffix index as it
   * is set using setLevel()
   */
  unsigned int getSuffixIndex();

  string getName();

  void setName(string n);

private:
  // Members
  ///A user friendly name.
  string userDefinedName;
  ///host name of this node.
  string host;
  ///port number for this node.
  unsigned int port;
  ///Current level of this node.
  unsigned int level;
  ///current prefix index as measured by Id::prefixIndex()
  unsigned int prefix;
  ///current suffix index as measured by Id::suffixIndex()
  unsigned int suffix;
};
} //namespace Tourist

#endif /*NODE_H_*/
