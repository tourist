#ifndef LOCALNODE_H_
#define LOCALNODE_H_

#include "Constants.h"
#include "Node.h"
#include "Id.h"
#include "AvlCache.h"
#include "Poco/SynchronizedObject.h"

using Poco::SynchronizedObject;

namespace Tourist {

typedef NodeCompSetTemplate <Node*, CompSet<Node*> > NodeSet;

typedef IntNodeMapTemplate <unsigned int, NodeSet, CmpOrd <unsigned int> >
IntNodeMap;

typedef AvliSet <unsigned int, CmpOrd <unsigned int> > IntSet;

/**
 * Tourist main data strucutres.
 * Hold the prefix/suffix/backup/random tables.
 * Provides methods for manipulating these tables
 * in a thread safe way.
 */
class LocalNode : public Node
{
public:
  LocalNode();
  /**
   * A new node with given id
   * \param 128 bit number in hex format.
   */
  LocalNode(string id);

  /**
   * Creates a new node with given level
   *
   * \param level of this node.
   */
  LocalNode(int level);
  /**
   * Creates a new node with given id and level
   * \param id 128 bit number in hex format.
   * \level level of this node
   */
  LocalNode(string id, int level);

  ~LocalNode();

  /**
   * Number of nodes in the prefix table
   */

   int getPrefixLen();

   /**
    * Number of nodes in the suffix table.
    */
    int getSuffixLen();

  /**
   * Add a remote node to local cache.
   *
   * The appropriate location (e.g prefix or suffix) is
   * decided based on the Id of this node and that of remoteNode.
   * It is also possible that a particular node is not added to
   * any of the cache location.
   *
   * \param remoteNode Node to be added.
   * \return Value indicating the type of table where the node was added.
   *        PREFIX (1) , SUFFIX (2) , BACKUP (3) , RANDOM (4)
   */
  int addRemote(Node* remoteNode);
  /**
   * Remotes a node from the local cache.
   *
   * \param remoteNode Node to be removed.
   * \return True if the node is successfuly removed.
   */
  bool delRemote(Node* remoteNode);
  /**
   * Removes all the nodes from the cache.
   *
   * \\return true if the operation is successful.
   */
   bool delRemoteAll();
  /**
   * Flush the cache, removing all the entries
   *
   * \return True on successful operaiton
   */
  //bool delRemoteAll();
  /**
   * Searches the cache for the 'node' and returns a pointer to it.
   *
   * \param node Node to search. Comparision is based on the id of the node.
   * \param result This contains the pointer to the searched node.
   * \return True if node was found.
   */
  bool findId(Node& node, Node** result);

  /**
   * Top protocol of the node tree.
   *
   * \param source Node of which top node is requested.
   * \param topNode Will contains top node, if search is successful.
   * \param type Wheter the node is prefix, suffix..
   *
   * \return True if the local cache have topNode for the 'soruce'
   */
  bool getTopNode(Node& source, Node **topNode, int type);

   /**
    * Find all nodes with 'recipient' being in their prefix/suffix table.
    *
    * Use the flags below to modify the search attributes.
    *
    *     Specify kind of query and different types can be or-ed together.
    *
    *     HIGHER_LEVEL: Search for the node in nodes higher than the current node.
    *     EQUAL_LEVEL: Search for the node in nodes at equal level to the current node.
    *     LOWER_LEVEL: Search for the node at the lower level
    *     FIND_PREFIXES: Only restrict to prefix nodes.
    *     FIND_SUFFIXES: Only restrict to suffix nodes.
    *     ONE_LEVEL_LOWER: Search for the nodes at one level lower.
    *
    * \param recipient
    * \param result Set containing all matching nodes.
    * \param flags Query parameters
    * \return True if at-least one result was found.
    */
  bool searchRemoteLinks(Node& recipient, NodeSet& result,
       unsigned int flags=HIGHER_LEVELS|EQUAL_LEVELS|LOWER_LEVELS|
                          FIND_SUFFIXES|FIND_PREFIXES|ONE_LEVEL_LOWER);
  /**
   * Search all nodes in suffix table that must have recipient in their prefix tables.
   * Use the flags below to modify the search attributes.
   *
   *     Specify kind of query and different types can be or-ed together.
   *
   *     HIGHER_LEVEL: Search for the node in nodes higher than the current node.
   *     EQUAL_LEVEL: Search for the node in nodes at equal level to the current node.
   *     LOWER_LEVEL: Search for the node at the lower level
   *
   * \param recipient
   * \param result Set containing all matching nodes.
   * \param flags Query parameters
   * \return True if at-least one result was found.
   */
  bool searchRemoteLinksViaSuffix(Node& recipient, NodeSet& result,
       unsigned int flags=HIGHER_LEVELS|EQUAL_LEVELS|LOWER_LEVELS);

  /**
   * Node cache information method.
   * Let you calculate number of nodes in prefix or suffix table
   * at a given level. 
   */

  int getNodeCount(int level, unsigned int flags=PREFIX|SUFFIX); 

  /**
   * Node cache information method.
   * Returns all the nodes at given level of a specified table.
   */
   
  int getNodes(int level, NodeSet &resultNodes, unsigned int flags=PREFIX|SUFFIX);

protected:
  //Cache
  NodeSet prefixTable[MAX_NODE_LEVEL];
  IntNodeMap suffixTable[MAX_NODE_LEVEL];
  NodeSet randomTable[MAX_NODE_LEVEL];
  Node* backupTable[MAX_NODE_LEVEL];

  void initialize();

private:
  unsigned int prefixLen;
  unsigned int suffixLen;
  static const SynchronizedObject LOCK;

  int internalAddRemote(Node* remoteNode);
  bool internalDelRemote(Node* remoteNode);
};
}// namespace Tourist
#endif /*LOCALNODE_H_*/
