#ifndef CONSTANTS_H_INCLUDED
#define CONSTATNS_H_INCLUDED

/* This file is configured by the Tourist build system (CMAKE) */

typedef unsigned int elem_t;

/**
   Internal variable as used by the Id class to create the arry
   of the size ID_SIZE for holding UUID of the Tourist's Node
*/
#define ID_SIZE 4

#define SIZEOF_UNSIGNED_INT 4

#define MAX_LEVEL 32

#define PREFIX_BIT (INT_MAX+1)

#define MAX_NODE_LEVEL 3

#define MIN_KNOWN 4

//Node tables.

//Don't start these values from 0, 0 indicats no table.
#define PREFIX 1
#define SUFFIX 2
#define BACKUP 3
#define RANDOM 4

//Transport Constants
#define TCP 1
#define RUDP 2
#define TCP_PORT  3045
#define RUDP_PORT 4045
#define HTTP_PORT 8085


//Node query types
#define HIGHER_LEVELS 1
#define EQUAL_LEVELS 2
#define LOWER_LEVELS 4
#define FIND_ALL 8
#define FIND_PREFIXES 16
#define FIND_SUFFIXES 32
#define ONE_LEVEL_LOWER 64
#define FIND_RANDOM 64

//Event Types
#define EVENT_PREFIX_JOIN 1
#define EVENT_PREFIX_DISCONNECT 2
#define EVENT_PREFIX_LEVEL_UP 3
#define EVENT_PREFIX_LEVEL_DOWN 4
#define EVENT_SUFFIX_JOIN 5
#define EVENT_SUFFIX_DISCONNECT 6
#define EVENT_SUFFIX_LEVEL_UP 7
#define EVENT_SUFFIX_LEVEL_DOWN 8
#define TYPE_PREFIX 9
#define TYPE_SUFFIX 10

// Path to the configuration file containing different customization
// parameters for this node.
#define CONFIG_FILE_PATH "./node.properties"
// Specify path to the logging properties file
#define LOG_FILE_PATH "./logger.properties"

//Message Types
#define HESSIAN 1

#define NULL_ID "00000000000000000000000000000000"

#endif
