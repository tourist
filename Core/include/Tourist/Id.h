 // Id class
// (C) 2005 California Institute of Technology
// Author Conrad Steenberg <conrad@hep.caltech.edu>

#ifndef ID_H_
#define ID_H_

#include <cstdlib>
#include <iostream>
#include <string>
#include "Constants.h"

//#include "debug.h"

using namespace std;

// Architecture dependent definitions
namespace Tourist {

typedef unsigned int elem_t;

class Id
/// Id is a super class for the Tourist Node. It represent a 128 bit
/// Id. Different comparision methods are available as operator
/// (>, <, == , !=) for direct comparision of Tourist nodes.
///
/// E.x  TODO Put a id format here.
{
public:

  /**
   * Fills the id with a randomly generated value.
   *
   * In order to ensure a fairly random value for multiple
   * nodes on the same machine make sure to use srand before
   * making a call to this contructor.
   *
   *  Here is one example.
   *
   *     struct timeval tv;
   *     struct timezone tz;
   *     if (gettimeofday(&tv, &tz)==-1)
   *       srand ( time(NULL) );
   *     else
   *       srand (tv.tv_usec); // Should be more random than time()
   *
   *     Id id();
   */
  Id();

  /**
   * Fills the id with a given string id.
   *
   * In order to ensure a fairly random value for multiple
   * nodes on the same machine make sure to use srand before
   * making a call to this contructor.
   *
   *  Here is one example.
   *
   *     struct timeval tv;
   *     struct timezone tz;
   *     if (gettimeofday(&tv, &tz)==-1)
   *       srand ( time(NULL) );
   *     else
   *       srand (tv.tv_usec); // Should be more random than time()
   *
   *   \Exceptions InvalidIDInput
   *   \param  id String representation of the node.
   */

  Id(string id);

  /**
    * Fills the id with a given string id.
    *
    * In order to ensure a fairly random value for multiple
    * nodes on the same machine make sure to use srand before
    * making a call to this contructor.
    *
    *  Here is one example.
    *
    *     struct timeval tv;
    *     struct timezone tz;
    *     if (gettimeofday(&tv, &tz)==-1)
    *       srand ( time(NULL) );
    *     else
    *       srand (tv.tv_usec); // Should be more random than time()
    *
    *  \Exceptions InvalidIDInput
    *   \param  input_data
    */

  Id(elem_t input_data[]);

  /**
   * Fills this Id instance with a randomly generated 128 bit number.
   */
  void randomFill();

  /**
   * Fills this instance with all zero values. Useful for initialization.
   */
  void zeroFill();

  /**
   * Calculate the XOR distance between 2 Id instances
   *
   * \param op2 Id to be compared.
   * \return Id object with value as the XOR distance of this instance to 'op2'
   */
  Id operator -(Id op2);

  /**
   * Compares two IDs.
   *
   * \param op2 Id to be compared.
   * \return true if this Id instance is greater than 'op2'
   */
  bool operator >(const Id op2) const;

  /**
   * Compares two IDs.
   *
   * \param op2 Id to be compared.
   * \return true if this Id instance is less than 'op2'
   */
  bool operator <(const Id op2) const;

  /**
   * Compares two IDs.
   *
   * \param op2 Id to be compared.
   * \return true if this Id instance is equal 'op2'
   */
  bool operator ==(Id op2) const;

  /**
   * Compares two IDs.
   *
   * \param op2 Id to be compared.
   * \return true if this Id instance is not equal to 'op2'
   */
  bool operator !=(const Id op2) const;

  /**
   * Masks the higher order sizeof(int) bits
   */
  bool calculateMask(const int mask) const;

  /**
   * Compares two IDs.
   *
   * \param Rhs Id to be compared.
   * \return -1 if this > Rhs
   *          1 if this > Rhs
   *          0 if this == Rhs
   */
  int Compare(Id Rhs);

  /**
   * Get the prefix bitmap index
   */
  unsigned int prefixIndex(unsigned int bits) const;

  /**
   *  Get the suffix bitmap index.
   */
  unsigned int suffixIndex(unsigned int bits) const;

    /**
   * Set the value of this id instance to the one provided.
   *
   * If the id is not valid (like it doesn't contain all numer values)
   * the modified Id will be all zeros.
   *
   * \param String representation of the Id.
   */
  void setID(string id) const;

  /**
   * Get the string representation of 128-bit id.
   */
  string getID() const;
  /**
   * A string representation of Id.
   *
   * The different between hexstring() and printable() is that printable()
   * return a 128 bit id string in non-cotiguous chucks of hex numbers.
   *
   * \return string representation of 128 bit number in hex format.
   */
  string printable() const;

  /**
   * A string representation of Id.
   *
   * The different between hexstring() and printable() is that printable()
   * return a 128 bit id string in non-cotiguous chucks of hex numbers.
   *
   * \return string representation of 128 bit number in hex format.
   */
  string hexstring() const;

  /**
   * A string representation of Id.
   *
   * Bot hexstring() and printable() uses this method to get the id string.
   *
   * \return string representation of 128 bit number in hex format.
   */
  string printable_raw(const char *sep) const;

private:
  // Members
  elem_t data[ID_SIZE];

};

} //namespace Tourist

/*ostream& operator<<(ostream& os, Id id);
ostream& operator<<(ostream& os, Id *id);

int CompareId(Id op1, Id op2);

void print_line();
void print_line(ostream& os);
*/


#endif /*ID_H_*/
