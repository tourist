#ifndef ABSTRACTMESSAGE_H_
#define ABSTRACTMESSAGE_H_

#include <string>

using namespace std;
/**
 * Abstracts the message message payload to be exchanged.
 *
 * This class offers an interface for the upper applications to send
 * different kind of payload data like hessian, xml, etc.
 *
 * For concrete implementations look at Tourist::Message::HessianMessage
 *
 * Note that this doesn't contain the message envelop.
 *
 * \Author Faisal Khan
 */
 namespace Tourist {

class AbstractMessage
{
public:
  /**
   * Header of this message.
   *
   * Currently all the messages in the Tourist should have
   * a header name to let MessageDispatcher decide where to
   * deliver a particular message within a peer.
   *   
   */
  virtual string getComponentName() = 0;

  /**
   * Component from where this message was generated. 
   * Used to identify remote sender compnent for sending
   * back reply
   */

  virtual string getSourceCompName() = 0;

  /**
   * Identify the type of the message.
   * //TODO: A way to register new types and define existing message types. 
   * Current concrete implementation only have HESSIAN type.
   */
  virtual int getType() = 0;

  /**
   * Serialized form of this message.
   *
   *\return serialized output
   */
  virtual int getString(string &serialized) = 0;
  
  /**
   * This method is called to pass the payload data recieved from the
   * network layer to upper layers.
   * 
   * @param data Bytes received from wire
   * @param num Size of data
   */
  virtual int setData(char* data, int num) = 0;
  
  /**
   * Indicate if there was any problem specially during serialization and
   * deserialization of this message. Should check this property before 
   * processing the message.
   */
  virtual bool isInErrorState() = 0;
};

}//namespace Tourist

#endif /*ABSTRACTMESSAGE_H_*/
