/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestLocalNode.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include "Poco/RunnableAdapter.h"
#include "Poco/Thread.h"
#include "sys/time.h"

using Tourist::LocalNode;
using Tourist::Node;
using Poco::RunnableAdapter;
using Poco::Thread;

TestLocalNode::TestLocalNode(const std::string& name): CppUnit::TestCase(name)
{
}

TestLocalNode::~TestLocalNode()
{
}

void TestLocalNode::setUp()
{
  struct timeval tv;
  struct timezone tz;
  //To ensure that we get fairly random Id on
  // same host.
  if (gettimeofday(&tv, &tz)==-1)
    srand ( time(NULL) );
  else
    srand (tv.tv_usec); // Should be more random than time()
}

void TestLocalNode::tearDown()
{
}

void TestLocalNode::testAddNode()
{
  std::string testid1("0123456789abcdef0123456789abcdef");
  std::string testid2("fedcba9876543210fedcba9876543210");

  //Level 0 node
  LocalNode node(testid1, 0);
  Node node2(testid2, 0);

  assert(node.hexstring() == testid1);
  assert(node.addRemote(&node2) == 1);

  //Searching node cache with the same object.
  Node *search = 0;
  assert(node.findId(node2, &search) == true);
  assert(search != NULL);
  assert(search -> hexstring() == testid2);

  //Searching node cache with different object
  // but with same id
  /*Node *search2;
  assert(node.findId(newNode, &search2) == true);
  assert(
  assert(search2.hexstring() == testid2);*/
}

void TestLocalNode::testRemoveNode()
{
  std::string testid1;

  //Level 0 node
  LocalNode node(0);
  Node node2(0);

  assert(node.addRemote(&node2) == true);
  assert(node.delRemote(&node2) == true);

  Node *temp = 0;
  assert(node.findId(node2, &temp) == false);

  //Trying to delete non existing node.
  Node nonExisting;
  assert(node.delRemote(&nonExisting) == false);
}

void TestLocalNode::testSearchRemoteLinks()
{

  LocalNode node(0);
  Node nodeL0a(0), nodeL0b(0), nodeL0c(0), nodeL1a(1),
       nodeL1b(2), nodeL2a(2);

  node.addRemote(&nodeL0a);
  node.addRemote(&nodeL0b);
  node.addRemote(&nodeL0c);
  node.addRemote(&nodeL1a );
  node.addRemote(&nodeL1b);
  node.addRemote(&nodeL2a);

  Tourist::NodeSet result;
  //Testing all the nodes at the equal level.
  assert(node.searchRemoteLinks(nodeL2a, result, FIND_PREFIXES|EQUAL_LEVELS) == true);
  assert(result.treeSize == 1);

  Tourist::NodeSet::Iter iter(result);
  while(iter)
  {
    assert(iter->key -> hexstring() == nodeL0a.hexstring() ||
           iter->key -> hexstring() == nodeL0b.hexstring() ||
           iter->key -> hexstring() == nodeL0c.hexstring());
    iter++;
  }

  result = Tourist::NodeSet();
  //Looking for all prefix nodes at the same level to 'node'
  assert(node.searchRemoteLinks(nodeL2a, result, FIND_ALL|FIND_PREFIXES|EQUAL_LEVELS) == true);
  assert(result.treeSize == 3);
  iter = Tourist::NodeSet::Iter(result);
  while(iter)
  {
    assert(iter->key -> hexstring() == nodeL0a.hexstring() ||
           iter->key -> hexstring() == nodeL0b.hexstring() ||
           iter->key -> hexstring() == nodeL0c.hexstring());
    iter++;
  }

  //TODO: More test cases.
}

void TestLocalNode::testSearchRemoteLinksViaSuffix()
{
  LocalNode node("f000000000000000000000000000000f", 5);
  LocalNode nodeSl0a("0f0000000000000000000000000000ff", 0);
  LocalNode nodeSl0b("0f0000000000000000000000000000ff", 0);
  LocalNode nodeSl1a("0f0000000000000000000000000000ff", 1);

  node.addRemote(&nodeSl0a);
  node.addRemote(&nodeSl0b);
  node.addRemote(&nodeSl1a);

  assert(node.getSuffixLen() == 3);
}

void TestLocalNode::testThreadedReadWrite()
{
  assert(globalNode.getPrefixLen() == 0);
  assert(globalNode.getSuffixLen() == 0);

  int i = 0;
  const int threadCount = 10;
  Thread ths[threadCount];

  RunnableAdapter<TestLocalNode> ra(*this, &TestLocalNode::doWork);
  for (i=0; i<threadCount; i++)
  {
    ths[i].start(ra);
  }

  Thread::sleep(20);

  //lets wait for all threads to stop
  for (i=threadCount-1; i>=0; i--)
    ths[i].join();

  assert(globalNode.getPrefixLen() == 10);
  assert(globalNode.getSuffixLen() == 0);
}

void TestLocalNode::doWork()
{
  int i=0;
  for (i=0; i<10; i++)
  {
    assert(globalNode.addRemote(&transactionNodes[i]));
    Thread::sleep(100);
  }
}

CppUnit::Test* TestLocalNode::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestLocalNode");
  CppUnit_addTest(pSuite, TestLocalNode, testAddNode);
  CppUnit_addTest(pSuite, TestLocalNode, testRemoveNode);
  CppUnit_addTest(pSuite, TestLocalNode, testThreadedReadWrite);
  CppUnit_addTest(pSuite, TestLocalNode, testSearchRemoteLinks);
 // CppUnit_addTest(pSuite, TestLocalNode, testSearchRemoteLinksViaSuffix);

  return pSuite;
}
