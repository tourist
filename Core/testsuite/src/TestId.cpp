/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestId.h"
#include "Tourist/Id.h"
#include "Tourist/TouristExceptions.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"

using Tourist::Id;
using Tourist::InvalidArgumentException;

TestId::TestId(const std::string& name): CppUnit::TestCase(name)
{
}

TestId::~TestId()
{
}

void TestId::setUp()
{
}

void TestId::tearDown()
{
}

void TestId::testEmptyConstructor()
{
  Id id;
  assert(id.hexstring() != "");
  assert(id.hexstring().length() == 32);
}

void TestId::testStringConstructor()
{
  std::string testid("0123456789abcdef0123456789abcdef");
  Id id(testid);

  assert(id.hexstring().length() == 32);
  assert(id.hexstring() == testid);

  //Invalid length test.
  testid = "0123456789abcdef0123456789abcdefdd";
  try
  {
    Id id2(testid);
    fail("Must catch exception for invalid length");
  } catch (InvalidArgumentException)
  { }

  //Invalid element in id
  testid = "xxxxx456789abcdef0123456789abcde";
  try
  {
    Id id3(testid);
    fail("Must catch exception for invalid element in ID");
  } catch (InvalidArgumentException &e)
  {
  }
}

void TestId::testXOROperator()
{
  std::string id1("0123456789abcdef0123456789abcdef");
  std::string id2("ffffffffffffffffffffffffffffffff");
  std::string result("fedcba9876543210fedcba9876543210");

  Id diffId1(id1);
  Id diffId2(id2);

  Id diff = diffId1-diffId2;

  assert(diff.hexstring() == result);
}

CppUnit::Test* TestId::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestId");
  CppUnit_addTest(pSuite, TestId, testEmptyConstructor);
  CppUnit_addTest(pSuite, TestId, testStringConstructor);
  CppUnit_addTest(pSuite, TestId, testXOROperator);

  return pSuite;
}
