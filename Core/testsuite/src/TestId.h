#ifndef TESTID_H_
#define TESTID_H_

#include "CppUnit/TestCase.h"

class TestId: public CppUnit::TestCase
{
public:
  TestId(const std::string& name);
  ~TestId();

  void setUp();
  void tearDown();
  void testEmptyConstructor();
  void testStringConstructor();
  void testXOROperator();
  static CppUnit::Test* suite();
};

#endif /*TESTID_H_*/
