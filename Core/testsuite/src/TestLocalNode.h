#ifndef TESTTEST_H_INCLUDED
#define TESTTEST_H_INCLUDED

#include "CppUnit/TestCase.h"
#include "Tourist/LocalNode.h"
#include "Tourist/Node.h"

using Tourist::LocalNode;
using Tourist::Node;

class TestLocalNode: public CppUnit::TestCase
{
  public:
    TestLocalNode(const std::string& name);
    ~TestLocalNode();
    void testAddNode();
    void testRemoveNode();
    //To ensure thread safety of LocalNode data structure.
    void testThreadedReadWrite();
    ///Test the search method.
    void testSearchRemoteLinks();
    void testSearchRemoteLinksViaSuffix();
    void setUp();
    void tearDown();

    static CppUnit::Test* suite();

  protected:
    void doWork();

  private:
    LocalNode globalNode;
    Node transactionNodes[10];
};

#endif //TESTTEST_H_INCLUDED
