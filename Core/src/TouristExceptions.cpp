/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/TouristExceptions.h"
#include <typeinfo>

//TOP level exceptions
TOURIST_IMPLEMENT_EXCEPTION(ConfigurationException, Exception, "Configuration Exception")
TOURIST_IMPLEMENT_EXCEPTION(RuntimeException, Exception, "Runtime Exception")
TOURIST_IMPLEMENT_EXCEPTION(MessageException, Exception, "Message Exception")

TOURIST_IMPLEMENT_EXCEPTION(InvalidArgumentException, ConfigurationException, "Invalid Argument")
TOURIST_IMPLEMENT_EXCEPTION(InvalidConfigurationException, ConfigurationException, "Invalid Configuration Parameter")

TOURIST_IMPLEMENT_EXCEPTION(IndexOutOfBoundException, RuntimeException, "Index Out of Bound")
TOURIST_IMPLEMENT_EXCEPTION(NullPointerException, RuntimeException, "Null Pointer")

TOURIST_IMPLEMENT_EXCEPTION(NetworkException, RuntimeException, "Network Exception")
TOURIST_IMPLEMENT_EXCEPTION(AlreadyInitializedException, NetworkException, "Null Pointer")

TOURIST_IMPLEMENT_EXCEPTION(ComponentNotFoundException, MessageException, "Component Not Found Exception")
