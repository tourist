/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Id class
// (C) 2005 California Institute of Technology
// Author Conrad Steenberg <conrad@hep.caltech.edu>

//#include "common.h"
#include "Tourist/Id.h"
#include "Tourist/TouristExceptions.h"
#include <iostream>
#include <sstream>

using namespace std;

using Tourist::Id;
using Tourist::InvalidArgumentException;

namespace Tourist {

//PREFIX_BIT is define in < TODO: Add where it is define
// and what's its value.

static long prefix_masks[]={0,
     (long) PREFIX_BIT,             //1000000000.....
     (long) PREFIX_BIT/2*3,         //1100000000.....
     (long) PREFIX_BIT/4*7,         //1110000000.....
     (long) PREFIX_BIT/8*15,        //1111000000.....
     (long) PREFIX_BIT/16*31,       //1111100000.....
     (long) PREFIX_BIT/32*63,       //1111110000.....
     (long) PREFIX_BIT/64*127,      //1111111000.....
     (long) PREFIX_BIT/128*255,     //1111111100.....
     (long) PREFIX_BIT/256*511,     //1111111110.....
     (long) PREFIX_BIT/512*1023,    //11111111110....
     (long) PREFIX_BIT/1024*2047,
     (long) PREFIX_BIT/2048*4095,
     (long) PREFIX_BIT/4096*8191,
     (long) PREFIX_BIT/8192*16383,
     (long) PREFIX_BIT/16384*32767,
};

static elem_t suffix_masks[]={0,1, 3, 7, 15, 31, 63,
                 127, 255, 511, 1023,
                 2047, 4095, 8191, 16383
};

Id::Id()
{
  this -> randomFill();
}

Id::Id(string id)
{
  setID(id);
}

Id::Id(elem_t input_data[])
{
  int inputSize = sizeof(input_data)/sizeof(elem_t);
  if (inputSize < ID_SIZE || inputSize > ID_SIZE)
    throw InvalidArgumentException("Size mismatched for input array");

  for (int i=0; i<ID_SIZE;i++)
    this->data[i]=input_data[i];
}

Id Id::operator-(const Id op2)
{
  Id result;

  for (int i=0; i<ID_SIZE;i++)
    result.data[i]=this->data[i]^op2.data[i];

  return result;
}

bool Id::operator<(const Id op2) const
{
  for (int i=0; i<ID_SIZE;i++)
    if (this->data[i] < op2.data[i]) return true;
    else if (this->data[i] == op2.data[i]) continue;
    else break;

  return false;
}

bool Id::operator>(const Id op2) const
{
  for (int i=0; i<ID_SIZE;i++)
    if (this->data[i] > op2.data[i]) return true;
    else if (this->data[i] == op2.data[i]) continue;
    else break;

  return false;
}

bool Id::operator==(const Id op2) const
{
  if (!&op2) return false;
  for (int i=0; i<ID_SIZE;i++)
    if (this->data[i] != op2.data[i]) return false;

  return true;
}

bool Id::operator!=(const Id op2) const
{
  if (!&op2) return true;
  for (int i=0; i<ID_SIZE;i++)
    if (this->data[i] != op2.data[i]) return true;

  return false;
}

bool Id::calculateMask(const int mask) const
{
  return this->data[0] & mask;
}

int Id::Compare(const Id op2)
{
  for (int i=0; i<ID_SIZE;i++){
    if (this->data[i] < op2.data[i]) return -1;
    else if (this->data[i] > op2.data[i]) return 1;
  }
  return 0;
}

int CompareId(Id op1, const Id op2)
{
  return op1.Compare(op2);
}

unsigned int Id::suffixIndex(unsigned int bits) const
{
  return data[ID_SIZE-1]&suffix_masks[bits];
}

unsigned int Id::prefixIndex(unsigned int bits) const
{
  return data[0]&prefix_masks[bits];
}

void Id::randomFill()
{
  for (int i=0; i<ID_SIZE;i++)
  {
    this->data[i]=(elem_t)((rand()<<1) + rand());
  }
}

void Id::zeroFill()
{
  for (int i=0; i<ID_SIZE;i++)
      this->data[i]=0;
}

void Id::setID(string id) const
{
  int i;

  if (id.length()!=32)
    throw InvalidArgumentException("Length of the input should be 32");

  for (i=0; i<ID_SIZE; i++)
  {
    if (!sscanf(id.substr(i*(32/ID_SIZE),(32/ID_SIZE)).c_str(),"%x",&data[i]))
      throw InvalidArgumentException("Id should contain only hex digits");
  }
}


string Id::getID() const
{
  return hexstring();
}

string Id::printable() const
{
  return printable_raw(" ");
}

string Id::hexstring() const
{
  return printable_raw("");
}

string Id::printable_raw(const char *sep) const
{
  ostringstream outs;
  for (int i=0; i<ID_SIZE; i++)
  {
    outs.width(2);
    outs.width(SIZEOF_UNSIGNED_INT*2); // Two characters for every byte
    outs.fill('0');
    outs<<hex<<this->data[i]<<sep;
  }
  return outs.str();
}

/*ostream& operator<<(ostream& os, Id id){
  for (int i=0; i<ID_SIZE;i++){
    os.width(SIZEOF_UNSIGNED_INT*2); // Two characters for every byte
    os.fill('0');
    os<<hex<<id.data[i]<<" ";
  }

  return os;
}

ostream& operator<<(ostream& os, Id &id){
  for (int i=0; i<ID_SIZE;i++){
    os.width(SIZEOF_UNSIGNED_INT*2); // Two characters for every byte
    os.fill('0');
    os<<hex<<id.data[i]<<" ";
  }

  return os;
}

ostream& operator<<(ostream& os, Id *id){
  if (!id)
    os<<"NULL";
  else for (int i=0; i<ID_SIZE;i++){
    os.width(SIZEOF_UNSIGNED_INT*2); // Two characters for every byte
    os.fill('0');
    os<<hex<<id->data[i]<<" ";
  }

  return os;
}

void print_line(ostream& os){
  for (int i=0;i<78;i++)
    os<<"-";
  os<<endl;
}

void print_line(){
  print_line(cout);
}*/

} //namespace Tourist
