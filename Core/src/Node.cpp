/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Node.h"

namespace Tourist {

Node::Node(): Id()
{
  this->setLevel(0);
}

Node::Node(unsigned int level) : Id()
{
  this->setLevel(level);
  userDefinedName = "Un-Titled";
}

Node::Node(string id) : Id(id)
{
}

Node::Node(string id, unsigned level) : Id(id)
{
  this->setLevel(level);
}

void Node::setLevel(unsigned int new_level)
{
    this->level=new_level;
    this->prefix=prefixIndex(new_level);
    this->suffix=suffixIndex(new_level);
}

int Node::getLevel()
{
  return this->level;
}

string Node::getName()
{
  return userDefinedName;
}

void Node::setName(string n)
{
  this->userDefinedName = n;
}

unsigned int Node::getPrefixIndex()
{
  return this->prefix;
}

unsigned int Node::getSuffixIndex()
{
  return this->suffix;
}

} //namespace Tourist
