/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/LocalNode.h"

namespace Tourist {

const Poco::SynchronizedObject LocalNode::LOCK;

LocalNode::LocalNode() : Node()
{
  initialize();
}

LocalNode::LocalNode(string id) : Node(id)
{
  initialize();
}

LocalNode::LocalNode(int level) : Node(level)
{
  initialize();
}

LocalNode::LocalNode(string id, int level) : Node(id, level)
{
  initialize();
}

LocalNode::~LocalNode()
{
}

void LocalNode::initialize()
{
  prefixLen = suffixLen = 0;
}

int LocalNode::getPrefixLen()
{
  return this->prefixLen;
}

int LocalNode::getSuffixLen()
{
  return this->suffixLen;
}

int LocalNode::addRemote(Node* remoteNode)
{
  if (remoteNode == this) return false;

  if (! LocalNode::LOCK.tryLock())
  {
    try
    {
      LocalNode::LOCK.wait(1000);
      LocalNode::LOCK.lock();
    }
    catch (Poco::Exception &e)
    {
      std::cout<<e.what()<<std::endl;
      throw e;
    }
  }

  Node *existingNode = NULL;
  if (findId(*remoteNode, &existingNode))
    internalDelRemote(existingNode);

  int result = internalAddRemote(remoteNode);
  LocalNode::LOCK.unlock();
  LocalNode::LOCK.notify();

  return result;
}

int LocalNode::internalAddRemote(Node* remoteNode)
{
  int result = 0;

  unsigned int remotePrefix = remoteNode -> prefixIndex(getLevel());
  unsigned int remoteSuffix = remoteNode -> suffixIndex(getLevel());

  //Check if it belongs to prefix table
  if (remotePrefix == getPrefixIndex() ||
       (remoteNode -> getLevel() < getLevel() &&
          remoteNode -> getPrefixIndex() == prefixIndex(remoteNode -> getLevel())))
  {
    prefixTable[remoteNode -> getLevel()].insert(remoteNode);
    prefixLen++;
    result = PREFIX;
  }
  else if (remoteSuffix == getSuffixIndex())
  {
    AvliMapEl<unsigned int, NodeSet> *el;
    el = suffixTable[remoteNode -> getLevel()].find(remotePrefix);
    NodeSet ns;

    if (!el)
      suffixTable[remoteNode -> getLevel()].insert(remotePrefix, ns);
    else
     ns = el->value;

    ns.insert(remoteNode);
    this->suffixLen++;
    result = SUFFIX;
  }
  //bring this back
  /*else //backup table
  {
    unsigned int mask = INT_MAX;
    mask++;

    unsigned int mask2 = 0;
    for (unsigned int i=0; i<=getLevel(); i++)
    {
      if (!backupTable[i] && (calculateMask(mask) != remoteNode -> calculateMask(mask)) &&
         (calculateMask(mask2) != remoteNode -> calculateMask(mask2)))
      {
        backupTable[i] = remoteNode;
        result = BACKUP;
      }
    }
    mask2+=mask;
    Gmask>>=1;
  }*/

  //if not added any where.
  else if (result == 0)
  {
    randomTable[remoteNode -> getLevel()].insert(remoteNode);
    result = RANDOM;
  }

  return result;
}

bool LocalNode::delRemote(Node* remoteNode)
{
  if (this == remoteNode)
    return false;

  if (! LocalNode::LOCK.tryLock())
  {
    try
    {
      LocalNode::LOCK.wait(1000);
      LocalNode::LOCK.lock();
    }
    catch (Poco::Exception &e)
    {
      std::cout<<e.what()<<std::endl;
      throw e;
    }
  }

  bool result = internalDelRemote(remoteNode);
  LocalNode::LOCK.unlock();
  LocalNode::LOCK.notify();
  return result;
}

bool LocalNode::delRemoteAll()
{
  if (! LocalNode::LOCK.tryLock())
  {
    try
    {
      LocalNode::LOCK.wait(1000);
      LocalNode::LOCK.lock();
    }
    catch (Poco::Exception &e)
    {
      std::cout<<e.what()<<std::endl;
      throw e;
    }
  }

  int i;
  for (i=0; i<MAX_NODE_LEVEL; i++)
  {
    if (suffixTable[i].treeSize>0)
    {
      IntNodeMap::Iter iter(suffixTable[i]);
      while (iter)
      {
        NodeSet ns = iter.ptr->value;
        ns.empty();
        free(&ns);
        iter++;
      }
      suffixTable[i].empty();
      suffixLen = 0;
    }
  }

  //prefix table
  for (i=0; i<MAX_NODE_LEVEL; i++)
  {
    //TODO randomTable[i].empty
    prefixTable[i].empty();
    prefixLen = 0;
  }

  unsigned int mask=INT_MAX;
  mask++;
  for (i=1; i<this->getLevel(); i++)
  {
    if(this->backupTable[i])
      this->backupTable[i] = NULL;
  }

  return prefixLen==0&&suffixLen==0 ? true : false;
}

bool LocalNode::internalDelRemote(Node* remoteNode)
{
  bool result = false;
  int i=0;
  for (i=0; i<MAX_NODE_LEVEL; i++)
  {
    if (prefixTable[i].treeSize > 0 && prefixTable[i].find(remoteNode))
    {
      prefixTable[i].remove(remoteNode);
      this->prefixLen--;
      result = true;
      break;
    }
  }

  if (!result)
    for (i= 0; i<MAX_NODE_LEVEL; i++)
    {
      if (suffixTable[i].treeSize == 0) continue;
      IntNodeMap::Iter iter(suffixTable[i]);
      while (iter)
      {
        NodeSet tmpset = iter.ptr->value;
        AvliSetEl<Node*> *tmp = tmpset.find(remoteNode);
        if (tmp)
        {
          tmpset.remove(remoteNode);
          this->suffixLen--;
          result = true;
          break;
        }
      }
    } // for loop(i<MAX_NODE)..

  //TODO backup
  /*if (!result)
    for (i=0; i<MAX_NODE_LEVEL; i++)
    {
      if (random
    }*/

  unsigned int mask = INT_MAX;
  mask++;
  if (!result)
    for(i=1; i<getLevel(); i++)
    {
      if (this->backupTable[i] && this->backupTable[i] == remoteNode)
      {
        this->backupTable[i] = NULL;
        result = true;
      }
    }

  return result;
}

bool LocalNode::findId(Node &id, Node **result)
{
  int i=0;
  bool retVal = false;
  //Searching suffix table.
  for (i=0; i<MAX_NODE_LEVEL; i++)
  {
    if (suffixTable[i].length()>0)
    {
      IntNodeMap::Iter iter(suffixTable[i]);
      while (iter)
      {
        NodeSet set = iter->value;
        AvliSetEl<Node*>* el = set.find(&id);
        if (el)
        {
          *result = el->key;
          retVal = true;
          break;
        }

        iter++;
      }
    }
  }

  //Prefix
  if (!retVal)
    for (i=0; i<MAX_NODE_LEVEL; i++)
    { 
      AvliSetEl<Node*>* el = prefixTable[i].find(&id);
      if (el)
      {
        *result = el->key;
        retVal = true;
        break;
      }
    }

   //backup
   /*if (!retVal)
     for (i=1; i<getLevel(); i++)
       if (backupTable[i] && *backupTable[i] == id)
       {
          *result = backupTable[i];
          retVal = true;
          break;
       }*/

  return retVal;
}

bool LocalNode::getTopNode(Node& query, Node** result, int type)
{ 
  NodeSet rs;
  bool r = false;
  unsigned int flags = HIGHER_LEVELS | EQUAL_LEVELS;
  flags |= (type == PREFIX ? FIND_PREFIXES : FIND_SUFFIXES);
  
  r = searchRemoteLinks(query, rs, flags);

  if (r)
  {
    Tourist::NodeSet::Iter iter = rs.first();
    *result = (iter) ? iter -> key : NULL;
  }

  return r;
}

bool LocalNode::searchRemoteLinks(Node& query, NodeSet& result, unsigned int flags)
{
  bool retVal = false;
  unsigned int recipientPrefix, recipientSuffix, level;

  for (level=0; level<MAX_NODE_LEVEL; level++)
  {
    if (!(flags&HIGHER_LEVELS) && level < getLevel()) continue;
    if (!(flags&EQUAL_LEVELS) && level == getLevel()) continue;
    if ((flags&ONE_LEVEL_LOWER) && level > getLevel() + 1) break;
    if (!(flags&LOWER_LEVELS) && level > getLevel()) break;

    unsigned int calcLevel = (level<query.getLevel()?level:query.getLevel());
    recipientPrefix = query.prefixIndex(calcLevel);
    recipientSuffix = query.suffixIndex(calcLevel);

    if (flags&FIND_PREFIXES)
    {
      // Search all nodes in the prefix table with the same pre/suffix as the
      // query
      NodeSet::Iter iter(prefixTable[level]);
      while (iter)
      {
        //we shouldn't use the node with the same id as our query.
        if (*iter -> key == query)
	{
	  iter++;
	  continue; 
	}

        if(iter->key->prefixIndex(calcLevel) == recipientPrefix)
        {
          result.insert(iter->key);
          if (!(flags&FIND_ALL))
            return true;
        }
        iter++;
      }
    }

    if (flags&FIND_SUFFIXES)
    {
      //Slow linear search
      IntNodeMap::Iter nmiter(suffixTable[level]);
      while (nmiter)
      {
        NodeSet ns = nmiter->value;
        NodeSet::Iter iter(ns);
        while (iter)
        {

          //we shouldn't use the node with the same id as our query.
	  if (*iter -> key == query)
	  {
	    iter++;
	    continue;
	  }

          if (iter->key->suffixIndex(level) == recipientSuffix)
          {
            result.insert(iter->key);
            if(!(flags&FIND_ALL))
              return true;
          }
          iter++;
        }
        nmiter++;
      }
    }
    //if (flags&FIND_RANDOM); //TODO
  }
  //The backup table is little different
  for (unsigned int i=0;i<getLevel();i++)
  {

    //we shouldn't use the node with the same id as our query.
    if (*backupTable[i] == query)
      continue;

    if (backupTable[i])
    {
      level = backupTable[i]->getLevel();
      if (!(flags&HIGHER_LEVELS) && level < getLevel()) continue;
      if (!(flags&EQUAL_LEVELS) && level == getLevel()) continue;
      if ((flags&ONE_LEVEL_LOWER) && level > getLevel()+1) continue;
      if (!(flags&LOWER_LEVELS) && level > getLevel()) continue;
      result.insert(backupTable[i]);

      // Only need one level if FIND_ALL not set
      if (!(flags&FIND_ALL))
        return true;
    }
  }

  return (result.treeSize>0 ? true : false);
}// searchRemoteLinks

bool LocalNode::searchRemoteLinksViaSuffix(Node& recipient, NodeSet& result, unsigned int flags)
{
  AvliMapEl<unsigned int, NodeSet> *el;
  bool retVal = false;
  unsigned int recipientPrefix;
  for (unsigned int level=0; level<MAX_NODE_LEVEL; level++)
  {
    if (!(flags&HIGHER_LEVELS) && level < getLevel()) continue;
    if (!(flags&EQUAL_LEVELS) && level == getLevel()) continue;
    if (!(flags&LOWER_LEVELS) && level > getLevel()) continue;
    recipientPrefix = recipient.prefixIndex(level);

    el = suffixTable[level].find(recipientPrefix);
    if (el)
    {
      retVal = true;
      NodeSet ns = el->value;
      NodeSet::Iter iter(ns);
      while (iter)
      {
        //we shouldn't use the node with the same id as our query.
	if (*iter -> key == recipient)
	{
	  iter++;
	  continue;
	}

        result.insert(iter->key);
        iter++;
      }
    }
  }
  return retVal;
}//searchRemoteLinksViaSuffix

int LocalNode::getNodeCount(int level, unsigned int flag)
{
  int count = 0;
  if (level < 0 && level > MAX_NODE_LEVEL)
    count = -1;

  else if (flag&PREFIX)  
    count = prefixTable[level].treeSize;
  
  else
  {
    int tempCount = 0;
    //suffix table is slightly different as we need to extract NodeSet first
    //to calculate size
    IntNodeMap::Iter nmiter(suffixTable[level]);
    while (nmiter)
    {
      NodeSet ns = nmiter -> value;
      tempCount += ns.treeSize;
      nmiter++;
    }    
    count = tempCount;
  }

  return count;  
}

int LocalNode::getNodes(int level, NodeSet &result, unsigned int flags)
{
  int resultCount = 0;
  if (level < 0 && level > MAX_NODE_LEVEL)
    resultCount = -1;

  else if (flags&PREFIX)
  {
    NodeSet::Iter iter(prefixTable[level]);
    while (iter)
    {
      result.insert(iter->key);
      resultCount++;
      iter++;
    }     
  }

  else if (flags&SUFFIX)
  {
    IntNodeMap::Iter nmiter(suffixTable[level]);
    while (nmiter)
    {
      NodeSet ns = nmiter -> value;
      NodeSet::Iter iter(ns);
      while (iter)
      {
        result.insert(iter->key);
        resultCount++;
        iter++;
      }
      nmiter++;
    } 
  }

  else if (flags&RANDOM)
  {
    NodeSet::Iter iter(randomTable[level]);
    while (iter)
    {
      result.insert(iter -> key);
      resultCount++;
      iter++;
    }
  }

  return resultCount;  
}//getNodes

} //namespace Tourist
