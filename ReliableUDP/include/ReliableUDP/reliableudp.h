#ifndef RELIABLEUDP_H_
#define RELIABLEUDP_H_


// Forward declarations to pacify the compiler :)
namespace ReliableUDP
{
	class NetAddress;
	class Packet;
	class Host;
	class Peer;
	class ReliableUDPReactor;
	class ConnectionNotification;
	//class MessageNotification;
	class DisconnectNotification;
	class HostMonitor;
}

#include "constants.h"
#include <enet/enet.h>
#include "NetAddress.h"
#include "Packet.h"
#include "Host.h"
#include "Peer.h"
//#include "Notifications.h"
#include "ReliableUDPReactor.h"
#include "HostMonitor.h"

namespace ReliableUDP
{
	bool initialize();
}

#endif /*RELIABLEUDP_H_*/
