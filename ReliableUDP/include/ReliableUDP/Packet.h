#ifndef PACKET_H_
#define PACKET_H_

#include "reliableudp.h"
#include "Peer.h"

#include <string>

using std::string;

namespace ReliableUDP
{

	class Packet
	{
		private:
			string data;	///< Data contained by this packet
			bool reliable;	///< If true, receipt of the packet is acknowledged
			Peer *peer;		///< The peer that sent the packet

		public:
			Packet(string data, bool reliable, Peer *peer = NULL);
			Packet();
			string  getData();
			size_t getLength();
			bool isReliable();
			void setData(string data);
			void append(string data);
			Peer * getSource();
			virtual ~Packet();
	};

}

#endif /*PACKET_H_*/
