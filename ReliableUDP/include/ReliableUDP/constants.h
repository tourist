#ifndef CONSTANTS_
#define CONSTANTS_

#include <enet/enet.h>

namespace ReliableUDP
{
	typedef ::enet_uint8 SHORT;
	typedef ::enet_uint16 INT;
	typedef ::enet_uint32 LONG;
}
 
#endif /*CONSTANTS_*/
