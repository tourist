#ifndef MESSAGENOTIFICATION_H_
#define MESSAGENOTIFICATION_H_

#include "reliableudp.h"
#include <string>
#include <Poco/Notification.h>

using Poco::Notification;
using std::string;

namespace ReliableUDP
{

	class MessageNotification : public Notification
	{
		private:
			string _msg;
			Peer *_source;
			
		public:
			MessageNotification(string msg, Peer *source);
			string getMessage() const;
			Peer * getSource() const;
			virtual ~MessageNotification();
	};
	
	class ConnectionNotification : public Notification
	{
		private:
			Peer *_peer;
			
		public:
			ConnectionNotification(Peer *peer);
			Peer * getPeer() const;
			virtual ~ConnectionNotification();
	};
	
	class DisconnectNotification : public Notification
	{
		private:
			Peer *_peer;
			
		public:
			DisconnectNotification(Peer *peer);
			Peer * getPeer() const;
			virtual ~DisconnectNotification();
	};
}

#endif /*MESSAGENOTIFICATION_H_*/
