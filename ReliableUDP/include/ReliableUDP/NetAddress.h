#ifndef NETADDRESS_H_
#define NETADDRESS_H_

#include "reliableudp.h"
#include "constants.h"
#include <enet/enet.h>

namespace ReliableUDP
{
	class NetAddress
	{
		private:
			::ENetAddress address;	///< The underlying enetaddress represented by the object.

		public:
			NetAddress(const char *address, INT port);
			void setAddress(const char *address);
			const ::ENetAddress * getENetAddress() const;
			void setPort(INT port);
			char* getAddress() const;
			INT getPort() const;

			virtual ~NetAddress();
	};
}

#endif /*NETADDRESS_H_*/
