#ifndef HOSTMONITOR_H_
#define HOSTMONITOR_H_

#include <Poco/Thread.h>
#include <Poco/Task.h>
#include <Poco/NotificationCenter.h>
#include <ReliableUDP/reliableudp.h>

using Poco::Thread;
using Poco::Task;
using Poco::AbstractObserver;
using Poco::NotificationCenter;


namespace ReliableUDP
{
	class HostMonitor : public Task
	{
		private:
			Host *_host;
			int _timeout;
			NotificationCenter _nc;

		public:
			HostMonitor(Host *host);

			void setTimeout(LONG timeout);
			void runTask();
			void addObserver(const AbstractObserver &obs);
			void removeObserver(const AbstractObserver &obs);

			virtual ~HostMonitor();
	};

}

#endif /*HOSTMONITOR_H_*/
