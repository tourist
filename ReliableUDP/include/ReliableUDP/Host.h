#ifndef HOST_H_
#define HOST_H_

#include "reliableudp.h"
#include "Host.h"
#include "NetAddress.h"
#include "Peer.h"
#include "constants.h"
#include <enet/enet.h>

namespace ReliableUDP
{

	class Host
	{
		private:
			::ENetHost *host;	///< Points to the underlying enethost
		public:
			Host(NetAddress *address, size_t peerCount);

			::ENetHost * getHost();

			//Packet * receivePacket(INT);
			//Peer * peerConnected(INT);

			Peer * connect(const NetAddress &address, LONG timeout);
			virtual ~Host();
	};

}

#endif /*HOST_H_*/
