#ifndef RELIABLEUDPREACTOR_H_
#define RELIABLEUDPREACTOR_H_

#include "reliableudp.h"
#include <Poco/Mutex.h>
#include <Poco/TaskManager.h>
#include <Poco/AbstractObserver.h>
#include <Poco/Thread.h>

#include <string>
#include <map>

using Poco::Thread;
using Poco::Mutex;
using Poco::TaskManager;
using Poco::AbstractObserver;
using std::map;


namespace ReliableUDP
{

	class ReliableUDPReactor
		{
			private:
				ReliableUDPReactor();
				static ReliableUDPReactor *instance; ///< The single instance of this class.
				typedef map<Host *, HostMonitor *> EventHandlerMap;
				//Activity<ReliableUDPReactor> _activity;	///< The activity object to run the reactor in a separate thread
				//Host *_host;	///< The host the reactor is running on
				Mutex _mutex;	///< Required to make the library thread safe

				/*typedef void (*acceptConnectionFunc)(Peer *);	///< Type declaration of function pointer for connection handler.
				typedef void (*receivePacketFunc)(Packet *);	///< Type declaration of function pointer for packet handler.
				typedef void (*disconnectFunc)(Peer *);			///< Type declaration of function pointer for disconnect handler.

				vector<acceptConnectionFunc> acceptConnection;	///< Vector to hold register connection handlers.
				vector<receivePacketFunc> receivePacket;		///< Vector to hold registered packet handlers.
				vector<disconnectFunc> disconnect;				///< Vector to hold registered disconnect handlers.*/
				//NotificationCenter _nc;

				TaskManager _taskManager;
				EventHandlerMap _handlers;

			protected:
				/**
				   The method that runs the reactor in a separate thread.
				 */
				//void runActivity();

			public:

				/**
				   Get pointer to singleton instance.
				   \param	host	The host this reactor will run on
				   \return Pointer to instance of this object
				 */
				static ReliableUDPReactor * getInstance();

				/**
				   Register a new connection handler
				   \param acceptConnection	The callback for connection handling
				 */
				//void registerConnectionHandler(acceptConnectionFunc acceptConnection);

				/**
				   Register a new packet handler.
				   \param receivePacketFunc	The callback for packet handling
				 */
				//void registerPacketHandler(receivePacketFunc receivePacket);

				/**
				   Register a new disconnect handler.
				   \param	disconnectFunc	The callback for disconnect handling
				 */
				//void registerDisconnectHandler(disconnectFunc disconnect);

				/**
				   Remove connection handler.
				   \param acceptConnection	The callback to remove
				 */
				//void removeConnectionHandler(acceptConnectionFunc acceptConnection);

				/**
				   Remove packet handler.
				   \param receivePacket	The callback to remove
				 */
				//void removePacketHandler(receivePacketFunc receivePacket);

				/**
				   Remove disconnect handler.
				   \param	disconnect	The callback to remove
				 */
				//void removeDisconnectHandler(disconnectFunc disconnect);

				void addObserver(Host &host, const AbstractObserver &observer);
				void removeObserver(Host &host, const AbstractObserver &observer);
				/**
				   Start the main loop of the reactor that handles events.
				   \param timeout	The period for which the reactor will wait for an event to occur before quitting
				 */
				//void handle_events(LONG timeout);

				/**
				   Start the Reactor as a separate activity.
				 */
				void start();

				/**
				   Stop the reactor as a separate thread.
				 */
				void stop();

				virtual ~ReliableUDPReactor();
		};

}

#endif /*RELIABLEUDPREACTOR_H_*/
