#ifndef PEER_H_
#define PEER_H_

#include "reliableudp.h"
#include <string>

namespace ReliableUDP
{
  class Peer
  {
    private:
      Host *host;	///< The host the peer is connected to
      ::ENetPeer *peer;	///< The underlying enetpeer
      NetAddress *address;	///< The address of the peer
    public:
      Peer(Host *host, ::ENetPeer *peer);
      //Peer(Host *host, NetAddress *address);
      std::string getHostname() const;
      int getPort() const;
      //bool connect(LONG);
      bool disconnect(LONG timeout);
      bool send(Packet *packet);
      bool send(Packet packets[], size_t arraySize);
      void reset();
      virtual ~Peer();
  };

}

#endif /*PEER_H_*/
