#include <ReliableUDP/Host.h>

namespace ReliableUDP
{
	/** Create a new host.
		\param	address	Specify the address at which the host should listen for connections
	    \param	peerCount	The number of peers the host can be connected to
	 */
	Host::Host(NetAddress *address, size_t peerCount)
	{
		host = ::enet_host_create( (address == NULL) ? NULL:address -> getENetAddress(), peerCount, 0, 0 );

		if (host == NULL)
			throw "Unable to create host";
	}

	/* Check if any packets have arrived
	 * @param	timeout	specify how long the host will wait to see if any packets arrive
	 * @return	the packet that was received
	 */
	/*Packet * Host::receivePacket(INT timeout)
	{
		::ENetEvent event;
		Packet *packet = NULL;

		while (::enet_host_service(host, &event, timeout) > 0)
		{
			if (event.type == ::ENET_EVENT_TYPE_RECEIVE)
			{
				char *hostname = new char[20];
				::enet_address_get_host(&(event.peer -> address), hostname, 20);
				Peer *peer = new Peer(NULL, hostname, event.peer -> address.port);
				packet  = new Packet(new std::string((const char *)event.packet -> data), false, peer);
				::enet_packet_destroy(event.packet);
			}
		}

		return packet;
	}*/

	/* Check if any peers have connected
	 * @param	timeout	wait for timeout seconds for peers to connect
	 * @return	the peer that just connected
	 */
	/*Peer * Host::peerConnected(INT timeout)
	{
		::ENetEvent event;
		Peer *peer = NULL;

		while (::enet_host_service(host, &event, timeout) > 0)
		{
			if (event.type == ::ENET_EVENT_TYPE_CONNECT)
			{
				char *hostname = new char[20];
				::enet_address_get_host(&event.peer -> address, hostname, 20);
				peer = new Peer(this, (const char *)hostname, (INT)event.peer -> address.port);
			}
		}

		return peer;
	}*/

	/** Get the underlying ENetHost structure. For internal use only.
	    \return Pointer to the underlying enethost
	 */

	Peer * Host::connect(const NetAddress &address, LONG timeout)
	{
		bool connected = false;
		Peer *newPeer = NULL;
		ENetEvent event;
		::ENetPeer *peer = ::enet_host_connect(host, address.getENetAddress(), 2);

		while (::enet_host_service(host, &event, timeout) > 0)
		{
			connected = (event.type == ::ENET_EVENT_TYPE_CONNECT);
		}

		if (!connected)
		{
			peer = NULL;
		}
		else
		{
			char *hostname = new char[20];
			::enet_address_get_host( &peer -> address, hostname, 20);

			newPeer = new Peer(this, peer);
		}

		return newPeer;
	}

	::ENetHost * Host::getHost()
	{
		return host;
	}

	Host::~Host()
	{
		if (host != NULL)
		{
			::enet_host_destroy(host);
			host = NULL;
		}
	}

}
