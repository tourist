#include <ReliableUDP/ReliableUDPReactor.h>

#include <iostream>

using namespace std;

namespace ReliableUDP
{
	ReliableUDPReactor::ReliableUDPReactor()
	{
	}
	
	ReliableUDPReactor * ReliableUDPReactor::getInstance()
	{
		if (instance == NULL)
			instance = new ReliableUDPReactor();
		
		return instance;
	}
	
	/*void ReliableUDPReactor::registerConnectionHandler(acceptConnectionFunc acceptConnection)
	{
		Mutex::ScopedLock lock(_mutex);
		this -> acceptConnection.push_back(acceptConnection);
	}
	void ReliableUDPReactor::registerPacketHandler(receivePacketFunc receivePacket)
	{
		Mutex::ScopedLock lock(_mutex);
		this -> receivePacket.push_back(receivePacket);
	}
	
	void ReliableUDPReactor::registerDisconnectHandler(disconnectFunc disconnect)
	{
		Mutex::ScopedLock lock(_mutex);
		this -> disconnect.push_back(disconnect);
	}
	
	void ReliableUDPReactor::removeConnectionHandler(acceptConnectionFunc acceptConnection)
	{
		Mutex::ScopedLock lock(_mutex);
		
		vector<acceptConnectionFunc>::iterator it;
		
		it = this -> acceptConnection.begin();
		while ( it != this -> acceptConnection.end() )
		{
			if ( acceptConnection == *it)
			{
				this -> acceptConnection.erase(it);
			}
			
			it++;
		}
	}
	
	void ReliableUDPReactor::removeDisconnectHandler(disconnectFunc disconnect)
	{
		Mutex::ScopedLock lock(_mutex);
		
		vector<disconnectFunc>::iterator it;
		
		it = this -> disconnect.begin();
		while ( it != this -> disconnect.end() )
		{
			if ( *it == disconnect )
				this -> disconnect.erase(it);
			
			it++;
		}
	}
	
	void ReliableUDPReactor::removePacketHandler(receivePacketFunc receivePacket)
	{
		Mutex::ScopedLock lock(_mutex);
		
		vector<receivePacketFunc>::iterator it;
		
		it = this -> receivePacket.begin();
		while ( it != this -> receivePacket.end() )
		{
			if ( *it == receivePacket )
				this -> receivePacket.erase(it);
				
			it++;
		}
	}*/
	
	void ReliableUDPReactor::addObserver(Host &host, const AbstractObserver &observer)
	{
		Mutex::ScopedLock lock(_mutex);
		
		EventHandlerMap::iterator it( _handlers.find(&host) );
		
		if ( it == _handlers.end() )
		{
			HostMonitor *hm = new HostMonitor(&host);
			_handlers[&host] = hm;
			hm->addObserver(observer);
			
			if ( _taskManager.taskList().front()->state() == Task::TASK_RUNNING )
			{
				_taskManager.start(hm);
			}
		}
		else
		{
			it->second->addObserver(observer);
		}
	}
	
	void ReliableUDPReactor::removeObserver(Host &host, const AbstractObserver &observer)
	{
		Mutex::ScopedLock lock(_mutex);
		
		HostMonitor *hm = _handlers.find(&host)->second;
		hm->removeObserver(observer);
	}
	
	/*void ReliableUDPReactor::handle_events(LONG timeout)
	{
		::ENetEvent event;
		while (::enet_host_service(_host -> getHost(), &event, timeout) > 0)
		{
			Peer *peer;
			
			Packet *packet;
			
			switch(event.type)
			{				
				case ::ENET_EVENT_TYPE_CONNECT:					
					peer = new Peer(_host, event.peer);
					
					{
						ConnectionNotification notification(peer);
						_nc.postNotification(&notification);
					}
					
					peer = NULL;
					break;
					
				case ::ENET_EVENT_TYPE_RECEIVE:
					//char *hostname = new char[20];
					
					cerr << "Packet received.\n";
					
					peer = new Peer(_host, event.peer);
					packet  = new Packet( (const char*)event.packet -> data, false, peer);
					::enet_packet_destroy(event.packet);
					
					{
						MessageNotification notification(packet -> getData(), peer);
						_nc.postNotification(&notification);
					}
					
					peer = NULL;
					packet = NULL; 
					break;
					
				case ::ENET_EVENT_TYPE_DISCONNECT:
					//char *hostname = new char[20];
										
				    peer = new Peer(_host, event.peer);
					
					{
						DisconnectNotification notification(peer);
						_nc.postNotification(&notification);
					}
					
					peer = NULL;
					break;
			}		
		}
	}*/
	
	void ReliableUDPReactor::start()
	{
		Mutex::ScopedLock lock(_mutex);
		
		EventHandlerMap::iterator it;
		it = _handlers.begin();
		
		while ( it != _handlers.end() )
		{
			_taskManager.start( it->second );
			it++;
		}
	}
	
	void ReliableUDPReactor::stop()
	{
		Mutex::ScopedLock lock(_mutex);
		
		_taskManager.cancelAll();
	}
	
	ReliableUDPReactor::~ReliableUDPReactor()
	{
	}
	
	ReliableUDPReactor *ReliableUDPReactor::instance = NULL;
}
