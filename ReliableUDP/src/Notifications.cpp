#include <ReliableUDP/Notifications.h>

namespace ReliableUDP
{

	MessageNotification::MessageNotification(string msg, Peer *source) : _msg(msg), _source(source)
	{
	}
	
	string MessageNotification::getMessage() const
	{
		return _msg;
	}
	
	Peer * MessageNotification::getSource() const
	{
		return _source;
	}
	
	MessageNotification::~MessageNotification()
	{
	}
	
	ConnectionNotification::ConnectionNotification(Peer *peer) : _peer(peer)
	{
	}
	
	Peer * ConnectionNotification::getPeer() const
	{
		return _peer;
	}
	
	ConnectionNotification::~ConnectionNotification()
	{
	}
	
	DisconnectNotification::DisconnectNotification(Peer *peer) : _peer(peer)
	{
	}
	
	
	Peer * DisconnectNotification::getPeer() const
	{
		return _peer;
	}
	
	DisconnectNotification::~DisconnectNotification()
	{
	}

}
