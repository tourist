#include <ReliableUDP/NetAddress.h>
#include <enet/enet.h>
#include <iostream>

namespace ReliableUDP
{	
	/** Create a new address.
	 	\param address The IP address of the form "xxx.xxx.xxx.xxx"
	 	\param port The port number
	 */
	NetAddress::NetAddress(const char *address, INT port)
	{
		::enet_address_set_host( &(this -> address), address);
		this -> address.port = port;
	}
	
	/** Get underlying ENetAddress. For internal use only.
	 	\return Pointer to the underlying enetaddress
	 */
	const ::ENetAddress * NetAddress::getENetAddress() const
	{
		return &address;
	}
	
	/** Set the ip.
	 	\param address The IP the address should represent.
	 */
	void NetAddress::setAddress(const char *address)
	{
		::enet_address_set_host( &(this -> address), address);
	}
	
	/** Set the port.
	 	\param port The port number
	 */
	void NetAddress::setPort(INT port)
	{
		this -> address.port = port;
	}
	
	/** Get the IP.
	 	\return Pointer to the IP represented by this object.
	 */
	char* NetAddress::getAddress() const
	{
		char *address = new char[20];
		if (::enet_address_get_host( &(this -> address), address, 20) != 0)
			throw "Cannot create NetAddress";
		return address;
	}
	
	/**	Get the port.
	 	\return The port number
	 */
	INT NetAddress::getPort() const
	{
		return address.port;
	}
	NetAddress::~NetAddress()
	{
	}
}
