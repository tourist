#include "ReliableUDP/reliableudp.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

namespace ReliableUDP
{

	bool initialize()
	{
		if (::enet_initialize() < 0)
		{
			cerr << "Error! Unable to initialize the library.\n";
			return false;
		}
		atexit(::enet_deinitialize);

		return true;
	}
}
