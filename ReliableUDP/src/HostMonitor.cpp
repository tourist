#include "ReliableUDP/HostMonitor.h"
#include "ReliableUDP/Notifications.h"
#include <iostream>

using std::cerr;

namespace ReliableUDP
{

	HostMonitor::HostMonitor(Host *host) : _host(host), _timeout(1000), Task("")
	{
	}

	void HostMonitor::setTimeout(LONG timeout)
	{
		_timeout = timeout;
	}

	void HostMonitor::runTask()
	{
		while ( !isCancelled() )
		{
			::ENetEvent event;
			while (::enet_host_service(_host -> getHost(), &event, _timeout) > 0)
			{
				Peer *peer;

				Packet *packet;

				switch(event.type)
				{
					case ::ENET_EVENT_TYPE_CONNECT:
						peer = new Peer(_host, event.peer);

						{
							ConnectionNotification notification(peer);
							_nc.postNotification(&notification);
						}

						peer = NULL;
						break;

					case ::ENET_EVENT_TYPE_RECEIVE:
						//char *hostname = new char[20];

						cerr << "Packet received.\n";

						peer = new Peer(_host, event.peer);
						packet  = new Packet( (const char*)event.packet -> data, false, peer);
						::enet_packet_destroy(event.packet);

						{
							MessageNotification notification(packet -> getData(), peer);
							_nc.postNotification(&notification);
						}

						peer = NULL;
						packet = NULL;
						break;

					case ::ENET_EVENT_TYPE_DISCONNECT:
						//char *hostname = new char[20];

					    peer = new Peer(_host, event.peer);

						{
							DisconnectNotification notification(peer);
							_nc.postNotification(&notification);
						}

						peer = NULL;
						break;
				}
			}
			Thread::sleep(500);
		}
	}

	void HostMonitor::addObserver(const AbstractObserver &obs)
	{
		_nc.addObserver(obs);
	}

	void HostMonitor::removeObserver(const AbstractObserver &obs)
	{
		_nc.removeObserver(obs);
	}

	HostMonitor::~HostMonitor()
	{
	}

}
