#include <ReliableUDP/Packet.h>
#include <string>

namespace ReliableUDP
{

	/// Create a packet that doesn't contain anything. The packet must be initialized before it can be used.
		Packet::Packet()
	{
		reliable = false;
	}
	
	/** Create a packet with the specified data.
		\param data The data that this packet contains
		\param reliable Specifies whether the packet should be flagged as a reliable packet. If true, packet delivery is acknowledged and guaranteed
		\param peer The peer is not NULL if, and only if the packet was received. In that case it points to the peer that sent the packet 
	*/
	Packet::Packet(string data, bool reliable, Peer *peer)
	{
		this -> data = data;
		this -> reliable = reliable;
		this -> peer = peer;
	}
	
	/** Get data contained in the packet.
	    \return Pointer the data contained by the packet
	 */
	string Packet::getData()
	{
		return data;
	}
	
	/** Get the peer that sent the packet.
	 	\return	The peer that sent the packet. NULL if packet is being sent.
	 */
	Peer * Packet::getSource()
	{
		return peer;
	}
	
	/**
	   Get the length of the packet
	 */
	size_t Packet::getLength()
	{
		return data.length();
	}
	
	/**
	   Check if the receipt of the packet is supposed to be acknowledged. Internal use only.
	 */
	bool Packet::isReliable()
	{
		return reliable;
	}
	
	/**
	   Specify new data for the packet.
	   
	   \param data Pointer to the data
	 */
	void Packet::setData(string data)
	{
		this -> data = data;
	}
	
	/**
	   Append data to an existing packet.
	   
	   \param data Pointer to data to append
	 */
	void Packet::append(string data)
	{
		this -> data += data;
	}
	
	/**
	   Destroy the packet.
	 */
	Packet::~Packet()
	{
		if (peer != NULL)
		{
			delete peer;
			peer = NULL;
		}
	}

}
