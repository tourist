#include <ReliableUDP/Peer.h>

namespace ReliableUDP
{

	/* Create a new peer.
	 * @param host    The host that will connect to the peer
	 * @param address The address of the peer
	 */	
	/*Peer::Peer(Host *host, NetAddress *address)
	{
		this -> host = host;
		peer = NULL;
		this -> address = address;
	}*/
	
	/* Create a new peer
	 * @param	host	The host that will connect to the peer
	 * @param	address	The address of the host
	 * @param	port	The port to which the host must connect
	 */ 
	Peer::Peer(Host *host, ::ENetPeer *peer)
	{
		this -> host = host;
		this -> peer = peer;
		
		char *hostname = new char[20];
		::enet_address_get_host(&(peer -> address), hostname, 20);
		this -> address = new NetAddress(hostname, peer -> address.port);
	}
	
	/* Connect to the peer
	 * @param	timeout	The host will try to connect to the peer for the specified number of seconds.
	 * @return	If connection is successful, returns true. If timeout expired, returns false;
	 */
	/*bool Peer::connect(LONG timeout)
	{
		bool connected = false;
		ENetEvent event;
		peer = ::enet_host_connect(host -> getHost(), address -> getENetAddress(), 2);
		
		while (::enet_host_service(host -> getHost(), &event, timeout) > 0)
		{
			connected = (event.type == ::ENET_EVENT_TYPE_CONNECT);
		}
		
		if (!connected)
		{
			peer = NULL;
		}
		
		return connected;
	}*/
	
	/* Disconnect from peer
	 * @param	timeout	Waits for acknowledgment from the peer for timeout seconds
	 * @return	Always returns true
	 */	
	bool Peer::disconnect(LONG timeout)
	{
		bool disconnected = false;
		
		if (peer == NULL)
			return peer;
			
		::enet_peer_disconnect(peer, timeout);
		ENetEvent event;
		
		while (::enet_host_service(host -> getHost(), &event, timeout) > 0)
		{
			if (event.type == ::ENET_EVENT_TYPE_DISCONNECT)
			{
				disconnected = true;
				peer = NULL;
			}
		}
		
		if (!disconnected)
		{
			::enet_peer_reset(peer);
			peer = NULL;
			disconnected = true;
		}			
		
		return disconnected;
	} 
	
	/* Send packet to the peer
	 * @param	packet	The packet to be sent
	 * @return	If packet was sent successfully, returns true, else returns false
	 */
	bool Peer::send(Packet *packet)
	{
		::ENetPacket *epacket = ::enet_packet_create(packet -> getData().c_str(), packet -> getLength(), ( (packet -> isReliable()) ? ::ENET_PACKET_FLAG_RELIABLE:0) );
		
		if (epacket == NULL)
			return false;
			
		::enet_peer_send(peer, 0, epacket);
		::enet_host_flush(host -> getHost());
		
		return true;
	}
	
	/* Send multilple packets to the peer
	 * @param	packet	array of packets
	 * @param	length	the number of packets to send
	 * @return	true if sending successful
	 */
	bool Peer::send(Packet packet[], size_t length)
	{
		for (int i = 0; i < length; i++)
		{
			::ENetPacket *epacket = ::enet_packet_create(packet[i].getData().c_str(), packet[i].getLength(), ( (packet[i].isReliable()) ? ::ENET_PACKET_FLAG_RELIABLE:0) );
		
			if (epacket == NULL)
				return false;
				
			::enet_peer_send(peer, 0, epacket);
		}
		
		::enet_host_flush(host -> getHost());
		
		return true;
	}
	
	// Forcefully reset the peer
	void Peer::reset()
	{
		::enet_peer_reset(peer);
	}
	
	// Get the address of the peer
	std::string Peer::getHostname() const
	{
		return address->getAddress();
	}
	
	int Peer::getPort() const
	{
		return address->getPort();
	}
	
	Peer::~Peer()
	{
		if (peer != NULL)
		{
			delete peer;
			peer = NULL;
		}
		
		if (host != NULL)
		{
			host = NULL;
			delete host;
		}
		address = NULL;
	}

}
