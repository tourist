/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "Tourist/Util/Config.h"
#include "Tourist/Util/Util.h"
#include "Tourist/Constants.h"
#include "Poco/StringTokenizer.h"
#include "Tourist/TouristExceptions.h"

using Tourist::IndexOutOfBoundException;
using Tourist::InvalidConfigurationException;
using Poco::StringTokenizer;
using Poco::Exception;

namespace Tourist {
namespace Util {

//Config* Config::_instance = 0;

Config::Config(const string logfile, const string configfile)
{
  //load the logging proeprties
  try
  {
    pConfig = new PropertyFileConfiguration(configfile);
    lConfig = new PropertyFileConfiguration(logfile);
    logConfig.configure(lConfig);
    load();
  }
  catch (Poco::Exception &e)
  {
    std::cerr<<"Could't configure tourist!"<<e.what()<<std::endl;
    std::cerr<<"     Make sure that file "<<CONFIG_FILE_PATH<<" is present and"<<
                         " properly configured"<<endl;
    std::cerr<<"     Also, check out the presence of "<<LOG_FILE_PATH<<std::endl;
  }
  
  this -> typeData = NULL;
  this -> messageBus = NULL;
}

Config::Config(const Config& conf)
{
  *this = conf;
}

Config& Config::operator=(const Config& conf)
{
  this -> pConfig = conf.pConfig;
  this -> lConfig = conf.lConfig;    
  this -> bootNodes = conf.bootNodes;
  this -> remoteLogServer = conf.remoteLogServer;
  this -> nodeName = conf.nodeName;
  this -> isTCP = conf.isTCP;
  this -> isRUDP = conf.isRUDP;
  this -> isNetworkLog = conf.isNetworkLog;
  this -> isApmon = conf.isApmon;
  this -> tcpPort = conf.tcpPort;
  this -> rudpPort = conf.rudpPort;
  this -> bootRole = conf.bootRole;
  this -> bwThreshold = conf.bwThreshold;
} 

Config::~Config()
{
}

//Removed the singleton initialization.

/*Config* Config::getInstance()
{
  if (_instance == 0)
    _instance = new Config();

  return _instance;
}*/

void Config::load()
{
 //Configuring Boot nodes.
  Logger &logger = Logger::get("Tourist.Util.Config");

  string bootservers = pConfig->getString("tourist.boot.node");
  debug(logger, "tourist.boot.node = " + bootservers);

  StringTokenizer st(bootservers, ",");
  if (st.count() == 0)
    throw InvalidConfigurationException("No entry found for boot node");

  for (int i=0; i<st.count(); i++)
  {
    StringTokenizer forport(st[i], ":");
    if (forport.count() != 2)
    {
      debug(logger, "Port missing for " + bootservers);
      continue;
    }

    SocketAddress addr(forport[0], forport[1]);
    bootNodes.push_back(addr);
  }

  if (bootNodes.size() == 0)
  {
    throw InvalidConfigurationException("No entry found for boot node");
  }

  bootRole = pConfig->getInt("tourist.boot.role", 1);
  debug(logger, "tourist.boot.role = " + toString(bootRole));

  //Node name
  nodeName = pConfig->getString("tourist.name", "");
  debug(logger, "tourist.name = " + nodeName);

  //transport - TCP
  isTCP = pConfig->getBool("tourist.transport.tcp", true);
  string temp = isTCP ? "1" : "0";
  debug(logger, "tourist.transport.tcp = " + temp);
  tcpPort = pConfig->getInt("tourist.transport.tcp.port", 2101);
  debug(logger, "tourist.transport.tcp.port = " + tcpPort);

  //transport - RUDP
  isRUDP = pConfig->getBool("tourist.transport.rudp", true);
  temp = isRUDP ? "1" : "0";
  debug(logger, "tourist.transport.rudp = " + temp);
  rudpPort = pConfig->getInt("tourist.transport.rudp.port", 2101);
  debug(logger, "tourist.transport.rudp.port = " + rudpPort);

  //network
  bwThreshold = (long) pConfig->getDouble("tourist.network.bwThreshold", 500);
  debug(logger, "tourist.network.bwThreshold = " + toString(bwThreshold));

  //
  isNetworkLog = pConfig->getBool("tourist.remotelog.isEnable", true);
  temp = isTCP ? "1" : "0";
  debug(logger, "tourist.remotelog.isEnable = " + temp);

  string networkLogServer = pConfig->getString("tourist.remotelog.server", "localhost:1234");
  debug(logger, "tourist.remotelog.server" + networkLogServer);

  StringTokenizer ntk(networkLogServer, ":");
  if ( ntk.count() != 2)
  {
    string emsg = "Port missing for remote log server";
    error(logger, emsg);
    throw InvalidConfigurationException(emsg);
  }

  remoteLogServer = SocketAddress(ntk[0], ntk[1]);

  isApmon = pConfig->getBool("tourist.apmon.isEnable", false);
  temp = isApmon ? "1" : "0";
  debug(logger, "tourist.apmon.isEnable = " + temp);
}

string Config::getNodeName()
{
  return nodeName;
}

void Config::setNodeName(string name)
{
  nodeName = name;
}

SocketAddress Config::getBootAddress(int index)
{
  if (index <0 || index >= bootNodes.size())
    throw IndexOutOfBoundException("Index not valid");

  return bootNodes[index];
}

vector<SocketAddress> Config::getBootAddresses()
{
  return bootNodes;
}

SocketAddress Config::getRemoteLogServer()
{
  return remoteLogServer;
}

int Config::getBootRole()
{
  return bootRole;
}

void Config::setBootRole(int role)
{
  bootRole = role;
}

int Config::getTCPPort()
{
  return tcpPort;
}

int Config::getRUDPPort()
{
  return rudpPort;
}

void Config::setTCPPort(int port)
{
  tcpPort = port;
}

void Config::setRUDPPort(int port)
{
  rudpPort = port;
}

int Config::getBWThreshold()
{
  return bwThreshold;
}

void Config::setBWThreshold(int th)
{
  bwThreshold = th;
}

bool Config::getTCPEnable()
{
  return isTCP;
}

bool Config::getRUDPEnable()
{
  return isRUDP;
}

void Config::setTCPEnable(bool isEnable)
{
  isTCP = isEnable;
}

void Config::setRUDPEnable(bool isEnable)
{
  isRUDP = isEnable;
}

bool Config::getApMonEnable()
{
  return isApmon;
}

void Config::setApMonEnable(bool isEnable)
{
  isApmon = isEnable;
}

void Config::setMessageTypeHandler(Tourist::Message::MessageTypeData *data)
{
  this -> typeData = data;
}
 
Tourist::Message::MessageTypeData* Config::getMessageTypeHandler()
{
  return this -> typeData;
}
 
void Config::setMessageBus(Tourist::Message::MessageDispatcher *msgBus)
{
  this -> messageBus = msgBus;
}
 
Tourist::Message::MessageDispatcher* Config::getMessageBus()
{
  return this -> messageBus;
}

Tourist::LocalNode* Config::getLocalNode()
{
  return this->localNode;
}

void Config::setLocalNode(Tourist::LocalNode* ln)
{
  this -> localNode = ln;
}
 
} //namespace Util
} //namespace Config
