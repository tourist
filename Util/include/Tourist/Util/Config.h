#ifndef CONFIG_H_
#define CONFIG_H_


#include "Poco/AutoPtr.h"
#include "Poco/FileChannel.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Util/PropertyFileConfiguration.h"
#include "Poco/Util/LoggingConfigurator.h"
#include "Tourist/Constants.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;

using Poco::AutoPtr;
using Poco::Net::SocketAddress;
using Poco::Util::PropertyFileConfiguration;
using Poco::Util::LoggingConfigurator;

//forward declaration

namespace Tourist {
  class LocalNode;
}

namespace Tourist {
namespace Message {
  class MessageTypeData;
  class MessageDispatcher;
}
}

/**
 * Provides access to configuration parameters of Tourist.
 * These parameters are configured via 'node.properties' files.
 *
 * \Author Faisal Khan
 */
 namespace Tourist {
 namespace Util {

class Config
{
public:
  
  /**
   * Loads the Application properties file given by CONFIG_FILE_PATH and also
   * loads the logger properties file configured as lLOG_FILE_PATH.
   * 
   * In order for 'Tourist' to successfuly operate the configuration operation
   * should execute successfuly. Currenlty there isn't any fallback to default values
   * but it should be a TODO: Configure fall-back values 
   * 
   * There are also classes like 'TransportFactory' which requires to pass on the
   * instance of this class. This is useful if we are interested in mulitple transports
   * each on differetn server.
   */
  Config(const string logfile = LOG_FILE_PATH , const string configfile = CONFIG_FILE_PATH);

  Config(const Config &conf);

  Config& operator=(const Config& conf);
  
  ~Config();
  
  //Removed this class as being singleton
  
  //static Config* getInstance();

  /**
   * Boot locations.
   *
   * \return Collection of all available boot locations.
   */
  vector<SocketAddress> getBootAddresses();

  /**
   * Returns a boot address at a given index.
   *
   * Throws IndexOutOfBoundException if the index is not valid.
   */
  SocketAddress getBootAddress(int index);

  /**
   * Address for logging server.
   */
  SocketAddress getRemoteLogServer();

  /**
   * Boot role (server, client, dual).
   *
   * Boot role is specified using property 'tourist.boot.role'.
   * \return BOOT_SERVER, BOOT_CLIENT, BOOT_DUAL
   */
  int getBootRole();

  /**
   * Via property tourist.transport.tcp.port
   */
  int getTCPPort();

  int getRUDPPort();

  /**
   * Via property tourist.network.bwThreshold
   */
  int getBWThreshold();

  /**
   * To enable TCP as one of the communication protocol
   */
  void setTCPEnable(bool isEnable);

  void setTCPPort(int port);

  void setRUDPEnable(bool isEnable);

  void setRUDPPort(int port);

  void setBootRole(int role);

  void setNodeName(string node);

  void setBWThreshold(int th);

  void setRemoteLogServer(SocketAddress addr);

  void setApMonEnable(bool isEnable);

  bool getTCPEnable();

  bool getRUDPEnable();

  bool getApMonEnable();

  string getNodeName();
  
  void setMessageTypeHandler(Tourist::Message::MessageTypeData *data);
  
  Tourist::Message::MessageTypeData* getMessageTypeHandler();
  
  void setMessageBus(Tourist::Message::MessageDispatcher *msgBus);
  
  Tourist::Message::MessageDispatcher* getMessageBus();

  void setLocalNode(Tourist::LocalNode *node);

  Tourist::LocalNode* getLocalNode();
  
private:
  
  //static Config* _instance;
  vector<SocketAddress> bootNodes;

  SocketAddress remoteLogServer;

  string nodeName;

  bool isTCP;
  bool isRUDP;
  bool isNetworkLog;
  bool isApmon;

  int tcpPort;
  int rudpPort;
  int bootRole;
  long bwThreshold;

  AutoPtr<PropertyFileConfiguration> pConfig;
  AutoPtr<PropertyFileConfiguration> lConfig;
  LoggingConfigurator logConfig;
  
  /* Some common objects */
  Tourist::LocalNode *localNode;
  Tourist::Message::MessageTypeData *typeData;
  Tourist::Message::MessageDispatcher *messageBus;
  

  void load();
};

} //Util namespace
}// Tourist namespace


#endif /*CONFIG_H_*/
