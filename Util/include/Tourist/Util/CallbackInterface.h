#ifndef SIMPLECALLBACK_H_
#define SIMPLECALLBACK_H_

/**
 *  A class offering a single (pure virtual) method for
 *  exchanging events between interested classes.
 * 
 */
 
namespace Tourist {
namespace Util {

class CallbackInterface
{
public:
  //The parameter type is kept void to reuse the same clas
  // at different places .
  virtual void callback(void *obj) = 0;

};

} // namespace Util
} // namespace Tourist


#endif /*SIMPLECALLBACK_H_*/
