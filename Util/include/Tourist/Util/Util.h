#ifndef UTIL_H_
#define UTIL_H_

#include "Poco/Logger.h"
#include <sstream>
#include <string>

using Poco::Logger;

namespace Tourist {
namespace Util {

#define fatal(logger, msg) \
     if ((logger).fatal()) \
       (logger).fatal(msg)

#define error(logger, msg) \
     if ((logger).error()) \
       (logger).error(msg)

#define warning(logger, msg) \
     if ((logger).warning()) \
       (logger).warning(msg)

#define notice(logger, msg) \
     if ((logger).notice()) \
       (logger).notice(msg)

#define notice(logger, msg) \
     if ((logger).notice()) \
       (logger).notice(msg)

#define info(logger, msg) \
     if ((logger).information()) \
       (logger).information(msg) 

#define debug(logger, msg) \
     if ((logger).debug()) \
       (logger).debug(msg)


///Converts an integer to string.
std::string toString(int);


} //namespace Util
} //namespace Tourist
#endif /*UTIL_H_*/
