#ifndef TESTUTIL_H_
#define TESTUTIL_H_

#include "CppUnit/TestCase.h"
#include "Tourist/Util/Util.h"

using namespace Tourist::Util;

class TestUtil: public CppUnit::TestCase
{
public:
  TestUtil(const std::string& name);

  ~TestUtil();

  void setUp();

  void tearDown();

  void testToString();

  void testGetNodeHeader();

  void testNodeFromHeader();

  static CppUnit::Test* suite();

};

#endif /*TESTUTIL_H_*/
