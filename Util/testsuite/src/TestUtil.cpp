/*
 * This file is part of TouristP2PImpl
 *
 * Copyright (C) 2007,2008 NUST Institute of Information Technology
 * Author: Faisal Khan <faisal.khan at [niit.edu.pk | cern.ch ]>
 *
 * TouristP2PImpl is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TouristP2PImpl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TouristP2PImpl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "TestUtil.h"
#include "Tourist/RemoteNode.h"
#include "Tourist/Id.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"

using Tourist::RemoteNode;
using Tourist::Id;


TestUtil::TestUtil(const std::string& name): CppUnit::TestCase(name)
{
}

TestUtil::~TestUtil()
{
}

void TestUtil::setUp()
{
}

void TestUtil::tearDown()
{
}

void TestUtil::testToString()
{
  int x = 12222;
  string s = Tourist::Util::toString(x);
  assert(s == "12222");
}

void TestUtil::testGetNodeHeader()
{
  hessian_output ho;
  Id id;
  RemoteNode testNode(id.hexstring(), 3, "localhost", 1111, TCP);

  string rpc;

  try
  {
    rpc = ho.start_call("Test");
    rpc = getNodeHeader(ho, rpc, testNode);
    rpc = ho.set_parameter(rpc, new Integer(1));
    rpc = ho.complete_call(rpc);
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    fail("Failed to create message contents: " + emsg);
  }
  catch (Poco::Exception &pe)
  {
    fail(pe.what());
  }

  //Lets verify the hessian message contents.
  try
  {
    auto_ptr<input_stream> is(new string_input_stream(rpc));
    hessian_input hi(is);

    string methodName = hi.start_call()->value();
    assert(methodName == "Test");

    string string_id    = hi.read_string(hi.get_tag());
    assert(string_id == id.hexstring());

    int level    = hi.read_int(hi.get_tag());
    assert (level == testNode.getLevel());

    string name  = hi.read_string(hi.get_tag());
    assert (name == testNode.getName());

    string host  = hi.read_string(hi.get_tag());
    assert (host == testNode.getHost());

    int port     = hi.read_int(hi.get_tag());
    assert (port == testNode.getPort());

    int protocol = hi.read_int(hi.get_tag());
    assert (protocol == testNode.getProtocol());

    int testparam = hi.read_int(hi.get_tag());
    assert (testparam == 1);
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    fail("Failed to verify message contents: " + emsg);
  }
}

void TestUtil::testNodeFromHeader()
{
   hessian_output ho;
  Id id;
  RemoteNode testNode(id.hexstring(), 3, "localhost", 1111, TCP);

  string rpc;

  try
  {
    rpc = ho.start_call("Test");
    rpc = getNodeHeader(ho, rpc, testNode);
    rpc = ho.set_parameter(rpc, new Integer(1));
    rpc = ho.complete_call(rpc);
  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    fail("Failed to create message contents: " + emsg);
  }
  catch (Poco::Exception &pe)
  {
    fail(pe.what());
  }

  //Lets verify the hessian message contents.
  try
  {
    auto_ptr<input_stream> is(new string_input_stream(rpc));
    hessian_input hi(is);

    string methodName = hi.start_call()->value();
    assert(methodName == "Test");

    RemoteNode rn;
    nodeFromHeader(hi, rn);

    assert(rn.hexstring() == testNode.hexstring());
    assert(rn.getLevel() == testNode.getLevel());
    assert(rn.getHost() == testNode.getHost());
    assert(rn.getPort() == testNode.getPort());
    assert(rn.getProtocol() == testNode.getProtocol());
    assert(rn.getName() == testNode.getName());

    int testparam = hi.read_int(hi.get_tag());
    assert (testparam == 1);

  }
  catch (hessian::exceptions::io_exception& e)
  {
    string emsg = e.what();
    fail("Failed to verify message contents: " + emsg);
  }
}

CppUnit::Test* TestUtil::suite()
{
  CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestUtil");
  CppUnit_addTest(pSuite, TestUtil, testToString);
  CppUnit_addTest(pSuite, TestUtil, testGetNodeHeader);
  CppUnit_addTest(pSuite, TestUtil, testNodeFromHeader);

  return pSuite;
}
