# Launch multipe Tourist node with its own configuration
# As input a working directory is specified where the script will
# copy all the necessary configuration files. 
#log message if the logging is
log() 
{
  if [ $isDebug -ne 0 ]; then
      echo $1
  fi
}

usage()
{
  echo ""
  echo "Usage :"
  echo "$0 [-d] [-n nodes] [-w workingDir] [-l logger.properties] "\
    "[ -p node.properties] [-r portStartRange] "

  echo ""
  echo "-d : Enable debugging"
  echo "-o : Overwrite existing node files"
  echo ""
}

#ensures that working direcoty is
#there otherwise will try to create
checkOrCreateWD()
{
    if [ x"$workingDir" == "x" ]; then
	echo "Please specify path to a working directory"
	usage
        exit -1
    fi
    if ! [ -d $workingDir ]; then
	log "$workingDir doesnt exist, creating one!"
        mkdir $workingDir

       # if directory creation was failed
       if [ $? != 0 ]; then
           echo "Failed to create $workingDir dir. Exiting!"
           exit -1
       fi  
    fi  
}

createPeerDir()
{  
    if [ x"$nodeCount" == "x" ]; then
        log "Node count was not set, defaulting to 1"
	nodeCount=1
    fi

    log "Creating working directories"
    for (( i=0; i<$nodeCount; i++ )); do
        dirName="$namePattern$i"
        if [ -d "$workingDir/$dirName" ]; then
	    log "Process $dirName direcotry already exists"
	    if [ $overWrite -eq 1 ]; then
	        rm $workingDir/$dirName/*
	        placeConfigFiles "$workingDir/$dirName" $i
	    fi
	else
	    mkdir "$workingDir/$dirName"
	    placeConfigFiles "$workingDir/$dirName" $i
	fi 
    done
}

#places node.properties and logger.properties
#in peer directories
placeConfigFiles()
{ 
    if ! [ -f $logFile ]; then
        echo "No log file specified or it doesn't exist";
        usage
        exit -1
    fi

    if ! [ -f $propFile ]; then
        echo "No node properties file specified or it doesn't exist!"
	usage
	exit -1
    fi

    if ! [ -d $1 ]; then
        echo "Couldn't copy log/node properties file as node direcotry doesn't exist"
	exit -1
    fi

    #copy the required file to desired directory
    #after processing node/log properties files for
    #some node specifiec values.
    tempFile="/tmp/tourist.tmp"
    cp $propFile $tempFile
    if [ $? -ne 0 ]; then
        echo "Copying of temp file failed"
    fi

    port=`expr $portStart + $2`
    cat $tempFile | sed s/"tcp.port[\t| ]*=[\t| ]*2020"/"tcp.port = $port"/ &> /tmp/out

    if [ $2 -eq $bootNode ]; then
        cp /tmp/out $tempFile       
        cat $tempFile | sed s/"boot.role[\t| ]*=[\t| ]*2"/"boot.role = 1"/ &> /tmp/out
    fi

    cp /tmp/out "$1/node.properties"
    cp $logFile $1

}

launchPeers()
{
    #first launch the bootnode
    if [ $bootNode -ne -1 ]; then
       dirName=$workingDir/$namePattern$bootNode
       log "Launching boot node at $dirName"
       launchPeerHelper $dirName
       sleep 5
    fi

    #Now launch the other nodes
    for (( i=0; i<$nodeCount; i++ )); do
        if [ $i -ne $bootNode ]; then
            dirName="$workingDir/$namePattern$i"
	    launchPeerHelper $dirName
	    sleep 2
	else
	    log "Ignoring node $i as it's probably already started"
	fi
    done
}

launchPeerHelper()
{
    if ! [ -d "$1" ]; then
        echo "Couldn't launch peer"
	echo "Working directory for one of the peer ($1) doesn't exist"
	exit -1;
    fi

    if ! [ -f "$1/node.properties" ]; then
        echo "Couldn't launch peer."
	echo "node.properties doesn't exist for peer $i"
	exit -1
    fi
	
    if  [ x"$executable" == "x" ] || ! [ -f $executable ]; then
        echo "Tourist executable doesn't exists"
        echo "Make sure you specified the path correctly. See help"
        exit -1
    fi

    cd $1	
    $executable -p$1/node.properties -o$1/logger.properties &>$1/out &
}


#### Main ####

#directory, where prop for each file 
workingDir=$1

#Number of tourist process by default 1
nodeCount=1

logFile=logger.properties

propFile=node.properties

isDebug=0

namePattern=node

portStart=50001

overWrite=0

bootNode=-1

executable=

while [ $# -gt 0 ]
do
    case "$1" in
        -n)  nodeCount=$2; shift;;  #number of nodes to launch
	-w)  workingDir=$2; shift;; #working dir
	-l) logFile=$2; shift;;  #log file
	-p) propFile=$2; shift;; #node.properties file
	-b) bootNode=$2; shift;; #node number which is also boot node.
	-e) executable=$2; shift;; #tourist launcher
	-d)  isDebug=1;; 
	-o) overWrite=1;;
	-r) portStart=$2; shift;;
	-*) usage; exit -1;;
	*) break;; #terminate
    esac
    shift
done

log "Node count : $nodeCount"
log "Logger properties : $logFile"
log "Node properties : $propFile"
log "Port start : $portStart"
log "Boot node : $bootNode"
log "Executable : $executable"

#creat working directory if it
#doesn't exist
checkOrCreateWD
createPeerDir
launchPeers
